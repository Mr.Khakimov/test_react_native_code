import React, { useCallback, useState, useMemo } from "react";
import { Pressable, StyleSheet, Text, View , ActivityIndicator} from 'react-native';

type RoundedButtonProps = {
  title: string;
  onPress: () => void;
  backgroundColor: string;
  hoverBackgroundColor: string;
  error?: string;
  type?: string;
  loading: false,
};

export const RoundedButton: React.FC<RoundedButtonProps> = ({ title, onPress, disabled, loading, type, backgroundColor, hoverBackgroundColor, error , ...props}) => {
  const [isHovered, setIsHovered] = useState(false);

  const buttonStyles = useMemo(() => {
    return [
      styles.button,
      { backgroundColor: type === 'primary' ? disabled ? '#545454' : isHovered ? '#002' : '#3c3ce8' : '#002' },
    ];
  }, [isHovered, backgroundColor, hoverBackgroundColor, disabled]);

  const handlePressIn = useCallback(() => {
    setIsHovered(true);
  }, []);

  const handlePressOut = useCallback(() => {
    setIsHovered(false);
  }, []);

  return (
    <View style={styles.container}>
      <Pressable
        style={buttonStyles}
        onPress={onPress}
        onPressIn={handlePressIn}
        onPressOut={handlePressOut}
        hasTVPreferredFocus={true}
        disabled={disabled}
        {...props}
      >
        {!loading ? <Text style={[styles.buttonText, disabled && styles.disabledText]}>{title}</Text> : <ActivityIndicator size="small" color="#fff" />}
      </Pressable>
      {error && <Text style={styles.errorText}>{error}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // alignItems: 'center',
    width: '100%',
  },
  button: {
    height: 48,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold',
  },
  disabledText: {
    color: '#c5c2c2',
  },
  errorText: {
    marginTop: 8,
    fontSize: 12,
    color: 'red',
  },
});
