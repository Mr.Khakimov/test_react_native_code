import React from 'react';
import {Modal, View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {CloseIcon} from './icons';

const CustomModal = ({
  visible,
  title,
  content,
  contentIsText,
  onClose,
  onCloseX,
  onConfirm,
}) => {
  return (
    <Modal visible={visible} transparent>
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <TouchableOpacity
            style={styles.closeIconContainer}
            onPress={onCloseX}>
            <CloseIcon style={styles.closeIcon} />
          </TouchableOpacity>

          <Text style={styles.title}>{title}</Text>

          {contentIsText ? (
            <Text style={styles.content}>{content}</Text>
          ) : (
            <View style={styles.contentView}>{content}</View>
          )}

          <View style={styles.buttonContainer}>
            {onClose && (
              <TouchableOpacity style={styles.button} onPress={onClose}>
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity>
            )}

            {onConfirm && (
              <TouchableOpacity style={styles.button} onPress={onConfirm}>
                <Text style={styles.buttonText}>Confirm</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#FFF',
    // borderRadius: 8,
    padding: 12,
    width: '100%',
    maxHeight: '100%',
  },
  closeIconContainer: {
    position: 'absolute',
    top: 12,
    right: 12,
    zIndex: 1,
  },
  closeIcon: {
    width: 24,
    height: 24,
    tintColor: '#888',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 12,
  },
  content: {
    fontSize: 16,
    marginBottom: 12,
  },
  contentView: {
    flexGrow: 1,
    height: '100%',
    marginBottom: 12,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  button: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 4,
    backgroundColor: '#007AFF',
    marginLeft: 8,
  },
  buttonText: {
    color: '#FFF',
    fontSize: 16,
  },
});

export default CustomModal;
