import React from "react";
import { TextInput, StyleSheet } from "react-native";

const TaskSearchInput = () => {
  return (
    <TextInput
      style={styles.input}
      placeholder="Vazifalar bo'yicha qidiruv"
      placeholderTextColor="#002"
    />
  );
};

const styles = StyleSheet.create({
  input: {
    borderBottomWidth: 1,
    borderBottomColor: "#002",
    color: "#002",
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 12,
    width: 230,
  },
});

export default TaskSearchInput;
