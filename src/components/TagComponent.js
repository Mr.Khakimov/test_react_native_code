import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export const Tag = ({ label, bg }) => {
  return (
    <View style={[styles.container, {backgroundColor: bg ? bg : 'lightgray'}]}>
      <Text style={styles.label}>{label}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 16,
    paddingHorizontal: 10,
    paddingVertical: 6,
    marginRight: 4,
    marginBottom: 4,
  },
  label: {
    fontSize: 12,
    color: 'white',
    fontWeight: 'bold',
  },
});
