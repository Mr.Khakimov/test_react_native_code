import React, { ReactNode, useCallback } from 'react';
import { View, StyleSheet } from 'react-native';
import CustomTitle from './Text/CustomTitle';
import IconButton from './Buttons/IconButton';
import { ArrowRightDarkIcon } from './icons';
import { constant } from '../utils/theme';

type SectionTitleProps = {
  title: string,
  value: number | string,
  onPress?: (value : number | string) => void,
  icon?: ReactNode,
}

const SectionTitle: React.FC<SectionTitleProps> = ({ title, icon, value = "flowers", onPress }) => {  
  
  const onHandle = useCallback(() => {
    onPress(value);
  }, [])
  
  return (
    <View style={styles.container}>
      <CustomTitle type='section'>{title}</CustomTitle>
      {onPress ? <IconButton onPress={onHandle} type="secondary" size="small">
        {icon ? icon : <ArrowRightDarkIcon />}
      </IconButton> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: constant.space2,
    paddingVertical: constant.space2,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333',
  },
});

export default SectionTitle;