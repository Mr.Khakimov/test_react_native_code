
import React, { forwardRef, Ref, useImperativeHandle } from 'react';
import {
  View,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextInputProps,
  Pressable,
} from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';
import { constant, MainStyle } from '../../utils/theme';
import CustomTextComponent from '../CustomTextComponent';

interface CustomInputProps extends TextInputProps {
  icon?: React.ReactNode;
  containerStyle?: StyleProp<ViewStyle>;
  onPress: () => void;
  value?: string,
}

export interface CustomInputRef {
  focus: () => void;
}

const SeachButton: React.FC<CustomInputProps> = ({ icon, containerStyle, onPress, value, ...restProps }) => {
 
  const { theme } = useTheme();

  return (
    <Pressable style={[styles.container, containerStyle, {backgroundColor: theme.grey3}]} onPress={onPress}>
      {icon && <View style={styles.iconContainer}>{icon}</View>}
      <CustomTextComponent
        style={MainStyle.mr1}
        textType="regular"
        size="large"
        color={'gray'}>{value}
      </CustomTextComponent> 
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: constant.defaultSize / 3.5,
    height: constant.defaultSize,
    paddingHorizontal: 12,
    // paddingVertical: 8,
  },
  iconContainer: {
    marginRight: 8,
  },
  input: {
    flex: 1, // Customize as needed
  },
});

export default SeachButton;