import React, {FC, ReactNode, useMemo} from 'react';
import {
    ActivityIndicator,
    Pressable,
    StyleSheet,
    ViewStyle,
} from 'react-native';
import {useTheme} from "../../customHooks/ThemeHook";
import { constant } from '../../utils/theme';
import CustomTextComponent from '../CustomTextComponent';

type CustomButtonProps = {
    style?: ViewStyle | ViewStyle[],
    children?: ReactNode,
    text: string,
    type?: string,
    size?: string,
    onPress: () => void,
    disabled?: boolean,
    loading?: boolean,
}

const CustomButton:FC<CustomButtonProps> = ({ children, loading, text, disabled, style = {}, type = 'primary', size = 'large', onPress}) => {
    const {theme} = useTheme();

    const btnMemoStyle = useMemo(() => {

        const s = size === 'small' ? constant.btnSizeSmall : 
                  size === 'medium' ? constant.btnSizeMedium : 
                  size === 'large' ? constant.btnSizeLarge : 
                  constant.defaultSize

        return {
            height: s,
            borderRadius: s / 3.5,
        }

    }, [size]);

    return (
        <Pressable disabled={disabled} onPress={onPress} style={({ pressed }) => [
            {
                backgroundColor: 
                    type === 'default' ?
                        pressed
                        ? theme.textGrey
                        : 'transparent'
                    : type === 'primary' ?
                        pressed
                        ? theme.primaryColorHover
                        : theme.primaryColor
                    : type === 'secondary' ?
                        pressed
                        ? theme.pressHover
                        : theme.grey3
                    : type === 'danger' ?
                        pressed
                        ? theme.primaryColorHover
                        : theme.red    
                    : type === 'success' ?
                        pressed
                        ? theme.pressHover
                        : theme.green
                    :
                        pressed
                            ? theme.pressHover
                            : theme.grey3
            },
            btnMemoStyle,
            styles.btn,
            disabled && {backgroundColor: theme.grey3},
            style,
        ]}>
            {
                loading ? <>
                    <ActivityIndicator size="small" color={theme.textDark} />
                </> : <>
                    {text ? <CustomTextComponent color={disabled ? 'gray' : type === 'secondary' ? 'dark' : 'white'} size='large' textType='bold'>{text}</CustomTextComponent> : null}
                    {children}
                </>
            }
            
        </Pressable>
    )
}

const styles = StyleSheet.create({
    btn: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default CustomButton;
