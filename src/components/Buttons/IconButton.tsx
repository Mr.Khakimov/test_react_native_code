import React, {FC, ReactNode, useMemo} from 'react';
import {
    Pressable,
    StyleSheet,
    ViewStyle,
} from 'react-native';
import {useTheme} from "../../customHooks/ThemeHook";
import { constant } from '../../utils/theme';

type IconButtonProps = {
    style?: ViewStyle | ViewStyle[],
    children?: ReactNode,
    type?: string,
    size?: string,
    onPress: () => void,
    disabled?: boolean,
}

const IconButton:FC<IconButtonProps> = ({ children, style = {}, disabled=false, type, size, onPress}) => {
    const {theme} = useTheme();

    const btnMemoStyle = useMemo(() => {

        const s = size === 'small' ? constant.btnSizeSmall : 
                  size === 'medium' ? constant.btnSizeMedium : 
                  size === 'large' ? constant.btnSizeLarge : 
                  constant.defaultSize

        return {
            width: s,
            height: s,
            borderRadius: s / 3.5,
        }

    }, [size]);

    const disableMemoStyle = useMemo(() => {
        return {opacity: disabled ? 0.4 : 1}
    }, [disabled])

    return (
        <Pressable onPress={onPress} disabled={disabled} style={({ pressed }) => [
            {
                backgroundColor: 
                    type === 'default' ?
                        pressed
                        ? theme.textGrey
                        : 'transparent'
                    : type === 'primary' ?
                        pressed
                        ? theme.primaryColorHover
                        : theme.primaryColor
                    : type === 'secondary' ?
                        pressed
                        ? theme.pressHover
                        : theme.grey3
                    :
                        pressed
                            ? theme.pressHover
                            : theme.grey3
            },
            btnMemoStyle,
            disableMemoStyle,
            styles.btn,
            style,
        ]}>
            {children}
        </Pressable>
    )
}

const styles = StyleSheet.create({
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default IconButton;
