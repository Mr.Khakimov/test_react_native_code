import React, {FC, ReactNode, useMemo} from 'react';
import {
    Pressable,
    StyleSheet,
    ViewStyle,
} from 'react-native';
import {useTheme} from "../../customHooks/ThemeHook";
import { constant } from '../../utils/theme';

type IconButtonProps = {
    style?: ViewStyle | ViewStyle[],
    children?: ReactNode,
    type?: string,
    size?: string,
    onPress: () => void,
}

const IconRoundedButton:FC<IconButtonProps> = ({ children, style = {}, type, size, onPress}) => {
    const {theme} = useTheme();

    const btnMemoStyle = useMemo(() => {

        const s = size === 'small' ? constant.btnSizeSmall : 
                  size === 'medium' ? constant.btnSizeMedium : 
                  size === 'large' ? constant.btnSizeLarge : 
                  constant.defaultSize

        return {
            width: s,
            height: s,
            borderRadius: s,
        }

    }, [size]);

    return (
        <Pressable onPress={onPress} style={({ pressed }) => [
            {
                backgroundColor: 
                    type === 'default' ?
                        pressed
                        ? theme.textGrey
                        : 'transparent'
                    : type === 'primary' ?
                        pressed
                        ? theme.primaryColorHover
                        : theme.primaryColor
                    : type === 'secondary' ?
                        pressed
                        ? theme.pressHover
                        : theme.grey3
                    :
                        pressed
                            ? theme.pressHover
                            : theme.grey3
            },
            btnMemoStyle,
            styles.btn,
            style,
        ]}>
            {children}
        </Pressable>
    )
}

const styles = StyleSheet.create({
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default IconRoundedButton;
