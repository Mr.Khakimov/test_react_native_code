import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export const ListItemWithIcon = ({
  icon,
  title,
  description,
  onPress,
  leftContent,
  divider,
  deskIsText,
  titleStyle,
  disabled,
  rightContent,
}) => {
  return (
    <>
      <TouchableOpacity style={[styles.container, !description && styles.containerDesc]} disabled={disabled} onPress={onPress}>
        {leftContent ? (
          <View style={styles.iconContainer}>{leftContent}</View>
        ) : null}

        <View style={styles.textContainer}>
          <Text style={[styles.title, titleStyle]}>{title}</Text>

          {description ? deskIsText ? (
            <Text style={styles.description}>{description}</Text>
          ) : (
            <View style={styles.description}>{description}</View>
          ) : null}
        </View>

        {rightContent ? (
          <View style={styles.iconContainer}>{rightContent}</View>
        ) : null}
      </TouchableOpacity>
      {divider && <View style={styles.divider} />}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 6,
    paddingHorizontal: 12,
    minHeight: 50,
  },
  containerDesc: {
    alignItems: 'center',
  },
  iconContainer: {
    marginRight: 12,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  description: {
    fontSize: 14,
    color: 'gray',
    width: '100%',
  },
  divider: {
    height: 1,
    backgroundColor: '#dcdcdc',
  },
});
