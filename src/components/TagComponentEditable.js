import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import {AddIcon, ClearIcon, DoneIcon} from './icons';

const TagComponentEditable = ({tags, onTagClear, onAddTag}) => {
  const [inputValue, setInputValue] = useState('');
  const [isInputEmpty, setIsInputEmpty] = useState(true);
  const [newTag, setNewTag] = useState(false);

  const handleInputChange = text => {
    setInputValue(text);
    setIsInputEmpty(text.trim() === '');
  };

  const handleAddTag = () => {
    if (inputValue.trim() !== '') {
      onAddTag(inputValue.trim());
      setInputValue('');
      setIsInputEmpty(true);
      setNewTag(false);
    }
  };

  const handleNewTag = () => {
    setNewTag(true);
  };

  return (
    <View style={styles.container}>
      {tags.map((tag, index) => (
        <View
          key={index}
          style={[styles.tagContainer, {backgroundColor: tag.bg}]}>
          <Text style={styles.tagText}>{tag.name}</Text>
          <TouchableOpacity
            style={styles.clearIconContainer}
            onPress={() => onTagClear(tag.id)}>
            <ClearIcon style={styles.clearIcon} />
          </TouchableOpacity>
        </View>
      ))}
      <View style={styles.inputContainer}>
        {newTag ? (
          <>
            <TextInput
              style={styles.input}
              placeholder="yangi qo'shish"
              placeholderTextColor={'#002'}
              value={inputValue}
              onChangeText={handleInputChange}
            />
            <TouchableOpacity
              style={[
                styles.addButton,
                isInputEmpty && styles.addButtonDisabled,
              ]}
              onPress={handleAddTag}
              disabled={isInputEmpty}>
              <DoneIcon />
            </TouchableOpacity>
          </>
        ) : (
          <>
            <TouchableOpacity style={[styles.addButton]} onPress={handleNewTag}>
              <AddIcon />
            </TouchableOpacity>
          </>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  tagContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
    borderRadius: 16,
  },
  tagText: {
    fontSize: 12,
    color: '#ffffff',
    marginRight: 4,
  },
  clearIconContainer: {
    padding: 4,
    marginLeft: 4,
  },
  clearIcon: {
    width: 12,
    height: 12,
    tintColor: '#888888',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  input: {
    flex: 1,
    height: 30,
    borderWidth: 1,
    borderColor: '#888888',
    borderRadius: 8,
    padding: 0,
    paddingHorizontal: 8,
    marginRight: 4,
    color: '#002',
  },
  addButton: {
    backgroundColor: '#e0e0e0',
    borderRadius: 8,
    padding: 4,
    marginLeft: 4,
  },
  addButtonDisabled: {
    backgroundColor: '#cccccc',
  },
  saveIcon: {
    width: 12,
    height: 12,
    tintColor: '#333333',
  },
});

export default TagComponentEditable;
