import React, {FunctionComponent, ReactNode, memo, useMemo} from 'react';
import {StyleSheet, Text, TextProps, TextStyle} from 'react-native';
import { useTheme } from '../customHooks/ThemeHook';
import { constant } from '../utils/theme';

const sizes = {
    small: "small",
    medium: "medium",
    large: "large",
    extraLarge: "extraLarge",
    big: "big",
} as const;

export interface CustomTextProps extends TextProps {
    style?: TextStyle | TextStyle[];
    textType?: 'regular' | 'bold' | 'semiBold';
    size?: keyof typeof sizes;
    children?: ReactNode;
    color?: 'dark' | 'grey' | 'gray' | 'white';
}

const CustomTextComponent: FunctionComponent<CustomTextProps> = memo(({ children, textType, size = 'medium', style, color = 'gray', ...rest }) => {

    const {theme} = useTheme();

    const textStyle = useMemo(() => {

        let textStyle: {}

        switch (textType) {
            case 'regular':
                textStyle = styles.regular
                break
            case 'bold':
                textStyle = styles.bold
                break
            case 'semiBold':
                textStyle = styles.semiBold
                break
            default:
                textStyle = styles.regular
                break
        }

        let textStyleSize: {}

        switch (size) {
            case 'small':
                textStyleSize = styles.smallSize
                break
            case 'large':
                textStyleSize = styles.largeSize
                break
            case 'extraLarge':
                textStyleSize = styles.extraLargeSize
                break
            case 'medium':
                textStyleSize = styles.mediumSize
                break 
            case 'big':
                textStyleSize = styles.bigSize
                break        
            default:
                textStyleSize = styles.mediumSize
                break
        }

        let textStyleColor: {}

        switch (color) {
            case 'white':
                textStyleColor = {color: theme.textWhite}
                break
            case 'dark':
                textStyleColor = {color: theme.textDark}
                break
            case 'grey':
                textStyleColor = {color: theme.textGrey}
                break
            case 'gray':
                textStyleColor = {color: theme.textGray}
                break
            default:
                textStyleColor = {color: theme.textGrey}
                break
        }

        return {...textStyle, ...textStyleSize, ...textStyleColor}
    }, [color, size, textType]);


    const passedStyles = Array.isArray(style) ? Object.assign({}, ...style) : style


    return (
        <Text style={[textStyle, { ...passedStyles } ]} {...rest}>{children}</Text>
    );
})

const styles = StyleSheet.create({
    regular: {
        fontFamily: constant.interRegular
    },
    bold: {
        fontFamily: constant.interBold
    },
    semiBold: {
        fontFamily: constant.interSemiBold
    },
    smallSize: {
        fontSize: constant.textSmall,
        lineHeight: constant.textSmallLineHeight
    },
    mediumSize: {
        fontSize: constant.textMedium,
        lineHeight: constant.textMediumLineHeight
    },
    largeSize: {
        fontSize: constant.textLarge,
        lineHeight: constant.textLargeLineHeight,
    },
    extraLargeSize: {
        fontSize: constant.textExtra,
        lineHeight: constant.textExtraLineHeight
    },
    bigSize: {
        fontSize: constant.textBig,
        lineHeight: constant.textBigLineHeight
    }
})

export default CustomTextComponent;
