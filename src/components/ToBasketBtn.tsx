import { Pressable, StyleSheet, View } from "react-native";
import { MainStyle, constant, lightTheme } from "../utils/theme";
import CustomTextComponent from "./CustomTextComponent";
import { useLocalization } from "../customHooks/LocalizationHook";
import { ChevronRightWhiteIcon } from "./icons";
import { isIos } from "../utils/constants";
import { useAppSelector } from "../redux/store";
import { BasketProductImg } from "./BasketImg";
import { useMemo } from "react";
import { ordersData } from "../MOCK/otherProducts";
import { PriceFormat } from "../utils/priceFormat";

export const ToBasketBtn = ({style = {}, handleBasketLink}) => {
    const {t} = useLocalization();
    const basket = useAppSelector(state => state.basket.products);
    const showBasketLink = useAppSelector(state => state.basket.showBottomLink);
    

    // console.log('showBasketLink', showBasketLink);
    

    const price = useMemo(() => {
        let pr = 0
        basket.forEach(el => {
            const productPrice = ordersData.find(item => el.id === item.id)?.price;
            if(productPrice) {
                pr += (parseFloat(productPrice) * el.count)
            }
        })

        return pr
    }, [basket]);
    
    if(!basket.length) {
        return null
    }

    if(!showBasketLink) {
        return null
    }

    return (
        <View style={[styles.container, style]}>
            <Pressable onPress={handleBasketLink} style={styles.wrapper}>
                <View style={styles.imgWrapper}>
                    {
                        basket.map((el, index) => {
                            if(index === 0) {
                                return <BasketProductImg key={index} id={el.id} />
                            }

                            if(index === 1) {
                                return <BasketProductImg key={index} style={styles.img2} id={el.id} />
                            }
                            if(index === 2) {
                                return <View style={styles.productLength}>
                                            <CustomTextComponent
                                                textType="bold"
                                                size="large"
                                                color={'dark'}>
                                                {basket.length}
                                            </CustomTextComponent>
                                        </View>
                            }
                            return null                            
                        })
                    }
                </View>
    
                <View style={MainStyle.flexGrow}>
                    <CustomTextComponent 
                        style={MainStyle.mr1}
                        textType="bold"
                        size="large"
                        color={'white'}>
                        {t('order')} {PriceFormat(price)} {t('currency')}
                    </CustomTextComponent>
                    <CustomTextComponent 
                        style={MainStyle.mr1}
                        textType="regular"
                        size="small"
                        color={'white'}>
                        {/* {state.deliveryTime} */}
                        {t('Доставка бесплатно 30-40 мин')}
                    </CustomTextComponent> 
                </View>
    
                <View>
                    <ChevronRightWhiteIcon />
                </View>
            </Pressable> 
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        zIndex: 100,
        bottom: isIos ? 75 : 50,
        left: 0,
        right: 0,
        backgroundColor: lightTheme.dark,
    },
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 60,
        padding: constant.space1,
    },
    imgWrapper: {
        position: 'relative',
        marginRight: 12,
        flexDirection: 'row',
        // maxWidth: 83,
    },
    img: {
        width: 46,
        height: 46,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: lightTheme.dark,
        borderRadius: 12,
    },
    img2: {
        width: 46,
        height: 46,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: lightTheme.dark,
        borderRadius: 12,
        marginLeft: -24,
    },
    productLength: {
        width: 46,
        height: 46,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: lightTheme.dark,
        borderRadius: 12,
        marginLeft: -24,
        alignItems: 'center',
        justifyContent: 'center'
    }
})