import React, {useCallback, useEffect, useState} from 'react';
import { PermissionsAndroid } from 'react-native';
import {FlatList, StyleSheet, View} from 'react-native';
import Contacts from 'react-native-contacts';
import {Contact} from './Contact';
import HeaderComponent from '../layaouts/HeaderComponent';
import { LeftChevronIcon } from '../icons';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { SafeAreaView } from 'react-native-safe-area-context';
import { isIos } from '../../utils/constants';

export const ContactsList = ({isOpen = false, handleClose, handleContact}) => {
  const {t} = useLocalization();
  const [contactList, setContactList] = useState([]);

  useEffect(() => {
    if(isIos) {
      Contacts.getAll().then(contacts => {
        setContactList(contacts);
      });
    } else {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: 'Contacts',
        message: 'This app would like to view your contacts.',
        buttonPositive: 'Please accept bare mortal',
    })
        .then((res) => {
            if(res === 'granted') {
              Contacts.getAll()
              .then((contacts) => {
                  // work with contacts
                  console.log(contacts);
                  setContactList(contacts);
              })
              .catch((e) => {
                  console.log(e);
              });

              console.log('Permission: ', res);
            }

            if(res === 'denied') {
              console.log('Permission', res)
            }

            if(res === 'never_ask_again') {
              console.log('Permission', res)
            }
           
        })
        .catch((error) => {
            console.error('Permission error: ', error);
        });
    }
  }, []);

  const keyExtractor = (item, idx) => {
    return item?.recordID?.toString() || idx.toString();
  };

  if(!isIos) return null

  if(!isOpen) return null

  return (
    <SafeAreaView style={styles.contactsModal}>
        <HeaderComponent
            leftIcon={<LeftChevronIcon />}
            onLeftIconPress={handleClose}
            title={t('contacts')}
        />
         <FlatList
             data={contactList}
             renderItem={({item}) => <Contact contact={item} handle={handleContact} />}
             keyExtractor={keyExtractor}
             style={styles.list}
        />
    </SafeAreaView>
    
  );
};
const styles = StyleSheet.create({
    contactsModal: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'white',
    },
  list: {
    flex: 1,
  },
});