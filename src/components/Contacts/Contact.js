import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
export const Contact = ({contact, handle}) => {
  return (
    <TouchableHighlight 
        onPress={() => handle(contact)} 
        activeOpacity={0.6}
        underlayColor="#f1f2f3" 
        style={styles.contactCon}>
        <>
        <View style={styles.imgCon}>
            <View style={styles.placeholder}>
            <Text style={styles.txt}>{contact?.givenName[0]}</Text>
            </View>
        </View>
        <View style={styles.contactDat}>
            <Text style={styles.name}>
            {contact?.givenName} {contact?.middleName && contact.middleName + ' '}
            {contact?.familyName}
            </Text>
            <Text style={styles.phoneNumber}>
            {contact?.phoneNumbers[0]?.number}
            </Text>
        </View>
        </>
    </TouchableHighlight>
  );
};
const styles = StyleSheet.create({
  contactCon: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderBottomWidth: 0.5,
    borderBottomColor: '#d9d9d9',
  },
  imgCon: {},
  placeholder: {
    width: 40,
    height: 40,
    marginRight: 10,
    borderRadius: 30,
    overflow: 'hidden',
    backgroundColor: '#d9d9d9',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contactDat: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 5,
  },
  txt: {
    fontSize: 18,
  },
  name: {
    fontSize: 16,
  },
  phoneNumber: {
    color: '#888',
  },
});