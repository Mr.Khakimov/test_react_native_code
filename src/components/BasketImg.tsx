import { Image, ImageProps, ImageSourcePropType, ImageStyle, StyleSheet } from "react-native"
import { lightTheme } from "../utils/theme";
import { useFindProduct } from "../customHooks/ProductFilterHook";
import React from "react";

interface IBasketProductImg {
    id: string | number | undefined;
    style?: ImageStyle | ImageStyle[];
}

export const BasketProductImg:React.FC<IBasketProductImg> = React.memo(({id, style}) => {
    const product = useFindProduct(id);
    if (product?.img) {
        return (
            <Image style={[styles.img, style]} source={{uri: product.img}}></Image>
        )
    }
    return null
});

const styles = StyleSheet.create({
    img: {
        width: 46,
        height: 46,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: lightTheme.dark,
        borderRadius: 12,
    },
});