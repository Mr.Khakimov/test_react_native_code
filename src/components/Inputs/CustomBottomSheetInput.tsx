
import React, { forwardRef, Ref, useImperativeHandle, useMemo, useState } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextInputProps,
} from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';
import { constant } from '../../utils/theme';
import { BottomSheetTextInput } from '@gorhom/bottom-sheet';

interface CustomBottomSheetInputProps extends TextInputProps {
  icon?: React.ReactNode; // Optional icon
  containerStyle?: StyleProp<ViewStyle>; // Additional container styles
}

export interface CustomInputRef {
  focus: () => void;
}

const CustomBottomSheetInput: React.ForwardRefRenderFunction<CustomInputRef, CustomBottomSheetInputProps> = (
  { icon, containerStyle, ...restProps },
  ref
) => {
  const inputRef = React.useRef<TextInput>(null);
  const { theme } = useTheme();

  const [isFocused, setIsFocused] = useState(false);

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };

  useImperativeHandle(ref, () => ({
    focus: () => {
      if (inputRef.current) {
        inputRef.current.focus();
      }
    },
  }));

  const memoStyle = useMemo(() => {
    return { borderColor: isFocused ? theme.textDark : theme.textGrey}
  }, [isFocused])

  return (
    <View style={[styles.container, containerStyle, memoStyle]}>
      {icon && <View style={styles.iconContainer}>{icon}</View>}
      <BottomSheetTextInput
        ref={inputRef}
        onFocus={handleFocus}
        showSoftInputOnFocus={true}
        onBlur={handleBlur}
        style={[styles.input]}
        placeholderTextColor={theme.gray}// Customize as needed
        {...restProps}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: constant.defaultSize / 3.5,
    borderWidth: 1,
    // height: constant.defaultSize,
    padding: 16,
    // paddingVertical: 8,
  },
  iconContainer: {
    marginRight: 8,
  },
  input: {
    fontFamily: constant.interBold,
    fontSize: constant.textExtra,
    flex: 1,
  },
});

export default forwardRef(CustomBottomSheetInput);