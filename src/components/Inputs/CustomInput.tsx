
import React, { forwardRef, Ref, useImperativeHandle } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextInputProps,
} from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';
import { constant } from '../../utils/theme';

interface CustomInputProps extends TextInputProps {
  icon?: React.ReactNode; // Optional icon
  containerStyle?: StyleProp<ViewStyle>; // Additional container styles
}

export interface CustomInputRef {
  focus: () => void;
}

const CustomInput: React.ForwardRefRenderFunction<CustomInputRef, CustomInputProps> = (
  { icon, containerStyle, ...restProps },
  ref
) => {
  const inputRef = React.useRef<TextInput>(null);
  const { theme } = useTheme();

  useImperativeHandle(ref, () => ({
    focus: () => {
      if (inputRef.current) {
        inputRef.current.focus();
      }
    },
  }));

  return (
    <View style={[styles.container, containerStyle, {backgroundColor: theme.grey3}]}>
      {icon && <View style={styles.iconContainer}>{icon}</View>}
      <TextInput
        ref={inputRef}
        style={[styles.input]}
        placeholderTextColor="#888" // Customize as needed
        {...restProps}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: constant.defaultSize / 3.5,
    height: constant.defaultSize,
    paddingHorizontal: 12,
    // paddingVertical: 8,
  },
  iconContainer: {
    marginRight: 8,
  },
  input: {
    flex: 1, // Customize as needed
  },
});

export default forwardRef(CustomInput);