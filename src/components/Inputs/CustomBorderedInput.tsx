
import React, { forwardRef, Ref, useImperativeHandle, useMemo, useState } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextInputProps,
} from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';
import { constant } from '../../utils/theme';
import { BottomSheetTextInput } from '@gorhom/bottom-sheet';

interface CustomBorderedInputProps extends TextInputProps {
  leftIcon?: React.ReactNode; // Optional icon
  rightIcon?: React.ReactNode; // Optional icon
  containerStyle?: StyleProp<ViewStyle>; // Additional container styles
  isBottomSheet?: boolean;
}

export interface CustomInputRef {
  focus: () => void;
}

const CustomBorderedInput: React.ForwardRefRenderFunction<CustomInputRef, CustomBorderedInputProps> = (
  { leftIcon, rightIcon, containerStyle, isBottomSheet, ...restProps },
  ref
) => {
  const inputRef = React.useRef<TextInput>(null);
  const { theme } = useTheme();

  const [isFocused, setIsFocused] = useState(false);

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };

  useImperativeHandle(ref, () => ({
    focus: () => {
      if (inputRef.current) {
        inputRef.current.focus();
      }
    },
  }));

  const memoStyle = useMemo(() => {
    return { borderColor: isFocused ? theme.textDark : theme.textGrey}
  }, [isFocused])

  return (
    <View style={[styles.container, containerStyle, memoStyle]}>
      {leftIcon && <View style={styles.iconContainer}>{leftIcon}</View>}
      
      {
        isBottomSheet ?
          <BottomSheetTextInput
            ref={inputRef}
            onFocus={handleFocus}
            showSoftInputOnFocus={true}
            onBlur={handleBlur}
            style={[styles.input]}
            placeholderTextColor={theme.gray}// Customize as needed
            {...restProps}
          />
        :
          <TextInput
            ref={inputRef}
            onFocus={handleFocus}
            showSoftInputOnFocus={true}
            onBlur={handleBlur}
            style={[styles.input]}
            placeholderTextColor={theme.gray}// Customize as needed
            {...restProps}
          />
      }
    
      {rightIcon && <View style={styles.iconRightContainer}>{rightIcon}</View>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: constant.defaultSize / 3.5,
    borderWidth: 1,
    // height: constant.defaultSize,
    padding: 16,
    // paddingVertical: 8,
  },
  iconContainer: {
    marginRight: 8,
  },
  iconRightContainer: {
    marginLeft: 8,
  },
  input: {
    fontFamily: constant.interBold,
    fontSize: constant.textExtra,
    flex: 1,
    padding: 0,
  },
});

export default forwardRef(CustomBorderedInput);