import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';

type ListItem = {
  id: number;
  title: string;
  description: string;

  onPress: () => void;
};

export const ListComponent: React.FC<ListItem> = ({ title, description , onPress}) => {
  return (
    <TouchableHighlight
      activeOpacity={0.6}
      underlayColor={"#9f9d9d"}
      style={styles.item}
      onPress={onPress}>
      <>
        <Text style={styles.title}>{title}</Text>
        {description && <Text style={styles.description}>{description}</Text>}
      </>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  description: {
    fontSize: 14,
  },
})
