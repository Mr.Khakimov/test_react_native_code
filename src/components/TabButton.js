import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const TabButton = ({ title, onPress, active }) => {
  const buttonStyles = [styles.button];
  const textStyles = [styles.title];

  if (active) {
    buttonStyles.push(styles.activeButton);
    textStyles.push(styles.activeTitle);
  }

  return (
    <TouchableOpacity style={buttonStyles} onPress={onPress}>
      <Text style={textStyles}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 20,
    // backgroundColor: '#fff',
    marginHorizontal: 2,
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeButton: {
    backgroundColor: '#007bff', // Some active color
  },
  title: {

    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333',
  },
  activeTitle: {
    color: '#fff', // Some active text color
  },
});

export default TabButton;
