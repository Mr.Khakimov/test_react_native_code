import ArrowRightIcon from "../../assets/icons/arrow_right.svg";
import ArrowRightDarkIcon from "../../assets/icons/arrow_right_dark.svg";

import FavoriteIcon from '../../assets/icons/favorite.svg';
import FavoriteFilledIcon from '../../assets/icons/favorite_filled.svg';

import EventIcon from '../../assets/icons/event.svg';
import EventFilledIcon from '../../assets/icons/event_filled.svg';

import HomeIcon from '../../assets/icons/home.svg';
import HomeFilledIcon from '../../assets/icons/home_filled.svg';

import ProfileIcon from '../../assets/icons/profile.svg';
import ProfileFilledIcon from '../../assets/icons/profile_filled.svg';

import MyOrdersIcon from '../../assets/icons/my_orders.svg';
import MyOrdersFilledIcon from '../../assets/icons/my_orders_filled.svg';
import NotificationIcon from '../../assets/icons/notification.svg';
import FilterIcon from '../../assets/icons/filter.svg';
import SearchIcon from '../../assets/icons/search.svg';
import LikeIcon from '../../assets/icons/like.svg';
import LikeFilledIcon from '../../assets/icons/like_filled.svg';
import StarIcon from '../../assets/icons/star.svg';
import PlusIcon from '../../assets/icons/plus.svg';
import MinusIcon from '../../assets/icons/minus.svg';
import RightChevronIcon from '../../assets/icons/chevron_right.svg';
import BonusStarIcon from '../../assets/icons/bonus_star.svg';
import LeftChevronIcon from '../../assets/icons/left_chevron.svg';
import DeliveryCarIcon from '../../assets/icons/delivery.svg';
import ChevronRightWhiteIcon from '../../assets/icons/chevron_right_white.svg';
import ContactsIcon from '../../assets/icons/contacts.svg';
import CancelIcon from '../../assets/icons/cancel.svg';
import ShareIcon from '../../assets/icons/share.svg'
import TimeIcon from '../../assets/icons/time.svg'
import TrashIcon from '../../assets/icons/trash.svg'
import ChkBoxActiveIcon from '../../assets/icons/checkBoxActive.svg'


// const icon = {
//   width: 24,
//   height: 24,
// };

// type IconType = {
//   fill?: string;
//   size?: number;
// }

// export const ArrowRightIcon: React.FC<IconType> = ({ fill = '#002', size = icon.width }) => {
//   return (
//     <ArrowRight height={size} width={size} fill={fill} />
//   );
// }

export {
  ArrowRightIcon, 
  FavoriteIcon, 
  FavoriteFilledIcon, 
  EventIcon, 
  EventFilledIcon, 
  HomeIcon, 
  HomeFilledIcon,
  ProfileIcon,
  ProfileFilledIcon,
  MyOrdersIcon,
  MyOrdersFilledIcon,
  NotificationIcon,
  FilterIcon,
  SearchIcon,
  ArrowRightDarkIcon,
  LikeIcon,
  LikeFilledIcon,
  StarIcon,
  PlusIcon,
  MinusIcon,
  RightChevronIcon,
  BonusStarIcon,
  LeftChevronIcon,
  DeliveryCarIcon,
  ChevronRightWhiteIcon,
  ContactsIcon,
  CancelIcon,
  ShareIcon,
  TimeIcon,
  TrashIcon,
  ChkBoxActiveIcon
}
