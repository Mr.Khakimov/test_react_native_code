import React, { useState } from 'react';
import { View, Image, StyleSheet, ImageSourcePropType, Pressable } from 'react-native';
import { width } from '../utils/constants';
import { MainStyle, constant, lightTheme } from '../utils/theme';
import IconRoundedButton from './Buttons/IconRoundedButton';
import { LikeFilledIcon, LikeIcon, StarIcon } from './icons';
import TagComponent from './TagComponent.tsx';
import { useTheme } from '../customHooks/ThemeHook';
import CustomTextComponent from './CustomTextComponent';
import { useLocalization } from '../customHooks/LocalizationHook';

interface CardItemProps {
  id: string | number,
  title: string;
  content: string;
  freeDelivery?: boolean,
  isOpen?: boolean,
  isFavorite?: boolean,
  reviews: number,
  stars: number,
  reviewCount: number | string;
  imageSource: string | ImageSourcePropType;
  containerWidth?: number | string,
  rate: string;
  handleItem: (id: string | number) => void,
  handleFavorite: (id: string | number) => void,
}

const CardItem: React.FC<CardItemProps> = ({id, title, rate, reviewCount, content, imageSource, containerWidth = 310, isFavorite, freeDelivery, isOpen, reviews, stars, handleItem, handleFavorite}) => {
  const {t} = useLocalization();

  const {theme} = useTheme();

  const [liked, setLiked] = useState(false);

  return (
    <View style={[styles.container, {width: containerWidth}]}>
      <Pressable onPress={() => handleItem(id)}>
        <Image source={ typeof imageSource === 'number' ? imageSource : {uri: imageSource}} style={styles.image} />
        {
          freeDelivery ? 
            <TagComponent 
              style={styles.tagDelivery} 
              textType="bold"  
              text={t("delivery")} 
              backgroundColor={theme.green} 
              textColor="white"
            />
          : null
        }

        <View style={styles.textContainer}>

          <View style={[MainStyle.flexRow,  MainStyle.jsb, MainStyle.aic]}>
              <CustomTextComponent 
                style={MainStyle.mr1}
                textType="bold"
                size="large"
                color={'dark'}>{title}
              </CustomTextComponent> 

              <TagComponent 
                textType="bold" 
                text={isOpen ? t("open") : t("close")}
                backgroundColor={isOpen ? theme.green : theme.red}
                textColor="white" />
          </View>

          <CustomTextComponent 
            style={MainStyle.mb1} 
            textType="bold" 
            size="small" 
            color={'gray'}>
              {content}
            </CustomTextComponent>   

          <View style={[MainStyle.flexRow, MainStyle.aic]}>
              <StarIcon />
              <CustomTextComponent 
                style={MainStyle.mh1} 
                textType="bold" 
                size="small"
                color={'dark'}>
                  {rate}
              </CustomTextComponent>   
              <TagComponent 
                text={reviewCount + " отзывов"}
                backgroundColor={theme.grey3}
                size='small'
                textType='regular'
                textColor="dark" />
          </View>
        </View>
      </Pressable>
      
      <IconRoundedButton 
        style={styles.likeBtn} 
        onPress={() => {
          handleFavorite(id)
          setLiked(!liked)
        }}
        size="small">
        {!liked ? <LikeIcon /> : <LikeFilledIcon />}
      </IconRoundedButton>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 16,
    borderWidth: 1,
    borderColor: lightTheme.grey3,
    marginRight: constant.space1,
    overflow: 'hidden',
  },
  image: {
    width: '100%',
    objectFit: 'cover',
    height: width / 2.5,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: lightTheme.grey3,
  },
  textContainer: {
    padding: constant.space2,
    
  },
  likeBtn: {
    position: 'absolute',
    top: constant.space2,
    right: constant.space2,
  },
  tagDelivery: {
    position: 'absolute',
    top: constant.space2,
    left: constant.space2,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  content: {
    fontSize: 14,
  },
});

export default CardItem;