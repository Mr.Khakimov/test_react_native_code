import React from 'react';
import { View, StyleSheet, ImageSourcePropType, ActivityIndicator } from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';

interface FullLoadingProps {
  imageSource: ImageSourcePropType;
  text: string;
  title: string,
  buttonText: string;
  onButtonPress: () => void;
}

const FullLoading: React.FC<FullLoadingProps> = () => {
    const {theme} = useTheme();
  return (
    <View style={styles.container}>
        <View style={[styles.inner, {backgroundColor: theme.textWhite}]}>
            <ActivityIndicator size="large" color={theme.primaryColor} />
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0, 0.4)'
  },
  inner: {
    width: 90,
    height: 90,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default FullLoading;
