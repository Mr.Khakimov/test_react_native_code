import React from 'react';
import { View, Image, StyleSheet, ImageSourcePropType } from 'react-native';
import CustomTitle from '../Text/CustomTitle';
import CustomTextComponent from '../CustomTextComponent';
import { MainStyle, constant } from '../../utils/theme';
import CustomButton from '../Buttons/CustomButton';

interface EmptyLayoutProps {
  imageSource: ImageSourcePropType;
  text: string;
  title: string,
  buttonText: string;
  onButtonPress: () => void;
}

const EmptyLayout: React.FC<EmptyLayoutProps> = ({
  imageSource,
  title,
  text,
  buttonText,
  onButtonPress,
}) => {
  return (
    <View style={styles.container}>
        <View style={styles.imageWrapper}>
            <Image source={imageSource} style={styles.image} />
        </View>
        <CustomTitle style={MainStyle.mb1}>{title}</CustomTitle>
        <CustomTextComponent
            textType="regular"
            size="medium"
            color="gray"
            style={[MainStyle.mb3, MainStyle.tac]}>
                {text}
        </CustomTextComponent>
        <View style={[MainStyle.w100, MainStyle.ph3]}>
            <CustomButton onPress={onButtonPress} text={buttonText} />
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: constant.space3,
  },
  imageWrapper: {
    width: 160,
    height: 160,
    marginBottom: constant.space3,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    marginBottom: constant.space3,
  }
});

export default EmptyLayout;
