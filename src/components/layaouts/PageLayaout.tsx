import React, { FC, ReactElement, ReactNode } from "react"
import { useTheme } from "../../customHooks/ThemeHook"
import { SafeAreaView } from "react-native";

interface EmptyLayoutProps {
    children: ReactNode; // Replace with the appropriate image source type
}

const PageLayout:React.FC<EmptyLayoutProps> = ({children}) => {
    const {theme} = useTheme();
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: theme.bgColor}}>
            {children}
        </SafeAreaView>
    )
}
export default PageLayout;