import React, { ReactNode } from 'react';
import { View, StyleSheet, TextStyle, ViewStyle } from 'react-native';
import CustomTitle from '../Text/CustomTitle';
import { constant } from '../../utils/theme';
import IconButton from '../Buttons/IconButton';

interface HeaderProps {
  leftIcon?: ReactNode;
  rightIcon?: ReactNode;
  title?: string;
  titleStyle?: TextStyle | TextStyle[],
  onLeftIconPress?: () => void;
  onRightIconPress?: () => void;
  leftIconBg?: string;
  rightIconBg?: string;
  style?: ViewStyle | ViewStyle[]
}

const HeaderComponent: React.FC<HeaderProps> = ({
  leftIcon,
  rightIcon,
  title,
  style,
  titleStyle,
  leftIconBg = "default",
  rightIconBg = "default",
  onLeftIconPress = () => {},
  onRightIconPress = () => {},

}) => {
  return (
    <View style={[styles.container, style]}>
        
          {
            leftIcon ? 
            <View style={styles.iconWrapper}>
              <IconButton onPress={onLeftIconPress} type={leftIconBg} size="small">
                {leftIcon}
              </IconButton>
            </View>
            : null
          }
       

        <View style={[styles.textStyle, {paddingHorizontal: leftIcon ? constant.space2 : 0}]}>
          <CustomTitle style={titleStyle}>{title}</CustomTitle>
        </View>
        

        <View style={styles.iconWrapper}>
          {
            rightIcon ? 
              <IconButton onPress={onRightIconPress} type={rightIconBg} size="small">
                {rightIcon}
              </IconButton> 
            : null
          }
        </View>
        
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 56,
    paddingTop: 12,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: constant.space3,
    paddingBottom: constant.space2,
  },
  iconContainer: {
    padding: 4,
    width: 30,
    marginHorizontal: constant.space2,
  },
  iconWrapper: {
    width: 30,
  },
  textStyle: {
    textAlign: 'center',
    flex: 1
  },
});

export default HeaderComponent;