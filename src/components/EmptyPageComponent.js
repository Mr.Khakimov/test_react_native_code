import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { EmptyIcon } from "./icons";

const EmptyPage = ({ title, icon }) => {
  return (
    <View style={styles.container}>
      {icon ? icon : <EmptyIcon size={96} />}
      <Text style={styles.warningText}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  warningText: {
    fontSize: 16,
    color: '#0e0e0e',
    marginTop: 20,
  },
});

export default EmptyPage;
