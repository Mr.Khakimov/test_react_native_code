import React, {ReactNode, useMemo, FC} from 'react';
import {StyleSheet, Text, TextStyle} from 'react-native';
import {useTheme} from "../../customHooks/ThemeHook";
import { constant } from '../../utils/theme';

type CustomTextProps = {
    style?: TextStyle | TextStyle[],
    children?: ReactNode,
    type?: 'section' | 'title' | 'extra',
}

const CustomTitle:FC<CustomTextProps> = ({ children, style, type = 'section'}) => {
    const {theme} = useTheme();


    const memoStyle = useMemo(() => {

        if(type === 'section') {
            return styles.section
        }

        if(type === 'title') {
            return styles.title
        }

        if(type === 'extra') {
            return styles.extra
        }

        return {}
    }, [type])


    return (
        <Text style={[styles.text, memoStyle, style, {color: theme.textDark}]}>{children}</Text>
    );
}

const styles = StyleSheet.create({
    text: {
        fontFamily: constant.mPlusBold,
    },
    section: {
        fontSize: constant.titleSmall,
        lineHeight: constant.lineHeightSmall
    },
    title: {
        fontSize: constant.titleMedium,
        lineHeight: constant.lineHeightMedium
    },
    extra: {
        fontSize: constant.titleExtra,
        lineHeight: constant.lineHeightExtra
    }
})

export default CustomTitle;