import React, {FunctionComponent, memo, ReactNode} from 'react';
import {StyleSheet, Text, TextStyle} from 'react-native';
import {useTheme} from "../../customHooks/ThemeHook";
import { constant } from '../../utils/theme';

type CustomTextProps = {
    style?: TextStyle | TextStyle[],
    children?: ReactNode,
}

const MegaTitle: FunctionComponent<CustomTextProps> = ({ children, style }) => {
    const {theme} = useTheme();

    return (
        <Text allowFontScaling={false} style={[styles.text, {color: theme.textDark}, style]}>{children}</Text>
    );
}

const styles = StyleSheet.create({
    text: {
        fontFamily: constant.mPlusBold,
        fontSize: constant.titleExtra,
        lineHeight: constant.lineHeightExtra
    }
})

export default MegaTitle;
