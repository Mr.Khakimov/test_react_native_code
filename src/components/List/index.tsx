
import React, { ReactNode } from 'react';
import { View, TouchableOpacity, StyleSheet, TextStyle } from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';
import CustomTextComponent from '../CustomTextComponent';
import { RightChevronIcon } from '../icons';

interface ListItemProps {
  title: ReactNode;
  rightIcon: any; // Replace with the appropriate right icon component or source
  onPress?: () => void;
  children?: ReactNode,
  titleStyle?: TextStyle | TextStyle[];
}

const ListItem: React.FC<ListItemProps> = ({ title, children, rightIcon, titleStyle, onPress }) => {
  const {theme} = useTheme();
  return (
    <TouchableOpacity onPress={onPress} style={{backgroundColor: theme.grey3}}>
      <View style={styles.container}>
        <CustomTextComponent style={[titleStyle, styles.title]} textType="regular" size="large" color="dark">
            {title}
            <CustomTextComponent style={titleStyle} textType="bold" size="large" color="dark">
                {' '}{children}
            </CustomTextComponent>
        </CustomTextComponent>
        {rightIcon ? <RightChevronIcon /> : null}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingRight: 12,
    paddingVertical: 12,
  },
  title: {
    flex: 1,
  },
});

export default ListItem;