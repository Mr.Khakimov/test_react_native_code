import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { ClearIcon, PeopleIcon } from "./icons";
import { Tag } from "./TagComponent";

export const Assigners = () => {
  const [assigners, setAssigners] = useState([]);

  const handleAddAssigner = () => {
    const newAssigner = {
      id: assigners.length + 1,
      name: `Assigner ${assigners.length + 1}`,
      avatar: 'https://example.com/avatar.png', // Replace with actual avatar URL
    };
    setAssigners([...assigners, newAssigner]);
  };

  const handleRemoveAssigner = (id) => {
    const updatedAssigners = assigners.filter((assigner) => assigner.id !== id);
    setAssigners(updatedAssigners);
  };

  const renderAssigners = () => {
    return assigners.map((assigner) => (
      <View key={assigner.id} style={styles.assigner}>
        <Tag>{assigner.name}</Tag>
        <TouchableOpacity
          style={styles.removeButton}
          onPress={() => handleRemoveAssigner(assigner.id)}
        >
          <ClearIcon />
        </TouchableOpacity>
      </View>
    ));
  };

  return (
    <View style={styles.container}>
      {renderAssigners()}
      <TouchableOpacity style={styles.addButton} onPress={handleAddAssigner}>
        {/*<Icon name="plus" size={20} color="white" />*/}
        <Text style={styles.addButtonText}>Add Assigner</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  assigner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 8,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 12,
  },
  assignerText: {
    fontSize: 16,
  },
  removeButton: {
    backgroundColor: 'red',
    borderRadius: 12,
    padding: 8,
  },
  addButton: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'blue',
    borderRadius: 12,
    paddingVertical: 10,
    paddingHorizontal: 16,
    marginTop: 16,
  },
  addButtonText: {
    marginLeft: 8,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
});
