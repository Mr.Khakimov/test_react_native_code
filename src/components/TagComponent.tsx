import React, { ReactNode } from 'react';
import { View, Text, StyleSheet, ViewStyle } from 'react-native';
import CustomTextComponent, { CustomTextProps } from './CustomTextComponent';

interface TagProps extends CustomTextProps {
  text: string | ReactNode;
  backgroundColor: string;
  textColor: 'white' | 'dark';
  style?: ViewStyle | ViewStyle[],
  textSize: string,
}

const TagComponent: React.FC<TagProps> = ({ style = {}, text, textType, textSize, backgroundColor, textColor = 'white', ...rest }) => {
  return (
    <View style={[styles.container, style, { backgroundColor }]}>
      <CustomTextComponent textType={textType} size={textSize} color={textColor}>{text}</CustomTextComponent>    
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 12,
    alignSelf: 'flex-start',
  },
  text: {
    fontSize: 12,
    fontWeight: 'bold',
  },
});

export default TagComponent;