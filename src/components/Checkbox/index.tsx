import React, { ReactNode, useCallback, useMemo, useState } from 'react';
import { View, TouchableOpacity, StyleSheet, ViewStyle } from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';

interface ICheckboxProps {
  children?: ReactNode;
  isChecked: boolean;
  toggleCheckbox?: () => void;
  style?: ViewStyle | ViewStyle[],
}

const Checkbox: React.FC<ICheckboxProps> = ({ children, style, isChecked = false, toggleCheckbox }) => {
  const {theme} = useTheme();

  const memoStyle = useMemo(() => {
    return {
        borderColor: isChecked ? theme.primaryColor : theme.grey,
        backgroundColor: isChecked ? theme.primaryColor : 'transparent'
    }
  }, [isChecked]);

  return (
    <TouchableOpacity onPress={toggleCheckbox}>
      <View style={[styles.checkboxContainer, style]}>
        <View style={[styles.checkbox, memoStyle]} />
        {children ? <View>{children}</View> : null}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkbox: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderRadius: 6,
    marginRight: 8,
  },
  checked: {
    backgroundColor: 'blue'
  },
});

export default Checkbox;