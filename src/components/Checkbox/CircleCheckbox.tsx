import React, { ReactNode, useCallback, useMemo, useState } from 'react';
import { View, TouchableOpacity, StyleSheet, ViewStyle } from 'react-native';
import { useTheme } from '../../customHooks/ThemeHook';

interface ICircleCheckboxProps {
  children?: ReactNode;
  isChecked: boolean;
  toggleCheckbox?: () => void;
  style?: ViewStyle | ViewStyle[],
}

const CircleCheckbox: React.FC<ICircleCheckboxProps> = ({ children, style, isChecked = false, toggleCheckbox }) => {
  const {theme} = useTheme();

  const memoStyle = useMemo(() => {
    return {
        borderColor: isChecked ? theme.textDark : theme.grey3,
        // backgroundColor: isChecked ? theme.textDark : 'transparent'
    }
  }, [isChecked]);

  return (
    <TouchableOpacity onPress={toggleCheckbox}>
      <View style={[styles.checkboxContainer, style]}>
        <View style={[styles.checkbox, memoStyle]} />
        {children ? <View>{children}</View> : null}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkbox: {
    width: 23,
    height: 23,
    borderWidth: 7,
    borderRadius: 20,
    marginRight: 12,
  },
  checked: {
    backgroundColor: 'blue'
  },
});

export default CircleCheckbox;