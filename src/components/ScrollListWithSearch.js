import React, { useState } from 'react';
import { View, Text, FlatList, TextInput, StyleSheet, TouchableOpacity } from 'react-native';

const ScrollListWithSearch = ({ data, activeData }) => {
  const [searchText, setSearchText] = useState('');
  const [filteredData, setFilteredData] = useState(data);

  const handleSearch = (text) => {
    setSearchText(text);
    const filteredItems = data.filter((item) =>
      item.name.toLowerCase().includes(text.toLowerCase())
    );
    setFilteredData(filteredItems);
  };

  const toggleItem = (item) => {
    if (activeData.includes(item)) {
      // Item is already in activeData, remove it
      // setActiveData(activeData.filter((activeItem) => activeItem !== item));
    } else {
      // Item is not in activeData, add it
      // setActiveData([...activeData, item]);
    }
  };

  const renderItem = ({ item }) => {
    const isActive = activeData.includes(item);

    return (
      <TouchableOpacity
        style={[styles.itemContainer, isActive && styles.activeItem]}
        onPress={() => toggleItem(item)}
      >
        <Text style={styles.itemText}>{item.name}</Text>
        <View style={styles.checkboxContainer}>
          {isActive && <Text style={styles.checkmark}>&#10003;</Text>}
        </View>
      </TouchableOpacity>
    );
  };

  const renderEmptyComponent = () => (
    <View style={styles.emptyContainer}>
      <Text style={styles.emptyText}>No items found.</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchInput}
        placeholder="Search..."
        placeholderTextColor={'#002'}
        value={searchText}
        onChangeText={handleSearch}
      />
      <FlatList
        data={filteredData}
        renderItem={renderItem}
        contentContainerStyle={styles.containerFlatlist}
        keyExtractor={(item, index) => item.id}
        ListEmptyComponent={renderEmptyComponent}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
  containerFlatlist: {
    flexGrow: 1,
  },
  searchInput: {
    height: 40,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    color: '#002'
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  itemText: {
    fontSize: 16,
    flex: 1,
  },
  checkboxContainer: {
    width: 24,
    height: 24,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkmark: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  activeItem: {
    backgroundColor: '#eaf2ff',
  },
  emptyContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  emptyText: {
    fontSize: 16,
    color: '#888',
  },
});

export default ScrollListWithSearch;
