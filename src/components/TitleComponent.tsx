import React, {FunctionComponent, memo} from 'react';
import {StyleSheet, Text, TextStyle} from 'react-native';

type CustomTextProps = {
  style?: TextStyle | TextStyle[],
  textType?: 'regular' | 'bold' | 'semiBold',
  size?: 'small' | 'medium' | 'large' | 'extraLarge',
  children?: any,
}

const CustomTitleComponent: FunctionComponent<CustomTextProps> = memo(({ children, textType, size = 'medium', style }) => {

  let textStyle: {}

  switch (textType) {
    case 'regular':
      textStyle = styles.regular
      break
    case 'bold':
      textStyle = styles.bold
      break
    case 'semiBold':
      textStyle = styles.semiBold
      break
    default:
      textStyle = styles.regular
      break
  }

  switch (size) {
    case 'small':
      textStyle = styles.small
      break
    case 'large':
      textStyle = styles.large
      break
    case 'extraLarge':
      textStyle = styles.extraLarge
      break
    default:
      textStyle = styles.regular
      break
  }

  const passedStyles = Array.isArray(style) ? Object.assign({}, ...style) : style


  return (
      <Text style={[textStyle, { ...passedStyles } ]}>{children}</Text>
  );
})

const styles = StyleSheet.create({
  regular: {
    fontFamily: 'Inter-Regular'
  },
  bold: {
    fontFamily: 'Inter-Bold'
  },
  semiBold: {
    fontFamily: 'Inter-SemiBold'
  },
  small: {
    fontSize: 10,
    lineHeight: 12.1
  },
  medium: {
    fontSize: 12,
    lineHeight: 15.6
  },
  large: {
    fontSize: 14,
    lineHeight: 16.94,
  },
  extraLarge: {
    fontSize: 16,
    lineHeight: 18.74,
  },
})

export default CustomTitleComponent;
