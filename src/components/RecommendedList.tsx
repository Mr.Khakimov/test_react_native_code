import { FlatList } from "react-native-gesture-handler";
import CardItem from "./CardItem";
import { flower } from "../utils/images";
import { MainStyle } from "../utils/theme";
import { useNavigation } from "@react-navigation/native";

const RecommendedList: React.FC = ({containerWidth, value, veritcal, data}) => {
    const navigation = useNavigation();
    const handleItem = (id: any) => {
        navigation.navigate('StorePage', {storeId: id});
    }

    const handleFavorite = (id:any) => {
      console.log('handleFavorite product', id); 
    }

  const renderItem = ({ item } :any) => (
    <CardItem
        id={item.id}
        handleItem={handleItem}
        handleFavorite={handleFavorite}
        title={item.title}
        content={item.desc}
        freeDelivery={item.freeDelivery}
        isOpen={item.open}
        imageSource={item.img}
        rate={item.rate}
        reviewCount={item.reviewsCount}
        containerWidth={containerWidth}
     />
  );

  const handleCategoryPress = (categoryId: string) => {
    console.log(`Category ${categoryId} pressed`);
  };

  return (
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        horizontal={!veritcal}
        scrollEventThrottle={100}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={MainStyle.pl2}
      />
  );
};

export default RecommendedList;