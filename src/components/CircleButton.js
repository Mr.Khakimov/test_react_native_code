import React from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';

const CircleButton = ({ onPress, icon, style }) => {
  return (
    <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
      {icon ? (
        <View style={styles.iconContainer}>
          {icon}
        </View>
      ) : null}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    alignItems: 'center',
  },
});

export default CircleButton;
