import React, { useEffect, useImperativeHandle, useRef, useState, useCallback } from 'react';
import { TextInput, StyleSheet, Text, View, Pressable } from "react-native";
import { VisibilityIcon, VisibilityOffIcon } from "./icons";

type TextInputWithFocusProps = {
  label: string;
  placeholder: string;
  value: string;
  onChangeText: (text: string) => void;
  isPassword: boolean;
};

const TextInputWithFocus = React.forwardRef<TextInput, TextInputWithFocusProps>(
  ({ label, isPassword, placeholder, value, onChangeText, ...props }, ref) => {
    const inputRef = useRef<TextInput>(null);
    const [isFocused, setIsFocused] = useState(false);

    const [secureTextEntry, setSecureTextEntry] = useState(isPassword);

    const toggleVisibilityPassword = useCallback(() => {
      setSecureTextEntry(prev => !prev);
    }, []);

    useEffect(() => {
      if (!props?.autoFocus) {
        return;
      }
      if (inputRef.current) {
        inputRef.current.focus();
      }
    }, []);

    useImperativeHandle(ref, () => inputRef.current);

    const handleFocus = () => {
      setIsFocused(true);
    };

    const handleBlur = () => {
      setIsFocused(false);
    };

    return (
      <View style={styles.container}>
        {label?.length ? <Text style={styles.label}>{label}</Text> : null}
        <TextInput
          ref={inputRef}
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
          onFocus={handleFocus}
          onBlur={handleBlur}
          style={[styles.input, isFocused && styles.inputFocused, props?.multiline && styles.multiline]}
          secureTextEntry={secureTextEntry}
          {...props}
        />
        {
          isPassword ? <Pressable onPress={toggleVisibilityPassword} style={styles.passwordEye}>
            {!secureTextEntry ? <VisibilityIcon /> : <VisibilityOffIcon />}
          </Pressable> : null
        }

      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginBottom: 12,
  },
  label: {
    marginBottom: 5,
    fontWeight: 'bold',
  },
  input: {
    height: 48,
    borderColor: 'gray',
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 8,
    color: '#002'
  },
  multiline: {
    height: "auto",
  },
  inputFocused: {
    borderColor: 'blue',
    borderWidth: 2,
  },
  passwordEye: {
    position: 'absolute',
    bottom: 13,
    right: 18,
  },
});

export default TextInputWithFocus;
