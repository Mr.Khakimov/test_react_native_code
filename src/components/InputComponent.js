import React, { useState } from "react";
import { View, Text, TextInput, Button, StyleSheet } from "react-native";
import CircleButton from "./CircleButton";
import { DoneIcon, EditIcon, MailIcon } from "./icons";

export const InputComponent = ({ value, placeholder, multiline, onInputChange }) => {
  const [inputValue, setInputValue] = useState(value);
  const [isEditing, setIsEditing] = useState(false);

  const handleInputChange = (text) => {
    setInputValue(text);
  };

  const handleEditClick = () => {
    setIsEditing(true);
  };

  const handleSaveClick = () => {
    setIsEditing(false);
    onInputChange(inputValue);
  };

  return (
    <View style={styles.container}>
      {isEditing ? (
        <TextInput
          style={styles.input}
          placeholder={placeholder}
          placeholderTextColor={'#002'}
          value={inputValue}
          onChangeText={handleInputChange}
          multiline={multiline}
        />
      ) : (
        <>
          <Text style={styles.input}>{inputValue}</Text>
          <View style={styles.btn}><CircleButton icon={<EditIcon />} title="Edit" onPress={handleEditClick} /></View>
        </>
      )}
      {isEditing && (
        <View style={styles.btn}><CircleButton icon={<DoneIcon />} title="Save" onPress={handleSaveClick} /></View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: "relative",
  },
  input: {
    fontSize: 16,
    marginBottom: 10,
    padding: 0,
    paddingRight: 50,
    color: "#002",
    lineHeight: 18,
    minHeight: 50,
  },
  btn: {
    position: 'absolute',
    bottom: 10,
    right: 0,
  }

});
