import { View } from "react-native"
import { MainStyle } from "../utils/theme"

export const Divider = ({}) => {
    return (
        <View style={[MainStyle.divider, MainStyle.mh3]}></View>    
    )
}