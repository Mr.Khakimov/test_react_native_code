import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {MainScreen} from '../pages';
import {Pressable} from 'react-native';
import {ExitIcon} from '../components/icons';
import DrawerContent from "./components/DrawerContent";
import {isIos} from "../utils/constants";

const Drawer = createDrawerNavigator();

export const DrawerNavigation = () => {
  return (
    <Drawer.Navigator initialRouteName="Home" drawerContent={DrawerContent}>
      <Drawer.Screen
        name="Home"
        component={MainScreen}
        options={{
          headerTitle: 'Doskalar',
          headerStatusBarHeight: isIos ? 40 : 0,
        }}
      />
    </Drawer.Navigator>
  );
};
