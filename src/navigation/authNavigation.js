import React from 'react';
import { AuthScreen, SmsCodeScreen, WelcomeScreen} from "../pages";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

const noHeaderOptions = {
    header: () => null,
}

export const AuthNavigation = () => {
  return (
    <Stack.Navigator initialRouteName={'AuthScreen'}>
      <Stack.Screen name="AuthScreen" component={AuthScreen} options={noHeaderOptions} />
      <Stack.Screen name="SmsCodeScreen" component={SmsCodeScreen} options={noHeaderOptions} />
    </Stack.Navigator>
  )
}
