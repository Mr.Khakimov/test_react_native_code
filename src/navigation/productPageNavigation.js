import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {MainScreen,  ProductScreen, StoreScreen, ReviewsScreen, StoreCatalogScreen, ShopsScreen} from '../pages';
import { noHeaderOptions } from './options';

const Stack = createNativeStackNavigator();

export const MainProductNavigation = () => {
  return (
    <Stack.Navigator initialRouteName='MainPage'>
      <Stack.Screen name="MainPage" component={MainScreen} options={noHeaderOptions} />
      <Stack.Screen name="ProductPage" component={ProductScreen} options={noHeaderOptions} />
      <Stack.Screen name="StorePage" component={StoreScreen} options={noHeaderOptions} />
      <Stack.Screen name="Reviews" component={ReviewsScreen} options={{...noHeaderOptions, presentation: 'modal'}} />
      <Stack.Screen name="StoreCatalog" component={StoreCatalogScreen} options={{...noHeaderOptions}} />
      <Stack.Screen name="ShopsScreen" component={ShopsScreen} options={noHeaderOptions} />
    </Stack.Navigator>
  );
};