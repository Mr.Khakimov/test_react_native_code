import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {EventsCreateScreen, SettingsScreen, WelcomeScreen, BasketScreen, SmsCodeScreen, SearchScreen, ReviewsScreen, ProductInnerScreen, ShopsScreen, CommentScreen, UsePromocodeScreen, DeliveryScreen} from '../pages';
import {NotificationsScreen} from '../pages/Notifications';
import {AboutScreen} from '../pages/About';
import { TabNavigator } from './tabNavigation';
import { noHeaderOptions } from './options';
import { useAppSelector } from '../redux/store';
import {MainStackParamList} from "./navigationTypes";

const Stack = createNativeStackNavigator<MainStackParamList>();

export const MainNavigation = () => {
  const onBoardingShow = useAppSelector((state) => state.settings.onboardingShow);

  return (
    <Stack.Navigator initialRouteName={onBoardingShow ? 'WelcomePage' : 'Main'}>
      <Stack.Screen name="WelcomePage" component={WelcomeScreen}  options={noHeaderOptions}/>
      <Stack.Screen name="Main" component={TabNavigator} options={noHeaderOptions} />
      <Stack.Screen name="BasketScreen" component={BasketScreen} options={noHeaderOptions} />
      <Stack.Screen name="CommentScreen"
                    component={CommentScreen}
                    options={noHeaderOptions}
      />
      <Stack.Screen name="UsePromoCodeScreen" component={UsePromocodeScreen} options={noHeaderOptions} />
      <Stack.Screen name="DeliveryScreen" component={DeliveryScreen} options={noHeaderOptions} />
      <Stack.Screen name="EventCreateScreen" component={EventsCreateScreen} options={noHeaderOptions} />

      {/*<Stack.Screen name="ProductInfoPage" component={ProductInnerScreen} options={{...noHeaderOptions, presentation: 'modal'}} />*/}
      <Stack.Screen name="Notification" component={NotificationsScreen} />

      {/*<Stack.Screen name="Search" component={SearchScreen} options={{...noHeaderOptions}} />*/}
      <Stack.Screen name="About" component={AboutScreen} />
      <Stack.Screen name="Settings" component={SettingsScreen} />
    </Stack.Navigator>
  );
};
