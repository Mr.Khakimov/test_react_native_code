import {NativeStackScreenProps} from "@react-navigation/native-stack";
import {BasketAddToOrder} from "../pages/Basket/BasketAddToOrder";

export type MainStackParamList = {
    WelcomePage: undefined;
    Main: undefined;
    EventCreateScreen: undefined;
    AuthScreen: undefined;
    SmsCodeScreen: undefined;
    Notification: undefined;
    About: undefined;
    Settings: undefined;
    Company: undefined;
    BasketScreen: undefined;
    ShopsScreen: undefined;
    CommentScreen: { title: string, data?: any } | undefined;
    UsePromoCodeScreen: undefined;
    DeliveryScreen: undefined;
};

export type CommentScreenProps = NativeStackScreenProps<MainStackParamList, 'CommentScreen'>;
export type BasketAddToOrderProps = NativeStackScreenProps<MainStackParamList, 'BasketScreen'>;
export type StackNavigation = NativeStackScreenProps<MainStackParamList>;
