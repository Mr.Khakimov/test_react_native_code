import React, { useState } from "react";
import { View, TextInput, TouchableOpacity, Text, StyleSheet } from "react-native";
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withTiming,
  Easing,
  interpolate,
  interpolateColor,
} from "react-native-reanimated";
import { DoneIcon, FilterIcon } from "../../components/icons";

const TaskFilterComponent = ({ onFilter }) => {
  const [searchVisible, setSearchVisible] = useState(false);
  const translateX = useSharedValue(200); // Initial translation value for slide animation

  const handleFilter = () => {
    // Implement your filter logic here
    onFilter();
    toggleSearchVisibility();
  };

  const toggleSearchVisibility = () => {
    setSearchVisible(!searchVisible);
    translateX.value = searchVisible ? 200 : 0; // Animate the translation value
  };

  const animatedContainerStyle = useAnimatedStyle(() => {
    const opacity = interpolate(translateX.value, [0, 200], [1, 0]); // Fade out the input during slide animation
    const backgroundColor = interpolateColor(
      translateX.value,
      [0, 200],
      ["rgba(255,255,255,1)", "rgba(255,255,255,0)"] // Fade out the background color during slide animation
    );

    return {
      transform: [{ translateX: translateX.value }],
      opacity: opacity,
      backgroundColor: backgroundColor,
    };
  });

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.filterButton} onPress={toggleSearchVisibility}>
        <FilterIcon />
      </TouchableOpacity>
      <Animated.View style={[styles.inputContainer, animatedContainerStyle]}>
        <TextInput
          style={styles.input}
          placeholder="Search tasks..."
        />
        <TouchableOpacity style={styles.addButton} onPress={handleFilter}>
          <DoneIcon />
        </TouchableOpacity>
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  filterButton: {
    alignSelf: "flex-end",
  },
  inputContainer: {
    position: "absolute",
    top: 0,
    right: 0,
    left: 0,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    flex: 1,
    backgroundColor: "#ffffff",
    borderRadius: 8,
    padding: 12,
    marginBottom: 8,
    borderWidth: 1,
    borderColor: "#ccc",
  },
  addButton: {
    backgroundColor: "#4CAF50",
    borderRadius: 8,
    paddingVertical: 12,
    paddingHorizontal: 16,
    alignItems: "center",
    marginLeft: 8,
  },
  addButtonLabel: {
    color: "#ffffff",
    fontWeight: "bold",
  },
});

export default TaskFilterComponent;
