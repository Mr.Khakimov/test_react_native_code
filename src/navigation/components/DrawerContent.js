import React from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { AvatarIcon, CompanyIcon, NotificationIcon, SettingsIcon, VerifiedIcon } from "../../components/icons";
import { useNavigation } from "@react-navigation/native";
import { ExitComponent } from "./ExitComponent";
import { ListItemWithIcon } from "../../components/ListItemWithIcon";
import { isIos } from "../../utils/constants";

const links = [
  { name: 'Yangiliklar', route: 'Notification', icon: <NotificationIcon /> },
  { name: 'Kompaniyalar', route: 'Company', icon: <CompanyIcon /> },
  { name: 'Sozlamalar', route: 'Settings', icon: <SettingsIcon /> },
  { name: 'Dastur Haqida', route: 'About', icon: <VerifiedIcon /> },
];

const DrawerContent = () => {
  const navigation = useNavigation();

  const renderLink = (link) => {
    return (
      <View key={link.name}>
        <ListItemWithIcon onPress={() => navigation.navigate(link.route)} leftContent={link.icon} title={link.name} />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={styles.avatar}>
          <AvatarIcon size={50} />
        </View>
        <Text style={styles.userName}>John Doe</Text>
      </View>

      <ScrollView contentContainerStyle={styles.linksContainer}>
        {links.map((link) => renderLink(link))}

        <View style={styles.bottom}>
          <ExitComponent />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: isIos ? 45 : 12,
    backgroundColor: '#fff',
    height: '100%'
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingBottom: 12,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  userName: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  bottom: {
    marginTop: 'auto',
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  },
  linksContainer: {
    flexGrow: 1,
    paddingBottom: 10, // Add padding to the bottom of the links container
  },
  linkContainer: {
    marginBottom: 10,
    height: 50,
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  linkText: {
    fontSize: 18,
    color: '#000',
    fontWeight: 'bold',
  },
});

export default DrawerContent;
