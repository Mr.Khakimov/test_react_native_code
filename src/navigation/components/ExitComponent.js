import React from 'react';
import {Alert} from 'react-native';
import {useAppDispatch} from '../../redux/store';
import {setUserLogout} from '../../redux/slices/userSlice';
import {ListItemWithIcon} from '../../components/ListItemWithIcon';
import { ExitIcon } from "../../components/icons";

const color = '#d2103d';
export const ExitComponent = () => {
  const dispatch = useAppDispatch();
  const showAlert = () =>
    Alert.alert('Tasdiqlash', 'Profildan chiqishni hohlaszimi ? ', [
      {
        text: 'Bekor qilish',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'Chiqish', onPress: handleLogout},
    ]);

  const handleLogout = () => {
    dispatch(setUserLogout());
  };

  return (
    <ListItemWithIcon
      onPress={showAlert}
      leftContent={<ExitIcon fill={color} />}
      titleStyle={{color}}
      title={'Chiqish'}
    />
  );
};
