import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { FavoritesScreen, MyOrdersScreen, EventsScreen, ProfileScreen} from '../pages';
import { noHeaderOptions } from "./options";
import { EventFilledIcon, EventIcon, FavoriteFilledIcon, FavoriteIcon, HomeFilledIcon, HomeIcon, MyOrdersFilledIcon, MyOrdersIcon, ProfileFilledIcon, ProfileIcon } from "../components/icons";
import { useTheme } from "../customHooks/ThemeHook";
import { StyleSheet } from "react-native";
import { constant } from "../utils/theme";
import { useLocalization } from "../customHooks/LocalizationHook";
import { MainProductNavigation } from "./productPageNavigation";
import { EventNavigation } from "./eventNavigation";

const Tab = createBottomTabNavigator();

export const TabNavigator = () => {
    const {theme} = useTheme()
    const {t} = useLocalization();
    return (
        <Tab.Navigator initialRouteName="Home" screenOptions={({ route }) => ({
                tabBarIcon: ({ focused }) => {
                    switch (route.name) {
                        case 'Home':
                            return focused ? <HomeFilledIcon /> : <HomeIcon />
                        case 'Favorites':
                            return focused ? <FavoriteFilledIcon /> : <FavoriteIcon />
                        case 'Event':
                            return focused ? <EventFilledIcon /> : <EventIcon />
                        case 'MyOrders':
                            return focused ? <MyOrdersFilledIcon /> : <MyOrdersIcon />
                        default:
                            return focused ? <ProfileFilledIcon /> : <ProfileIcon />
                    }
                },
                tabBarAllowFontScaling: false,
                tabBarLabelStyle: style.label,
                tabBarActiveTintColor: theme.textDark,
                tabBarInactiveTintColor: theme.textGrey,
            })}
            >
            <Tab.Screen name="Home" component={MainProductNavigation} options={{...noHeaderOptions, title: t('main')}} />
            <Tab.Screen name="Favorites" component={FavoritesScreen} options={{...noHeaderOptions, title: t('favorites')}} />
            <Tab.Screen name="Event" component={EventNavigation} options={{...noHeaderOptions, title: t('event')}} />
            <Tab.Screen name="MyOrders" component={MyOrdersScreen} options={{...noHeaderOptions, title: t('myOrders')}} />
            <Tab.Screen name="Profile" component={ProfileScreen} options={{...noHeaderOptions, title: t('profile')}} />
        </Tab.Navigator>
    )
}

const style = StyleSheet.create({
    label: {
        fontFamily: constant.interRegular,
        fontSize: constant.textSmall,
        lineHeight: constant.textSmallLineHeight
    }
});