import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {MainScreen,  ProductScreen, ProductInnerScreen, StoreScreen, EventsScreen, EventsCreateScreen} from '../pages';
import { noHeaderOptions } from './options';

const Stack = createNativeStackNavigator();

export const EventNavigation = () => {
  return (
    <Stack.Navigator initialRouteName='EventScreen'>
      <Stack.Screen name="EventScreen" component={EventsScreen} options={noHeaderOptions} />
      {/* <Stack.Screen name="EventCreateScreen" component={EventsCreateScreen} options={noHeaderOptions} /> */}
    </Stack.Navigator>
  );
};