import {configureStore, combineReducers} from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useSelector, useDispatch} from "react-redux";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistReducer,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE,
} from "redux-persist";
import reduxStorage from "./storage";
import themeReducer from "./slices/themeSlice";
import counterReducer from "./slices/counterSlice";
import todosReducer from "./slices/todosSlice";
import userReducer from "./slices/userSlice";
import settingsReducer from "./slices/settingsSlice";
import registerReducer from "./slices/registerSlice";
import basketReducer from "./slices/basketSlice";

const rootReducer = combineReducers({
  theme: themeReducer,
  count: counterReducer,
  todos: todosReducer,
  user: userReducer,
  settings: settingsReducer,
  register: registerReducer,
  basket: basketReducer,
});

const persistConfig = {
  key: "root",
  version: 1,
  storage: reduxStorage,
  timeout: 0,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);

export const useAppDispatch = () => useDispatch();
export type RootState = ReturnType<typeof rootReducer>;


export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export default store;
