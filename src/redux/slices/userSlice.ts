import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import api from '../../services/api';
import { endpoints } from '../../services/endpoints';
import { AxiosError } from 'axios';
import { setIsScreen, setIsScreenLoginModal } from './registerSlice';

export const userNewRegister = createAsyncThunk('user/userNewRegister', async (data, thunkApi) => {  
  try {
    if(data?.is_client) {
      thunkApi.dispatch(setIsScreen(null));

      return thunkApi.fulfillWithValue(data)
    } else {
      const response = await api.post(endpoints.registerUser, {
          phone: data?.phone,
          given_names: "",
          sur_name: "",
          birthday: "",
          key: data?.key
      })
      thunkApi.dispatch(setIsScreen(null));
      return thunkApi.fulfillWithValue(response.data.result)
    }
  } catch (error) {
    const err = error as AxiosError
    return thunkApi.rejectWithValue(err.response?.data || err.message)
  }
})

const UserSlice = createSlice({
  name: 'user',
  initialState: {
    user: null,
    error: '',
    logged: false,
    loading: false,
    token: null,
    expiresIn: null,
  },
  reducers: {
    setUserLogout: state => {
      state.user = null;
      state.logged = false;
      state.token = null;
    },
    setUserClearFields: state => {
      state.user = null;
      state.token = null;
      state.error = '';
      state.logged = false;
    },
  },
  extraReducers: builder => {
    builder
      .addCase(userNewRegister.pending, (state, action) => {
        state.loading = true
        return state
      })
      .addCase(userNewRegister.fulfilled, (state, action) => {  
        state.loading = false;
        state.token = action.payload?.access_token;
        state.tokenType = action.payload?.token_type;
        state.expiresIn = action.payload?.expires_in;
        state.user = action.payload?.client;
        return state
      })
      .addCase(userNewRegister.rejected, (state, action) => {  
        if(action.payload?.code === 401) {
          state.error = 'smsGetUser'
        }

        if(action.payload?.code === 500) {
          state.error = 'serverError'
        }

        if(action.payload?.code === undefined) {
          state.error = 'smsGetUser'
        }
        return state
      })
      // .addCase(getUser.pending, (state, action) => {
      //   state.loading = true;
      // })
      // .addCase(getUser.fulfilled, (state, action) => {
      //   state.loading = false;
      //   state.user = action.payload;
      // })
      // .addCase(getUser.rejected, (state, action) => {
      //   state.loading = false;
      //   state.error = action.payload?.message;
      // })
      // .addCase(userLogout.pending, (state, action) => {
      //   state.loading = true;
      // })
      // .addCase(userLogout.fulfilled, (state, action) => {
      //   state.loading = false;
      //   state.user = null;
      //   state.logged = false;
      // })
      // .addCase(userLogout.rejected, (state, action) => {
      //   state.loading = false;
      //   state.error = action.payload?.message;
      // });
  },
});

export const {setUserLogout, setUserClearFields} = UserSlice.actions;

export default UserSlice.reducer;
