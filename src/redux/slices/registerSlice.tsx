import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import api from '../../services/api';
import { endpoints } from "../../services/endpoints";
import { AxiosError } from "axios";
import { userNewRegister } from "./userSlice";

type RegisterState = {
  isScreen: "auth" | "sms" | null,
  phone: string;
  policySelect: boolean;
  btnDisable: boolean;
  btnLoading: boolean;
  registerResult: any;
  error: string | any;
  timeOffset: number | null;
  smsLoading: boolean;
  smsError: string | null
};

const initialState: RegisterState = {
  isScreen: null,
  phone: '',
  btnDisable: true,
  policySelect: false,
  btnLoading: false,
  registerResult: null,
  error: '',
  timeOffset: null,
  smsLoading: false,
  smsError: null
};

export const registerSendSms = createAsyncThunk('register/sendSms', async (data, thunkApi) => {
    try {
        const response = await api.post(endpoints.sendSms, data?.data)
        return thunkApi.fulfillWithValue(response.data.result)
    } catch (error) {
        const err = error as AxiosError
        console.log('registerSendSms', error);
        
        return thunkApi.rejectWithValue(err.response?.data || err.message)
    }
})

export const registerConfirmSms = createAsyncThunk('register/confirmSms', async (data, thunkApi) => {
    try {
        console.log('data.data', data.data);
    

        const response = await api.post(endpoints.confirmSms, data.data)
         
        thunkApi.dispatch(userNewRegister({...data.data, ...response.data.result}));
        
        return thunkApi.fulfillWithValue(response.data.result)
    } catch (error) {
        console.log('registerConfirmSms', error);
        const err = error as AxiosError
        return thunkApi.rejectWithValue(err.response?.data || err.message)
    }
})

const registerSlice = createSlice({
  name: "register",
  initialState,
  reducers: {
    setPolicySelect: (state) => {
        state.policySelect = !state.policySelect;
        state.btnDisable = true  

        if(state.policySelect){
            if (state.phone.length === 11) {
                state.btnDisable = false 
            }
        }
        return state
    },
    setPhone: (state, action) => {
        state.phone = action.payload;
        state.btnDisable = true 

        if(state.policySelect) {
            if (state.phone.length === 11) {
                state.btnDisable = false 
            }
        }
        return state
    },
    setBtnDisable: (state, action) => {
        state.btnDisable = action.payload;
        return state
    },
    setBtnLoading: (state, action) => {
        state.btnLoading = action.payload;
        return state
    },
    setRegisterClear: (state) => {        
        state = initialState
        state.isScreen = null
        return state
    },
    setTimerOffset: (state) => {
        state.timeOffset = null
        return state
    },
    setIsScreen: (state, action) => {
        state.isScreen = action.payload
        return state
    },
    setIsScreenLoginModal: (state) => {
        state.isScreen = 'auth'
        return state
    }
  },
  extraReducers: builder => {
    builder
      .addCase(registerSendSms.pending, (state, action) => {
        state.btnLoading = true
        state.btnDisable = true
        return state
      })
      .addCase(registerSendSms.fulfilled, (state, action) => {  
        state.btnLoading = false;
        state.error = ''
        state.btnDisable = false
        state.registerResult = action.payload
        state.timeOffset = new Date().getTime();
        state.smsError = null
        state.isScreen = 'sms'
        console.log('action', action);
        return state
        
      })
      .addCase(registerSendSms.rejected, (state, action) => {  
        state.error = action.payload
        state.btnLoading = false
        state.btnDisable = false
        state.registerResult = null
        state.smsError = null
        return state
      })
      .addCase(registerConfirmSms.pending, (state, action) => {
        state.smsLoading = true
        state.btnDisable = true
        state.smsError = null
        return state
      })
      .addCase(registerConfirmSms.fulfilled, (state, action) => {  
        if(action.payload.isClient) {

        } else {

        }
        state.smsLoading = false
        return state
      })
      .addCase(registerConfirmSms.rejected, (state, action) => {  
        if(action.payload.code === 422) {
            state.smsError = 'smsErrorValidation'
        }
        if(action.payload.code === 500) {
            state.smsError = 'smsErrorServer'
        }

        if(action.payload.code === undefined) {
            state.smsError = 'smsErrorUndefined'
        }
        
        state.smsLoading = false

        return state
      })
  },
});

export const { setPolicySelect,setIsScreen, setBtnDisable, setPhone, setTimerOffset, setRegisterClear, setBtnLoading, setIsScreenLoginModal} = registerSlice.actions;

export default registerSlice.reducer;
