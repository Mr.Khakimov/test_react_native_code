import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface Product {
  id: number;
  name: string;
  shopId: number; // Add the shopId property
}

interface BasketState {
  products: Product[];
}

const initialState: BasketState = {
  products: [],
};

const basketSlice = createSlice({
  name: 'basket',
  initialState,
  reducers: {
    addProduct: (state, action: PayloadAction<Product>) => {
      state.products.push(action.payload);
    },
    removeProduct: (state, action: PayloadAction<number>) => {
      state.products = state.products.filter((product) => product.id !== action.payload);
    },
    clearBasket: (state) => {
      state.products = [];
    },
  },
});

export const { addProduct, removeProduct, clearBasket } = basketSlice.actions;

export default basketSlice.reducer;