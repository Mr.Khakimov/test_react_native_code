import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import API from "../../services/api";

export const createTodos = createAsyncThunk(
  "Todos/createTodos",
  async ({ updatedTodosData, navigate, toast }, { rejectWithValue }) => {
    try {
      const response = await API.post("/Todos", updatedTodosData);
      // toast.success("Added Successfully");
      // navigate("/dashboard");
      return response.data;
    } catch (err) {
      return rejectWithValue(err.response.data);
    }
  }
);


export const getTodoByUser = createAsyncThunk(
  "Todos/getTodoByUser",
  async (userId, { rejectWithValue }) => {
    try {
      const response = await API.get(`/Todos/userTodo/${userId}`);;
      return response.data;
    } catch (err) {
      return rejectWithValue(err.response.data);
    }
  }
);


export const updateTodos = createAsyncThunk(
  "Todos/updateTodos",
  async ({ id, updatedTodosData, toast, navigate }, { rejectWithValue }) => {
    try {
      const response = await API.patch(`/Todos/${id}`, updatedTodosData);
      // toast.success("Todos Updated Successfully");
      // navigate("/dashboard");
      return response.data;
    } catch (err) {
      return rejectWithValue(err.response.data);
    }
  }
);

export const deleteTodos = createAsyncThunk(
  "Todos/deleteTodos",
  async ({ id, toast }, { rejectWithValue }) => {
    try {
      const response = await API.delete(`/Todos/${id}`);
      // toast.success("Todos Deleted Successfully");
      return response.data;
    } catch (err) {
      return rejectWithValue(err.response.data);
    }
  }
);

const Todolice = createSlice({
  name: "todos",
  initialState: {
    todos: {},
    todo: [],
    userTodo: [],
    tagTodo: [],
    relatedTodo: [],
    currentPage: 1,
    numberOfPages: null,
    error: "",
    loading: false,
  },
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(createTodos.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(createTodos.fulfilled, (state, action) => {
        state.loading = false;
        state.todo = [action.payload];
      })
      .addCase(createTodos.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload.message;
      })
      .addCase(getTodoByUser.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(getTodoByUser.fulfilled, (state, action) => {
        state.loading = false;
        state.userTodo = action.payload;
      })
      .addCase(getTodoByUser.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload.message;
      })
      .addCase(updateTodos.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(updateTodos.fulfilled, (state, action) => {
        state.loading = false;
        const {
          arg: { id },
        } = action.meta;
        if (id) {
          state.userTodo = state.userTodo.map((item) =>
            item._id === id ? action.payload : item
          );
          state.Todo = state.Todo.map((item) =>
            item._id === id ? action.payload : item
          );
        }
      })
      .addCase(updateTodos.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload.message;
      })
      .addCase(deleteTodos.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(deleteTodos.fulfilled, (state, action) => {
        state.loading = false;
        const {
          arg: { id },
        } = action.meta;
        if (id) {
          state.userTodo = state.userTodo.filter((item) => item._id !== id);
          state.Todo = state.Todo.filter((item) => item._id !== id);
        }
      })
      .addCase(deleteTodos.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload.message;
      });
  },
});

export const { setCurrentPage } = Todolice.actions;

export default Todolice.reducer;
