import { createSlice, PayloadAction } from '@reduxjs/toolkit';


interface IAction {
    key: string;
    value: any;
}

interface Product {
  id: number | string;
  storeId: number | string;
  count: number;
}

interface DeliveryAddr {
  position: any;
  name: string;
  apartment: string | number;
  floor: number | string;
  entrance: number | string;
}

interface BasketState {
  products: Product[];
  addPostcard: boolean;
  postcardText: string;
  deliveryType: string | number;
  deliveryPerson: 'mine' | 'others';
  deliveryPersonOther: any;
  deliveryComment: string;
  deliveryAddress: DeliveryAddr;
  showBottomLink: boolean;
  [key: string]: any,
}

const initialState: BasketState = {
  products: [],
  addPostcard: false,
  postcardText: "",
  deliveryType: "",
  deliveryPerson: 'mine',
  deliveryPersonOther: {
    name: '',
    phone: null,
  },
  deliveryComment: "",
  deliveryAddress: {
    name: '',
    position: {
      latitude: 41.7235123,
      longitude: 71.1243593,
    },
    apartment: "",
    floor: "",
    entrance: "",
  },
  showBottomLink: true,
};



const basketSlice = createSlice({
  name: 'basket',
  initialState,
  reducers: {
    addProduct: (state, action: PayloadAction<Product>) => {
      state.products.push(action.payload);
      return state
    },

    removeProduct: (state, action: PayloadAction<number>) => {
      state.products = state.products.filter((product) => product.id !== action.payload);
      return state
    },

    updateProduct: (state, action) => {
      const productIndex = state.products.findIndex((product) => product.id === action.payload.id);
      if (productIndex !== -1) {
        state.products[productIndex] = action.payload; // Update the product in the array
      }
      return state
    },

    clearBasket: (state) => {
      state.products = [];
      return state
    },

    updateBasketOptions: (state) => {
      state.products = [];
      return state
    },

    showBottomBasketLink: (state, action:PayloadAction<boolean>) => {
      state.showBottomLink = action.payload
      return state
    },

    setDeliveryPerson: (state, action) => {
      console.log('state.deliveryPersonOther.name', action.payload, state.deliveryPersonOther);

      if(action.payload === 'mine') {
        state.deliveryPersonOther.name = ''
        state.deliveryPersonOther.phone = null
      }
      state.deliveryPerson = action.payload

      return state
    },

    setDeliveryOptions: (state, action: PayloadAction<IAction>) => {
      state[action.payload.key] = action.payload.value
      return state
    }
    // setOptions: (state, action) => {
      // if(action.payload === 'mine') {
      //   state.deliveryPersonOther.name = ''
      //   state.deliveryPersonOther.phone = null
      // }
      // state[] = action.payload
    // }
  },
});

export const {
  addProduct,
  removeProduct,
  clearBasket,
  updateProduct,
  showBottomBasketLink,
  setDeliveryPerson,
  setDeliveryOptions
} = basketSlice.actions;

export default basketSlice.reducer;
