import { createSlice } from '@reduxjs/toolkit';

interface ISettingsState {
  listKanban: boolean;
  onboardingShow: boolean;
}

const initialState: ISettingsState = {
  listKanban: false,
  onboardingShow: true,
};

export const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    tableStyleToggle: (state, action) => {
      state.listKanban = action.payload;
    },
    toggleOnboarding: (state, action) => {
      state.onboardingShow = action.payload;
    },
  },
});

export const { tableStyleToggle, toggleOnboarding } = settingsSlice.actions;

export default settingsSlice.reducer;