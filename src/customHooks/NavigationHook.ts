import { NavigationProp, ParamListBase, useNavigation } from "@react-navigation/native";

type UseAppNavigation = () => NavigationProp<ParamListBase>;

export const useAppNavigation: UseAppNavigation = useNavigation
