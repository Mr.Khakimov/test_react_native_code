import { useMemo } from "react";
import { allData } from "../MOCK/flowersShop";
import { ordersData } from "../MOCK/otherProducts";
import { useAppSelector } from "../redux/store";

export const useFindStore = (id) => {
    const store = allData.find(el => el.id.toString() == id.toString());    
    return store;
};

export const useFindProduct = (id) => {
    const product = ordersData.find(el => el.id.toString() == id.toString());    
    return product;
};

export const useBasketPriceHook = () => {
    const basket = useAppSelector(state => state.basket.products);

    const price = useMemo(() => {
        let pr = 0
        let discount = 0;

        basket.forEach(el => {
            const product = ordersData.find(item => el.id === item.id);
            const price = product?.price
            if(price) {
                pr += (parseFloat(price) * el.count)
            }
        })

        return pr
    }, [basket]);

    const deliveryTime = '40 - 50'

    return {price, deliveryTime}
}

// export const useProductPrice = (id, count) => {
//     const product = ordersData.find(el => el.id.toString() == id.toString());    
//     return product;
// };