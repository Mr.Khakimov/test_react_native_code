const beauty = require('../../assets/images/beauty.png');
const clothes = require('../../assets/images/clothes.png');
const souvenir = require('../../assets/images/souvenir.png');
const toys = require('../../assets/images/toys.png');
const cakes = require('../../assets/images/cakes.png');
const flowers = require('../../assets/images/flowers.png');

const cFlowers = require('../../assets/images/categoryFlowers.png');
const cToys = require('../../assets/images/categoryToys.png');
const cCakes = require('../../assets/images/categoryCake.png');
const cSuv = require('../../assets/images/categorySuv.png');

const flower = require('../../assets/images/flower.png');

const favEmpty = require('../../assets/images/favorite_empty.png');
const evEmpty = require('../../assets/images/events_empty.png');
const mOrderEmpty = require('../../assets/images/my_orders_empty.png');
const profileImage = require('../../assets/images/profile_icon.png');
const bonusStar = require('../../assets/images/bonus_star.png');
const fw_1 = require('../../assets/images/fw_1.png');
const fw_2 = require('../../assets/images/fw_2.png');
const fw_3 = require('../../assets/images/fw_3.png');
const fw_4 = require('../../assets/images/fw_4.png');
const birthDayCake = require('../../assets/images/birthdayCake.png');

const logoPaper = require('../../assets/images/logoPaper.png');



export {
    beauty, clothes, 
    souvenir, toys, cakes, 
    flowers, cFlowers, cToys, 
    cCakes, cSuv, flower,
    favEmpty, evEmpty, mOrderEmpty, profileImage,
    bonusStar, fw_1, fw_2, fw_3, fw_4, birthDayCake,
    logoPaper
}