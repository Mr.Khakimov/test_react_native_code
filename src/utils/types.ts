export interface Localization {
    t: (key: string) => string;
}

export type IObjString = {
    [key: string]: string
}
export interface IObj<T = any>{
    [key: string]: T
}
