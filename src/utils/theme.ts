import { StyleSheet } from "react-native";
import { isIos, width } from "./constants";

export const lightTheme = {
    primaryColor: '#FF3B65',
    primaryColorHover: '#FFBBBB',
    secondaryColor: '#6C757D',
    textDark: '#222222',
    textWhite: '#FFFFFF',
    textGrey: '#D3D3D3',
    textGray: '#8B8B8B',
    gray: '#8B8B8B',
    grey: '#D3D3D3',
    grey3: '#F5F5F5',

    bgColor: '#ffffff',

    pressHover: '#D3D3D3',
    green: '#5EBD62',
    greenHover: "#5EBD62",
    red: '#FF3B65',
    dark: '#222222',
};

export const darkTheme = {
    primaryColor: '#FF3B65',
    primaryColorHover: '#FFBBBB',
    secondaryColor: '#CCCCCC',
    textDark: '#222222',
    textWhite: '#FFFFFF',
    textGrey: '#D3D3D3',
    textGray: '#8B8B8B',
    gray: '#8B8B8B',
    grey: '#D3D3D3',
    grey3: '#F5F5F5',

    bgColor: '#ffffff',

    pressHover: '#D3D3D3',
    green: '#5EBD62',
    greenHover: "#5EBD62",
    red: '#FF3B65',
    dark: '#222222',
    // ...other theme properties
};

export const constant = {
    btnSizeLarge: 60,
    btnSizeSmall: 30,
    btnSizeMedium: 48,

    defaultSize: 48,

    space1: 8,
    space2: 12,
    space3: 16,
    space4: 20,

    interRegular: 'Inter-Regular',
    interMedium: 'Inter-Regular',
    interSemiBold: 'Inter-SemiBold',
    interBold: 'Inter-Bold',

    mPlusBold: isIos ? 'Rounded Mplus 1c Bold' : 'MPLUSRounded1c-Bold',

    textSmall: 10,
    textSmallLineHeight: 12.1,
    textMedium: 12,
    textMediumLineHeight: 15.6,
    textLarge: 14,
    textLargeLineHeight: 16.94,
    textExtra: 16,
    textExtraLineHeight: 18.76,

    textBig: 24,
    textBigLineHeight: 28,

    titleSmall: 20,
    titleMedium: 24,
    titleExtra: 32,

    lineHeightSmall: 29.7,
    lineHeightMedium: 41.6,
    lineHeightExtra: 41.6,
}

export const MainStyle = StyleSheet.create({
    relative: { position: "relative" },
    z1: { zIndex: 1 },
    z2: { zIndex: 2 },
    flex: { flex: 1 },
    flexGrow: {flexGrow: 1},
    flexRow: { flexDirection: "row" },

    contentContainerStyle: { flexGrow: 1 },

    jse: { justifyContent: "flex-end" },
    jsc: { justifyContent: "center" },
    jsb: { justifyContent: "space-between" },

    aic: {alignItems: 'center'},
    ais: {alignItems: 'flex-start'},
    aie: {alignItems: 'flex-end'},

    tac: {textAlign: 'center'},
    taj: {textAlign: 'justify'},
    tar: {textAlign: 'right'},

    tUppercase: {textTransform: 'uppercase'},
    tLowecase: {textTransform: 'capitalize'},

    line: {height: 2, backgroundColor: lightTheme.grey3},
    divider: {height: 1, backgroundColor: lightTheme.grey3},

    rowBetween: { 
        flex: 1,
        flexDirection: "row", 
        justifyContent: "space-between" 
    },

    layout: {
        position: 'relative',
        flex: 1,
        backgroundColor: 'white'
    },

    img: {width: '100%', height: '100%'},
    
    pm0: { margin: 0, padding: 0 },

    w100: {width: '100%'},

    bottomPadding: {paddingBottom: 80},

    inputStyle: {
        fontFamily: 'Inter-Regular',
        lineHeight: 18,
        fontSize: 14,
        // interMedium: 'Inter-Regular',
        // interSemiBold: 'Inter-SemiBold',
    },

    // margin constant theme
    m1: { margin: constant.space1 },
    m2: { margin: constant.space2 },
    m3: { margin: constant.space3 },
    m4: { margin: constant.space4 },

    mb0: { marginBottom: 0 },
    mb1: { marginBottom: constant.space1 },
    mb2: { marginBottom: constant.space2 },
    mb3: { marginBottom: constant.space3 },
    mb4: { marginBottom: constant.space4 },

    mtAuto: { marginTop: "auto" },
    mt1: { marginTop: constant.space1 },
    mt2: { marginTop: constant.space2 },
    mt3: { marginTop: constant.space3 },
    mt4: { marginTop: constant.space4 },

    mr1: { marginRight: constant.space1 },
    mr2: { marginRight: constant.space2 },
    mr3: { marginRight: constant.space3 },
    mr4: { marginRight: constant.space4 },

    ml05: { marginLeft: constant.space1 / 2},

    ml1: { marginLeft: constant.space1 },
    ml2: { marginLeft: constant.space2 },
    ml3: { marginLeft: constant.space3 },
    ml4: { marginLeft: constant.space4 },

    // paddingHorizontal constant constant
    mh1: { marginHorizontal: constant.space1 },
    mh2: { marginHorizontal: constant.space2 },
    mh3: { marginHorizontal: constant.space3 },
    mh4: { marginHorizontal: constant.space4 },

    // paddingVertival constant constant
    mv0: { marginVertical: 0 },
    mv1: { marginVertical: constant.space1 },
    mv2: { marginVertical: constant.space2 },
    mv3: { marginVertical: constant.space3 },
    mv4: { marginVertical: constant.space4 },

    // padding constant constant
    p1: { padding: constant.space1 },
    p2: { padding: constant.space2 },
    p3: { padding: constant.space3 },
    p4: { padding: constant.space4 },

    // paddingHorizontal constant constant
    ph0: { paddingHorizontal: 0 },
    ph1: { paddingHorizontal: constant.space1 },
    ph2: { paddingHorizontal: constant.space2 },
    ph3: { paddingHorizontal: constant.space3 },
    ph4: { paddingHorizontal: constant.space4 },

    // paddingVertival constant constant
    pv1: { paddingVertical: constant.space1 },
    pv2: { paddingVertical: constant.space2 },
    pv3: { paddingVertical: constant.space3 },
    pv4: { paddingVertical: constant.space4 },
    pb1: { paddingBottom: constant.space1 },
    pb2: { paddingBottom: constant.space2 },
    pb3: { paddingBottom: constant.space3 },
    pb4: { paddingBottom: constant.space4 },

    pt0: { paddingTop: 0 },
    pt1: { paddingTop: constant.space1 },
    pt2: { paddingTop: constant.space2 },
    pt3: { paddingTop: constant.space3 },
    pt4: { paddingTop: constant.space4 },

    pr0: { paddingRight: 0 },
    pr1: { paddingRight: constant.space1 },
    pr2: { paddingRight: constant.space2 },
    pr3: { paddingRight: constant.space3 },
    pr4: { paddingRight: constant.space4 },

    pl0: { paddingLeft: 0 },
    pl1: { paddingLeft: constant.space1 },
    pl2: { paddingLeft: constant.space2 },
    pl3: { paddingLeft: constant.space3 },
    pl4: { paddingLeft: constant.space4 },
});
