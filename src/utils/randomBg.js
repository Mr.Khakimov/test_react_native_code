export function getRandomBackgroundColor() {
    const colors = [
      '#FF5733',
      '#33FF77',
      '#3366FF',
      '#FF33E8',
      '#FF9933',
      '#33FFFF',
      '#FF33CC',
      '#33FF33',
      '#FF3366',
      '#3333FF',
    ];
  
    const randomIndex = Math.floor(Math.random() * colors.length);
    return colors[randomIndex];
  }