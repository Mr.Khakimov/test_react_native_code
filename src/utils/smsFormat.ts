export function smsFormat(v:string) {
    let r = v.replace(/\D/g, '');
    if (r.length > 6) {
      r = r.replace(/^(\d\d\d)(\d{3})(\d{0,6}).*/, '$1-$2-$3');
      return r;
    } else if (r.length > 3) {
      r = r.replace(/^(\d\d\d)(\d{0,3})/, '$1-$2');
    } else {
      r = r.replace(/^(\d*)/, '$1');
    }
    return r;
  }