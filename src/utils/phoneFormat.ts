export function phoneFormat(v:string) {
    let r = v.replace(/\D/g, '');
    if (r.length > 5) {
      r = r.replace(/^(\d\d)(\d{3})(\d{0,4}).*/, '$1 $2 $3');
      return r;
    } else if (r.length > 2) {
      r = r.replace(/^(\d\d)(\d{0,2})/, '$1 $2');
    } else {
      r = r.replace(/^(\d*)/, '$1');
    }
    return r;
  }