export const PriceFormat = a => {
    if (a) {
        return a.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
    } else {
        return a;
    }
};
  