import { Platform, Dimensions } from "react-native";

const isIos = Platform.OS === 'ios';
const {width, height} = Dimensions.get('screen')

const urlPolicy = 'https://ildam.uz';
const prefix = '998';

export {isIos, width, height, urlPolicy, prefix};
