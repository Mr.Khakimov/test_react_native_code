import React from 'react';
import {useSelector, useDispatch} from "react-redux";
import { Text, View, Button, StyleSheet } from "react-native";
import { useAppSelector } from "../../redux/store";
import EmptyPage from "../../components/EmptyPageComponent";
import { VerifiedIcon } from "../../components/icons";

export const AboutScreen = () => {
  const count = useSelector(state => state.count) // getting the counter value
  const dispatch = useDispatch();
  const isDarkTheme = useAppSelector((state) => state.theme);

  return <EmptyPage title={'Sozlamalar hozircha mavjud emas'} icon={<VerifiedIcon size={96} />} />
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    fontSize: 24
  }
});
