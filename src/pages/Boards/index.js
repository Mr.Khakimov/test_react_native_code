import React from 'react';
import {useSelector, useDispatch} from "react-redux";
import { Text, View, Button, StyleSheet } from "react-native";
import { useAppSelector } from "../../redux/store";
import { setDarkTheme } from "../../redux/slices/themeSlice";

export const BoardsScreen = () => {
  const count = useSelector(state => state.count) // getting the counter value
  const dispatch = useDispatch();
  const isDarkTheme = useAppSelector((state) => state.theme);

  return (
    <View style={[styles.container, {backgroundColor: isDarkTheme.darkTheme ? '#fff' : '#002'}]}>
      <Text style={[styles.text, {color: !isDarkTheme.darkTheme ? '#fff' : '#002'}]}>BoardsScreen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    fontSize: 24
  }
});
