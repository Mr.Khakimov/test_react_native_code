import { View } from "react-native"
import ReviewComponent from "./ReviewComponent"
import { useCallback, useState } from "react"
import { useNavigation } from "@react-navigation/native"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { CustomButton, SectionTitle } from "../../components"
import { MainStyle } from "../../utils/theme"
import { DataReviewers } from "./data"

export const Reviews = ({productId, count}) => {
    const navigation = useNavigation();
    const {t} = useLocalization();

    const [btnTitle] = useState("Все отзывы магазина " + count)

    const handleAllReviews = useCallback(() => {
        navigation.navigate('Reviews');
    }, []);

    return (
        <>
        
            <SectionTitle title={t("reviewsAndRates")} />

            {
                DataReviewers.slice(0, 3).map(el => {
                    return <ReviewComponent 
                                key={el.id} 
                                name={el.name}
                                text={el.text}
                                rate={el.ball}
                                date={el.date}
                                imageUrl={el.avatar}
                                revImageUrl={el.image}
                            />
                })
            }

            {
                count > 3 &&  <View style={[MainStyle.m3, MainStyle.pb3]} >
                                <CustomButton onPress={handleAllReviews} type="secondary" text={btnTitle}></CustomButton>
                            </View>
            }

           
        </>
    )
}