export const DataReviewers = [
    {
        id: 12,
        avatar: 'https://avavatar.ru/images/avatars/32/avatar_0D4Ji6UXJveUm94P.jpg',
        name: 'Татьяна Иванова',
        date: '11 октябрь 2023',
        ball: '5.0',
        text: 'Цветы не свежие, лепестки уже были выдраны. розы полупустые некоторые, подвянувшие. неприятно так...',
        image: null
    },
    {
        id: 2,
        avatar: 'https://fikiwiki.com/uploads/posts/2022-02/1645015369_19-fikiwiki-com-p-kartinki-krasivikh-devushek-skachat-bespla-23.jpg', 
        name: 'Светлана Мартыновa', 
        date: '11 октябрь 2023', 
        ball: '3.0', 
        text: 'Цветы не свежие, лепестки уже были выдраны', 
        image: null
    },
    {
        id: 32,
        avatar: 'https://img.freepik.com/premium-photo/portrait-smiling-young-man-looking-camera_33839-1731.jpg', 
        name: 'Андрей Макаров', 
        date: '9 октябрь 2023', 
        ball: '4.0', 
        text: 'Всё классно, спасибо!!!', 
        image: 'https://leaflor.ru/wp-content/uploads/2020/12/7_1.jpg'
    },
    {
        id: 34,
        avatar: 'https://i.insider.com/63fb81d984099d001960d513?width=1136&format=jpeg', 
        name: 'Елена Павлова ', 
        date: '9 октябрь 2023', 
        ball: '4.0', 
        text: 'Этот магазин цветов всегда приятно удивляет своим ассортиментом. Каждый раз, когда я иду сюда, я нахожу что-то удивительное. Спасибо за вашу креативность!', 
        image: null
    },
    {
        id: 65,
        avatar: 'https://t4.ftcdn.net/jpg/02/24/86/95/360_F_224869519_aRaeLneqALfPNBzg0xxMZXghtvBXkfIA.jpg', 
        name: 'Александр Иванов', 
        date: '4 октябрь 2023', 
        ball: '5.0', 
        text: 'Цветы из этого магазина всегда свежие и долго радуют своей красотой. Это место, где можно найти настоящие шедевры природы', 
        image: 'https://alleya-cvetov.ru/images/product/big/2022/2022-10-27/635aa6df5d988.jpg'
    },
    {
        id: 878,
        avatar: null, 
        name: 'Сергей Кузнецов ',
        date: '4 октябрь 2023', 
        ball: '5.0', 
        text: 'Цветы из этого магазина всегда свежие', 
        image: null
    },
    {
        id: 353,
        avatar: 'https://media.meer.com/attachments/fab1026dcd503986811212d6bf4f44b28551991c/store/fill/860/645/95695d4e4c4394fb92cc829a02c7b4ed57dae7e1d0e9a5c2cf9600084c5e/A-bright-woman.jpg', 
        name: 'Ольга Козлова', 
        date: '5 октябрь 2023', 
        ball: '4.0', 
        text: 'Цветы из этого магазина всегда свежие и долго радуют своей красотой. Это место, где можно найти настоящие шедевры природы', 
        image: 'https://alleya-cvetov.ru/images/product/big/2022/2022-10-27/635aa6df5d988.jpg'
    },
    {
        id: 2323,
        avatar: 'https://nypost.com/wp-content/uploads/sites/2/2020/12/yael-most-beautiful-video.jpg?quality=75&strip=all', 
        name: 'Ольга Лебедевa', 
        date: '4 октябрь 2023', 
        ball: '5.0', 
        text: 'Цветы из этого магазина всегда свежие и долго радуют своей красотой. Это место, где можно найти настоящие шедевры природы', 
        image: 'https://storage.florist.ru/f/get/iflorist/product/O/V/_dPZ3A4SWxNeDhsZ2pKd0RLVFUyVHprYnliVjdxSGo1eE42bFVlOE9FZTFOYnJYbDNoRktYZStLQUs2OG13a05CRDRKeE4ra05EWG9xU0FZMkxDMkQ5N1MyTXFpWWhPbnU5Z1RDRUk5Wmg4RGpnTXk4MCs0eHhKd1d0VGx3VUx3Um1KaVhoQlpHbzZBNE5KdG10T0pBPT0=/800x800/img.jpg'
    },
    {
        "id": 3,
        "avatar": "https://randomuser.me/api/portraits/men/3.jpg",
        "name": "Алексей Иванов",
        "date": "6 октябрь 2023",
        "ball": "4.0",
        "text": "Букет для особенного случая. Очень доволен!",
        "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzkSP8mtHl5GaJazqOK191xT2Ttf3U4V9RX88eZD4jnTJstiQMJ6atSiQ5Y590vXUaxhA&usqp=CAU"
      },
      {
        "id": 4,
        "avatar": "https://randomuser.me/api/portraits/women/4.jpg",
        "name": "Мария Сергеева",
        "date": "7 октябрь 2023",
        "ball": "4.2",
        "text": "Покупала цветы для мамы. Она была в восторге!",
        "image": "https://letoflowers.ru/upload/iblock/602/z114c68ylm9ofti52zuahkucfom19ubg.jpeg"
      },
      {
        "id": 5,
        "avatar": "https://randomuser.me/api/portraits/men/5.jpg",
        "name": "Денис Новиков",
        "date": "8 октябрь 2023",
        "ball": "4.8",
        "text": "Букет на свадьбу был великолепен. Спасибо!",
        "image": null
      },
      {
        "id": 6,
        "avatar": "https://randomuser.me/api/portraits/women/6.jpg",
        "name": "Екатерина Козлова",
        "date": "9 октябрь 2023",
        "ball": "4.5",
        "text": "Прекрасный выбор цветов. Всегда радуют качеством.",
        "image": 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOa9NWTDhv9tjQ8JZqXijVG431GOVo_kTFGHOZHkE7IIineY-SPji5NDl417ko0dHMJR0&usqp=CAU'
      },
      {
        "id": 7,
        "avatar": "https://randomuser.me/api/portraits/men/7.jpg",
        "name": "Павел Соколов",
        "date": "10 октябрь 2023",
        "ball": "5.0",
        "text": "Лучший магазин цветов в городе!",
        "image": "https://storage.florist.ru/f/get/iflorist/product/O/V/_dPZ3A4SWxNeDhsZ2pKd0RLVFUyVHprYnliVjdxSGo1eE42bFVlOE9FZTFOYnJYbDNoRktYZStLQUs2OG13a1BUQUtRaWZ6U0RWc3B4K3NDR1NPcUpVbXFqODhNQzc3dWh1eThwRGRSU0trYjBJdFlDTk1sQ09VZEVJQmFBRjh3Um1KaVhoQlpHbzZBNE5KdG10T0pBPT0=/270x270/img.jpg"
      },
      {
        "id": 8,
        "avatar": "https://randomuser.me/api/portraits/women/8.jpg",
        "name": "Анастасия Федорова",
        "date": "11 октябрь 2023",
        "ball": "4.7",
        "text": "Купила цветы для свадьбы подруги. Все были поражены.",
        "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnOQgd9_VQqOLyKFdGM55B_wmjyKnzscK5v_BBZTvmox014YGInosSS8oqYD57DMtbo1Q&usqp=CAU"
      },
      {
        "id": 9,
        "avatar": "https://randomuser.me/api/portraits/men/9.jpg",
        "name": "Игорь Кузнецов",
        "date": "12 октябрь 2023",
        "ball": "4.3",
        "text": "Сервис отличный, а цветы просто чудо!",
        "image": "https://dari-cvety.com/assets/images/products/1379/9-kustovyh-hrizantem-1.jpg"
      },
      {
        "id": 10,
        "avatar": "https://randomuser.me/api/portraits/women/10.jpg",
        "name": "Наталья Лебедева",
        "date": "13 октябрь 2023",
        "ball": "4.8",
        "text": "Букет для мамы был удачным подарком. Она в восторге!",
        "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_bCRF7rpjfVfGfQXjXRiM1c9vttaDg8IQ-u1-5iVqtpptdGWfL_89jabCdFTj73vBr-w&usqp=CAU"
      },
      {
        "id": 11,
        "avatar": "https://randomuser.me/api/portraits/men/11.jpg",
        "name": "Дмитрий Александров",
        "date": "14 октябрь 2023",
        "ball": "5.0",
        "text": "Заказал цветы для свадьбы. Они были невероятны!",
        "image": null
      },
      {
        "id": 5463,
        "avatar": "https://randomuser.me/api/portraits/women/12.jpg",
        "name": "Татьяна Ковалева",
        "date": "15 октябрь 2023",
        "ball": "4.6",
        "text": "Прекрасное место для выбора цветов. Рекомендую!",
        "image": null
      },
]