import React, { useCallback, useRef } from 'react';
import { SafeAreaView, View } from "react-native";
import { LeftChevronIcon, PlusIcon } from "../../components/icons";
import { CustomInput, EmptyLayout, MultipleTextInput } from '../../components';
import { favEmpty } from '../../utils/images';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { MainStyle } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useNavigation } from '@react-navigation/native';
import { useAppDispatch } from '../../redux/store';
import { setIsScreenLoginModal } from '../../redux/slices/registerSlice';
import { DataReviewers } from './data';
import ReviewComponent from './ReviewComponent';
import { ScrollView } from 'react-native-gesture-handler';

const favotireData = [];

export const ReviewsScreen = () => {
  const {t} = useLocalization();
  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const input = useRef();

  const onRightIconPress = () => {
    console.log('handleright icon');
  }

  const handleButton = () => {
    dispatch(setIsScreenLoginModal());
  }

  const onLeftIconPress = useCallback(() => {
    navigation.goBack();
  }, []);

  return (
    <SafeAreaView style={[MainStyle.layout]}>
      <HeaderComponent style={{height: 70}} leftIcon={<LeftChevronIcon />} onLeftIconPress={onLeftIconPress} title={t('reviewsAndRates')}></HeaderComponent>
      
      <ScrollView>
        {
                DataReviewers.map(el => {
                    return <ReviewComponent 
                                key={el.id} 
                                name={el.name}
                                text={el.text}
                                rate={el.ball}
                                date={el.date}
                                imageUrl={el.avatar}
                                revImageUrl={el.image}
                            />
                })
            }
      </ScrollView>
    

    </SafeAreaView>
  ) 
}