import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, ImageSourcePropType } from 'react-native';
import { MainStyle, constant, lightTheme } from '../../utils/theme';
import { CustomTextComponent } from '../../components';
import { StarIcon } from '../../components/icons';


interface ReviewProps {
  name: string;
  text: string;
  rate: number | string;
  imageUrl: string | ImageSourcePropType;
  date: string;
  revImageUrl: string | ImageSourcePropType;
}

const ReviewComponent: React.FC<ReviewProps> = ({ name, text, rate, imageUrl, date, revImageUrl}) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggleExpansion = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <View style={[MainStyle.p3]}>
      <View style={[MainStyle.rowBetween]}>
        <View style={[MainStyle.flexRow, MainStyle.aic]}>
            <Image source={{ uri: imageUrl }} style={styles.image} />
            <View style={[styles.titleWrapper]}>
                <CustomTextComponent textType='bold' color="dark" size={'large'}>{name}</CustomTextComponent>
                <CustomTextComponent textType='regular' color="gray" size={'medium'}>{date}</CustomTextComponent>
            </View>
        </View>
        
        <View style={[MainStyle.flexRow, MainStyle.aic]}>
            <StarIcon />
            <CustomTextComponent style={[MainStyle.ml05]} textType='bold' color="dark" size={'large'}>{rate}</CustomTextComponent>
        </View>
      </View> 

        <View style={styles.textWrapper}>
            {isExpanded ? (
                <CustomTextComponent textType='regular' color="dark" size={'medium'}>{text}</CustomTextComponent>
            ) : (
                <CustomTextComponent numberOfLines={1} ellipsizeMode="tail" textType='regular' color="dark" size={'medium'}>{text}</CustomTextComponent>
            )}
            {text.length > 50 && (
                <TouchableOpacity onPress={toggleExpansion}>
                    <Text style={styles.readMore}>{isExpanded ? 'Скрыть' : 'Показать ещё '}</Text>
                </TouchableOpacity>
            )}
        </View>

        {
            revImageUrl &&  <View style={styles.imgWrapper}>
                <Image source={{ uri: revImageUrl }} style={styles.revImage} />
            </View>
        }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flexDirection: 'row',
    // alignItems: 'center',
    // padding: 16,
  },
  titleWrapper: {
    flexGrow: 1,
  },
  image: {
    width: 32,
    height: 32,
    borderRadius: 30,
    marginRight: constant.space1,
    backgroundColor: lightTheme.grey,
  },
  textWrapper: {
    marginTop: 14,
  },
  imgWrapper: {
    marginTop: 12,
  },
  revImage: {
    width: 149,
    height: 149,
    borderRadius: 30,
    marginRight: constant.space1,
  },
  readMore: {
    fontSize: 12,
    color: lightTheme.red,
    marginTop: 4,
  },
});

export default ReviewComponent;
