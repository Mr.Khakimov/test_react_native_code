import { StyleSheet, View } from "react-native"
import { MainStyle, constant, lightTheme } from "../../utils/theme";
import MapView from 'react-native-maps';

export const DeliveryMap = () => {
    return (
        <View style={MainStyle.p3}>
            <View style={styles.mapWrapper}>
            <MapView
                style={styles.map}
                zoomControlEnabled
                maxZoomLevel={20}
                minZoomLevel={4}
                initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
            />
            <View style={styles.pinWrapper} pointerEvents="none">
                <View style={styles.pin}>
                    <View style={styles.pinInner}>
                    
                    </View>
                </View>
                <View style={styles.pinLine} />
            </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mapWrapper: {
        height: 160,
        borderRadius: constant.space3,
        overflow: 'hidden',
        backgroundColor: lightTheme.grey,
        position: 'relative',
    },
    map: {
        flex: 1,
    },
    pinWrapper: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    pinInner: {
        width: 14,
        height: 14,
        borderRadius: 60,
        backgroundColor: lightTheme.textWhite,
    },
    pin: {
        width: 32,
        height: 32,
        borderRadius: 60,
        backgroundColor: lightTheme.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    pinLine: {
        width: 3,
        height: 12,
        borderRadius: 12,
        backgroundColor: lightTheme.primaryColor,
    }
});