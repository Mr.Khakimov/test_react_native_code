import { View } from "react-native"
import { CircleCheckbox, CustomBorderedInput, CustomButton, CustomTextComponent, CustomTitle } from "../../components"
import { MainStyle } from "../../utils/theme"
import { ScrollView, TouchableHighlight } from "react-native-gesture-handler";
import { useTheme } from "../../customHooks/ThemeHook";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { setDeliveryPerson } from "../../redux/slices/basketSlice";
import { ContactsIcon } from "../../components/icons";
import { ContactsList } from "../../components/Contacts/ContactsList";
import { useState } from "react";

type TProps = {
    handleOk: () => void;
    handleContactModalShow: () => void;
}

export const DeliveryBottomSheet = ({handleOk, handleContactModalShow}: TProps) => {
    const {theme} = useTheme();
    const {t} = useLocalization();
    const dispatch = useAppDispatch();
    const deliveryPerson = useAppSelector(state => state.basket.deliveryPerson);
    const deliveryPersonOther = useAppSelector(state => state.basket.deliveryPersonOther);

    console.log('deliveryPersonOther', deliveryPerson, deliveryPersonOther);
    

    const handleCheckBox1 = () => {
        dispatch(setDeliveryPerson('mine'))
    };
    const handleCheckBox2 = () => {
        dispatch(setDeliveryPerson('others'))
    };

    return (
        <View style={[MainStyle.pb3, MainStyle.flex]}>
            <CustomTitle style={[MainStyle.tac]}>{t('receiver')}</CustomTitle>
            <CustomTextComponent 
                style={[MainStyle.tac, MainStyle.pv1]} 
                color={'gray'} 
                size={'medium'}>
                    {t('whoIsReceive')}
            </CustomTextComponent>

            <ScrollView style={[MainStyle.flex, {minHeight: 150}]}>
                <CircleCheckbox toggleCheckbox={handleCheckBox1} isChecked={deliveryPerson === 'mine'} style={[MainStyle.p3]}>
                    <CustomTextComponent 
                        textType="bold"
                        size="extraLarge"
                        color="dark">
                            {t('iWillReceive')}
                    </CustomTextComponent>
                </CircleCheckbox>
         
                <CircleCheckbox 
                    toggleCheckbox={handleCheckBox2} 
                    isChecked={deliveryPerson === 'others'} style={[MainStyle.p3]}>
                    <CustomTextComponent 
                        textType="bold"
                        size="extraLarge"
                        color="dark">
                            {t('orherWillReceive')}
                    </CustomTextComponent>
                </CircleCheckbox>

                {
                    deliveryPerson === 'others' ? <View style={MainStyle.p3}>
                        <CustomBorderedInput
                            containerStyle={MainStyle.mb3} 
                            placeholder={t('nameReceiver')}
                            value={deliveryPersonOther?.name}
                            isBottomSheet={true}
                            rightIcon={
                                <TouchableHighlight 
                                    underlayColor={theme.grey3} 
                                    onPress={handleContactModalShow}>
                                        <ContactsIcon />
                                </TouchableHighlight>
                            }
                        />
                        <CustomBorderedInput value={deliveryPersonOther?.phone} containerStyle={MainStyle.mb3} placeholder={t('phoneReceiver')}  />
                    </View> : null
                }
            </ScrollView> 

            <View style={MainStyle.mh3}>
                <CustomButton onPress={handleOk} text={'Сохранить'} />
            </View>
        </View>
    )
}