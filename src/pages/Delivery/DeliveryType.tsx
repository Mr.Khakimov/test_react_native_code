import { View } from "react-native"
import { DeliveryTypeCheckbox } from "./DeliveryTypeCheckbox"
import { useState } from "react"
import { MainStyle } from "../../utils/theme";
import { ScrollView } from "react-native-gesture-handler";

const deliveryData = [
    {id: 1, title: 'Быстрее', text: '30 минут', isFree: true},
    {id: 2, title: 'Другое время', text: 'Выберите время и дату', isFree: false},
];

export const DeliveryType = () => {
    const [active, setActive] = useState<number | string>(1);

    const handleType = (id: number | string) => {
        setActive(id);
    }

    return (
        <View style={MainStyle.flexRow}>
            <ScrollView horizontal>
                {
                    deliveryData.map(el => {
                        return <DeliveryTypeCheckbox
                                    key={el.id}
                                    onPress={() => handleType(el.id)}
                                    active={active === el.id} 
                                    title={el.title}
                                    text={el.text}
                                    isFree={el.isFree}  />
                    })
                }
            </ScrollView>
        </View>
    )
}