import { SafeAreaView, TouchableHighlight, View } from "react-native";
import { LeftChevronIcon, RightChevronIcon } from "../../components/icons";
import HeaderComponent from "../../components/layaouts/HeaderComponent";
import { MainStyle } from "../../utils/theme";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import React, { useCallback, useEffect, useLayoutEffect, useMemo, useRef, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { setDeliveryOptions, showBottomBasketLink } from "../../redux/slices/basketSlice";
import { DeliveryType } from "./DeliveryType";
import { useTheme } from "../../customHooks/ThemeHook";
import { CustomTextComponent } from "../../components";
import { DeliveryMap } from "./DeliveryMap";

import {
    BottomSheetBackdrop,
    BottomSheetModal,
    BottomSheetModalProvider,
  } from '@gorhom/bottom-sheet';
  import { Portal } from '@gorhom/portal';
import { DeliveryBottomSheet } from "./DeliveryBottomSheet";
import { ContactsList } from "../../components/Contacts/ContactsList";
import { setGestureState } from "react-native-reanimated";

export const DeliveryScreen = () => {
    const {t} = useLocalization();
    const navigation = useNavigation();
    const dispatch = useAppDispatch();
    const {theme} = useTheme();
    const [value, setValue] = useState('');
    const [showContactModal, setShowContactModal] = useState(false)

    const deliveryPersonOthers = useAppSelector(state => state.basket.deliveryPerson === 'others');
    const deliveryComment = useAppSelector(state => state.basket?.deliveryComment);

    const bottomSheetModalRef = useRef<BottomSheetModal>(null);

    const snapPoints = useMemo(() => [deliveryPersonOthers ? 440 : 290], [deliveryPersonOthers])

    const handlePresentModalPress = useCallback(() => {
        bottomSheetModalRef.current?.present();
    }, []);

    const handleModalDismiss = useCallback(() => {
        bottomSheetModalRef.current?.dismiss();
    }, []);

    const handleSheetChanges = useCallback((index: number) => {
        console.log('handleSheetChanges', index);
    }, []);

    const goBack = useCallback(() => {
        navigation.goBack();
    }, [])

    const onChangeText = useCallback((e : any) => {
        if(e.length < 401) {
            setValue(e)
        } else {

        }      
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            setTimeout(() => {
                dispatch(showBottomBasketLink(false));
            }, 10)
            
            return () => dispatch(showBottomBasketLink(true));
        }, [])
    );

    const navigationToCommentScreen = () => {
        navigation.navigate('CommentScreen', {title: 'addressComment', data: {}});
    }

    const handleA = useCallback(() => {
        // navigation.navigate('CommentScreen');
    }, [])

    const handleContact = (item : any) => {
        dispatch(setDeliveryOptions({key: 'deliveryPersonOther', value: {
            name: `${item?.givenName} ${item.familyName}`,
            phone: item?.phoneNumbers[0]?.number
        }}));
        setShowContactModal(false)
    }

    const renderBackdrop = useCallback(
        props => (
          <BottomSheetBackdrop
            {...props}
            disappearsOnIndex={-1}
            appearsOnIndex={0}
          />
        ),
        []
      );

    return (
        <SafeAreaView style={MainStyle.layout}>
            <HeaderComponent 
                leftIcon={<LeftChevronIcon />}
                onLeftIconPress={goBack}
                title={t('Доставка')}
            />

            <View style={[MainStyle.flexRow, MainStyle.p3]}>
                <DeliveryType />
            </View>

            <TouchableHighlight onPress={handlePresentModalPress} underlayColor={theme.grey3} style={[MainStyle.flexRow, MainStyle.p3, MainStyle.jsb,  MainStyle.aic]}>
                <>
                    <CustomTextComponent 
                        textType="bold"
                        size="extraLarge"
                        color="dark">
                            Получатель
                    </CustomTextComponent>
                    <View style={[MainStyle.flexRow, MainStyle.jsb,  MainStyle.aic]}>
                        <CustomTextComponent 
                            textType="regular" 
                            size="medium"
                            color="gray">
                                {!deliveryPersonOthers ? t('iWillReceive') : t('orherWillReceive')}
                        </CustomTextComponent>
                        <RightChevronIcon />  
                    </View>
                </>
            </TouchableHighlight>

            <TouchableHighlight 
                onPress={navigationToCommentScreen} 
                underlayColor={theme.grey3} 
                style={[MainStyle.flexRow, MainStyle.p3, MainStyle.jsb,  MainStyle.aic]}>
                <>
                    <CustomTextComponent 
                        textType="bold"
                        size="extraLarge"
                        color="dark">
                            Комментарий
                    </CustomTextComponent>
                    <View style={[MainStyle.flexRow, MainStyle.jsb,  MainStyle.aic]}>
                        <CustomTextComponent 
                            textType="regular" 
                            size="medium"
                            numberOfLines={1}
                            color="gray">
                                delivery
                                {/* {deliveryComment?.length ? deliveryComment : t('add')} */}
                        </CustomTextComponent>
                        <RightChevronIcon />  
                    </View>
                </>
            </TouchableHighlight>

            <TouchableHighlight onPress={handleA} underlayColor={theme.grey3} style={[MainStyle.flexRow, MainStyle.p3, MainStyle.jsb,  MainStyle.aic]}>
                <>
                    <CustomTextComponent 
                        textType="bold"
                        size="extraLarge"
                        color="dark">
                            Адрес
                    </CustomTextComponent>
                    <View style={[MainStyle.flexRow, MainStyle.jsb,  MainStyle.aic]}>
                        <CustomTextComponent 
                            textType="regular" 
                            size="medium"
                            color="gray">
                                Ташкент, ул. Навои, д. 26
                        </CustomTextComponent>
                        <RightChevronIcon />  
                    </View>
                </>
            </TouchableHighlight>

            <TouchableHighlight onPress={handleA} underlayColor={theme.grey3} style={[MainStyle.flexRow, MainStyle.p3, MainStyle.jsb,  MainStyle.aic]}>
                <>
                    <CustomTextComponent 
                        textType="bold"
                        size="extraLarge"
                        color="dark">
                            Кв./оф./этаж/подъезд
                    </CustomTextComponent>
                    <View style={[MainStyle.flexRow, MainStyle.jsb,  MainStyle.aic]}>
                        <CustomTextComponent 
                            textType="regular" 
                            size="medium"
                            color="gray">
                               Добавить
                        </CustomTextComponent>
                        <RightChevronIcon />  
                    </View>
                </>
            </TouchableHighlight>

            <DeliveryMap />

            <Portal hostName="CustomPortalHost1">
            <BottomSheetModalProvider>
                <BottomSheetModal
                  ref={bottomSheetModalRef}
                  index={0}
                  snapPoints={snapPoints}
                  keyboardBehavior="interactive"
                  handleIndicatorStyle={{
                    backgroundColor: theme.grey3
                  }}
                  android_keyboardInputMode={'adjustPan'}
                  backgroundStyle={{borderRadius: 32}}
                  backdropComponent={renderBackdrop}
                  onChange={handleSheetChanges}
                >
                    <DeliveryBottomSheet handleContactModalShow={() => setShowContactModal(true)} handleOk={handleModalDismiss} />
                </BottomSheetModal>
              </BottomSheetModalProvider>

              <ContactsList isOpen={showContactModal} handleContact={handleContact} handleClose={() => setShowContactModal(false)} />

          </Portal>

        </SafeAreaView>    
    )
}