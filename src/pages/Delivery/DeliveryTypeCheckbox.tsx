import { useNavigation } from "@react-navigation/native";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { useCallback, useMemo, useState } from "react";
import { useTheme } from "../../customHooks/ThemeHook";
import { StyleSheet, TouchableHighlight, View } from "react-native";
import { CustomTextComponent, TagComponent } from "../../components";
import { ChkBoxActiveIcon } from "../../components/icons";
import { MainStyle, constant, lightTheme } from "../../utils/theme";

type TProps = {
    active?: boolean,
    isFree: boolean,
    title?: string,
    text?: string,
    onPress: () => void;
}

export const DeliveryTypeCheckbox = ({active = false, title, text, isFree, onPress}: TProps) => {
    const {t} = useLocalization();
    const {theme} = useTheme();

    const memoActiveStyle = useMemo(() => {
        return {
            wrapper: {borderColor: active ? theme.textDark : theme.grey3},
            chbx: {borderColor: active ? theme.textDark : theme.grey3},
        }
    }, [active]);

    return (
        <TouchableHighlight onPress={onPress} underlayColor={theme.grey3} style={[styles.tag, memoActiveStyle.wrapper]}>
            <>
            
                <View style={styles.main}>
                    <CustomTextComponent 
                        style={styles.text}
                        textType="bold"
                        size="large"
                        color="dark">
                            {title}
                    </CustomTextComponent>

                    <CustomTextComponent 
                        style={styles.text}
                        textType="regular"
                        color="gray">
                            {text}
                    </CustomTextComponent>

                    {
                        isFree 
                        && 
                        <TagComponent 
                            textColor="dark" 
                            textSize={'small'} 
                            style={MainStyle.mt1}
                            backgroundColor={theme.grey3} 
                            text={t('Бесплатно')} />
                    }
                </View>
                <View style={styles.left}>
                    <View style={[styles.chkbox, memoActiveStyle.chbx]}>
                        {active ? <ChkBoxActiveIcon /> : null}
                    </View>
                </View>

            </>
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    tag: {
        borderRadius: 12,
        borderWidth: 1,
        flexDirection: 'row',
        overflow: 'hidden',
        minWidth: 150,
        maxWidth: 170,
        marginRight: constant.space1,
    },

    main: {
        flex: 1,
        paddingVertical: 8,
        paddingLeft: 8,
    },

    text: {
        marginBottom: 4,
    },

    left: {
        paddingRight: 8,
        paddingVertical: 8,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },

    chkbox: {
        width: 20,
        height: 20,
        borderRadius: 6,
        borderWidth: 1,
    },

    textArea: {
        height: 100,
        borderRadius: 18,
        borderWidth: 1,
        padding: 12,
        borderColor: lightTheme.dark,
    },
});