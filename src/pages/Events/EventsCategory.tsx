import React, { useCallback } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, } from 'react-native';
import { constant, lightTheme } from '../../utils/theme';
import { CustomTextComponent } from '../../components';

import {Grayscale} from 'react-native-color-matrix-image-filters'

const GrayscaledImage = (imageProps: any) => (
    <Grayscale>
     <Image {...imageProps} />
    </Grayscale>
)

interface EventCategoryProps {
  id: string | number,
  title: string;
  imageSource: any;
  onPress: (id: string | number) => void;
  active: boolean,
}

const EventCategory: React.FC<EventCategoryProps> = ({ id, title, imageSource, onPress, active }) => {

    const handler = useCallback(() => {
        onPress(id)
    }, [])

    return (
    <TouchableOpacity onPress={handler} style={styles.container}>
        <View style={styles.imgWrapper}>
            {
               active ? <Image source={imageSource} style={styles.image} /> : <GrayscaledImage source={imageSource} style={styles.image} />
            }
        </View>
        <CustomTextComponent style={styles.title} color={active ? "dark" : 'gray'} size="small" textType="bold">{title}</CustomTextComponent>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    minWidth: 76,
    marginRight: 8,
    alignItems: 'center',
  },
  imgWrapper: {
    display: 'flex',
    width: 76,
    height: 76,
    borderRadius: 16,
    paddingTop: 17,
    paddingHorizontal: 5,
    justifyContent: 'center',
    backgroundColor: lightTheme.grey3,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  title: {
    textAlign: 'center',
    padding: constant.space1,
  },
});

export default EventCategory;