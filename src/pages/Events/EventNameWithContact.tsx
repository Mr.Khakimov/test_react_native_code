import { Alert, FlatList, PermissionsAndroid, Platform, TouchableHighlight, View } from "react-native"
import { CustomBorderedInput, ListItem } from "../../components"
import { MainStyle } from "../../utils/theme"
import { ContactsIcon } from "../../components/icons"
import { useTheme } from "../../customHooks/ThemeHook"
import { useEffect, useState } from "react"
import Contacts from 'react-native-contacts';

export const EventNameWithContact = () => {
    const {theme} = useTheme();

    let [contacts, setContacts] = useState([]);

    useEffect(() => {
      if (Platform.OS === 'android') {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
            title: 'Contacts',
            message: 'This app would like to view your contacts.',
          }).then(() => {
            loadContacts();
          }
        );
      } else {
        loadContacts();
      }
    }, []);

    const loadContacts = () => {
        //  Contacts.getAll()
        //   .then(contacts => {
        //     // contacts.sort(
        //     //   (a, b) => 
        //     //   a.givenName.toLowerCase() > b.givenName.toLowerCase(),
        //     // );
        //     // setContacts(contacts);
        //     console.log('contacts to access contacts was contacts',contacts);
        //   })
        //   .catch(e => {
        //     // Alert('Permission to access contacts was denied');
        //     console.log('Permission to access contacts was denied');
        //   });

        // Contacts.getAll()
        //   .then(contacts => {
        //     contacts.sort(
        //       (a, b) => 
        //       a.givenName.toLowerCase() > b.givenName.toLowerCase(),
        //     );
        //     setContacts(contacts);
        //   })
        //   .catch(e => {
        //     Alert('Permission to access contacts was denied');
        //     console.warn('Permission to access contacts was denied');
        //   });
      };

    const handle = () => {
        console.log('handle');
    }

    const openContact = () => {};

    return (
        <View>
            <CustomBorderedInput 
                containerStyle={MainStyle.mb3} 
                placeholder='Имя именинника' 
                rightIcon={<TouchableHighlight underlayColor={theme.grey3} onPress={handle}><ContactsIcon /></TouchableHighlight>}
            />

        {/* <FlatList
          data={contacts}
          renderItem={(contact) => {
            {
              console.log('contact -> ' + JSON.stringify(contact));
            }
            return (
              <ListItem
                key={contact.item.recordID}
                title={contact.item}
                onPress={openContact}
              />
            );
          }}
          keyExtractor={(item) => item.recordID}
        /> */}
        </View>
    )
}