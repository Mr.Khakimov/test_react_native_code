import React from 'react';
import { SafeAreaView } from "react-native";
import { PlusIcon } from "../../components/icons";
import { EmptyLayout } from '../../components';
import { evEmpty, favEmpty } from '../../utils/images';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { MainStyle } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useTheme } from '../../customHooks/ThemeHook';

const favotireData = [];

export const EventsScreen = ({navigation}) => {
  const {t} = useLocalization();
  const {theme} = useTheme();

  const onRightIconPress = () => {
    navigation.navigate('EventCreateScreen');
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
      <HeaderComponent rightIconBg='secondary' title={t('event')} rightIcon={<PlusIcon stroke={theme.textDark}  width={24} height={24} />} onRightIconPress={onRightIconPress}></HeaderComponent>
      {favotireData.length 
        ? <>
        
        </> : 
        <EmptyLayout 
          imageSource={evEmpty} 
          title={t('eventsEmptyTitle')} 
          text={t('eventsEmptyText')}
          buttonText={t('addEvent')}   
        />
      }
    </SafeAreaView>
  ) 
}