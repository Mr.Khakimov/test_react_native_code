import React, { useState } from 'react';
import { KeyboardAvoidingView, SafeAreaView, StyleSheet, TouchableHighlight, View } from "react-native";
import { LeftChevronIcon, RightChevronIcon } from "../../components/icons";
import { CustomBorderedInput, CustomButton, CustomTextComponent } from '../../components';
import { birthDayCake } from '../../utils/images';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { MainStyle } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useTheme } from '../../customHooks/ThemeHook';
import { ScrollView } from 'react-native-gesture-handler';
import { isIos } from '../../utils/constants';
import EventCategory from './EventsCategory';
import { EventNameWithContact } from './EventNameWithContact';

const eventCategory = [
  {id: 1, name: 'birthday', img: birthDayCake},
  {id: 2, name: 'anniversary', img: birthDayCake},
  {id: 3, name: 'eventCat', img: birthDayCake},
  {id: 4, name: 'measure', img: birthDayCake},
];

interface IFormProps {
  categoryId: string | number,
}

export const EventsCreateScreen = ({navigation}) => {
  const {t} = useLocalization();
  const {theme} = useTheme();
  const [state, setState] = useState<IFormProps>({
    categoryId: 1,
  });

  const onLeftIconPress = () => {
    navigation.goBack();
  }

  const handleEventCategory = (id: number | string) => {
    setState({categoryId: id})
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
      <KeyboardAvoidingView
        behavior={isIos ? 'padding' : 'height'}
        style={MainStyle.flex}>
        <HeaderComponent leftIcon={<LeftChevronIcon />} onLeftIconPress={onLeftIconPress} title={t('newEvent')} titleStyle={MainStyle.tac}></HeaderComponent>
        
        <ScrollView contentContainerStyle={{flexGrow: 1}}>

          <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={MainStyle.pl2}>
            {eventCategory.map(el => <EventCategory key={el.id} id={el.id} active={state.categoryId === el.id} onPress={handleEventCategory} title={t(el.name)} imageSource={el.img} />)}
          </ScrollView>

          <View style={[MainStyle.p3, MainStyle.flex]}>
            <EventNameWithContact />
            <CustomBorderedInput containerStyle={MainStyle.mb3} placeholder='Номер телефона (необязательно)'  />
            <CustomBorderedInput containerStyle={MainStyle.mb3} placeholder='Адрес получателя (необязательно)'  />
          
            <TouchableHighlight style={[MainStyle.flexRow, MainStyle.pv3, MainStyle.jsb,  MainStyle.aic]}>
              <>
                <CustomTextComponent textType="bold" size="extraLarge" color="dark">Когда</CustomTextComponent>
                <View style={[MainStyle.flexRow, MainStyle.jsb,  MainStyle.aic]}>
                  <CustomTextComponent textType="regular" size="medium" color="gray">Понедельник, 28 августа 2023</CustomTextComponent>
                  <RightChevronIcon />  
                </View>
              </>
            </TouchableHighlight>

            <TouchableHighlight style={[MainStyle.flexRow, MainStyle.pv3, MainStyle.jsb,  MainStyle.aic]}>
              <>
                <CustomTextComponent textType="bold" size="extraLarge" color="dark">Напоминание</CustomTextComponent>
                <View style={[MainStyle.flexRow, MainStyle.jsb, MainStyle.aic]}>
                  <CustomTextComponent textType="regular" size="medium" color="gray">Понедельник, 28 августа 2023</CustomTextComponent>
                  <RightChevronIcon />  
                </View>
              </>
          </TouchableHighlight>
          </View>

      

          <View style={MainStyle.m3}>
            <CustomButton text={"Сохранить"}></CustomButton>
          </View>
         
        </ScrollView>
      
      </KeyboardAvoidingView>

    </SafeAreaView>
  ) 
}

const styles = StyleSheet.create({
  container: {

  },
});