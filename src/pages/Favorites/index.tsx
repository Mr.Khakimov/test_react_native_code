import React, { useCallback, useMemo, useRef } from 'react';
import { StyleSheet, SafeAreaView, View, Button, Text, KeyboardAvoidingView} from "react-native";
import { PlusIcon } from "../../components/icons";
import { CustomButton, CustomTextComponent, CustomTitle, EmptyLayout } from '../../components';
import { favEmpty } from '../../utils/images';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { MainStyle } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useTheme } from '../../customHooks/ThemeHook'

import {
  BottomSheetBackdrop,
  BottomSheetModal,
  BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import { Portal } from '@gorhom/portal';
import CustomBottomSheetInput from '../../components/Inputs/CustomBottomSheetInput';

const favotireData = [];

export const FavoritesScreen = ({navigation}) => {
  const {t} = useLocalization();
  const bottomSheetModalRef = useRef<BottomSheetModal>(null);
  const snapPoints = useMemo(() => [320], [])

  const handlePresentModalPress = useCallback(() => {
    bottomSheetModalRef.current?.present();
  }, []);
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);

  const onRightIconPress = () => {
    console.log('handleright icon');
  }

  const {theme} = useTheme();

  const goToMain = () => {
    navigation.navigate('MainPage')
  }

 // renders
 const renderBackdrop = useCallback(
  props => (
    <BottomSheetBackdrop
      {...props}
      disappearsOnIndex={-1}
      appearsOnIndex={0}
    />
  ),
  []
);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
      style={styles.container}>
    <SafeAreaView style={MainStyle.layout}>
      <HeaderComponent rightIconBg='secondary' title={t('favorites')} rightIcon={<PlusIcon width={24} height={24} stroke={theme.textDark} />} onRightIconPress={handlePresentModalPress}></HeaderComponent>
      {favotireData.length 
        ? <>



        </> : 
        <EmptyLayout 
          imageSource={favEmpty} 
          title={t('favoritesEmptyTitle')} 
          text={t('favoritesEmptyText')}
          buttonText={t('goToMain')}   
          onButtonPress={goToMain}
        />
      }

        {/* <View style={styles.container}> */}
          <Portal hostName="CustomPortalHost1">
            <BottomSheetModalProvider>
                <BottomSheetModal
                  ref={bottomSheetModalRef}
                  index={0}
                  snapPoints={snapPoints}
                  keyboardBehavior="fillParent"
                  handleComponent={null}
                  backdropComponent={renderBackdrop}
                  onChange={handleSheetChanges}
                >
                  <View style={[MainStyle.p3, MainStyle.flex]}>
                    <CustomTitle style={[MainStyle.tac, MainStyle.pb3]}>Новая подборка</CustomTitle>
                    <CustomBottomSheetInput autoFocus={true} placeholder='Название подборки' />
                   
                   <View style={MainStyle.flex}>
                    <CustomTextComponent style={MainStyle.pv3} color={'dark'} size={'medium'}>
                        Создайте персональные коллекции для особых моментов в жизни – дни рождения, праздники и многие другие радостные события.
                      </CustomTextComponent>
                      
                    </View> 
            
                   
                     <CustomButton onPress={handlePresentModalPress} text={'Сохранить'} />

                  </View>
                </BottomSheetModal>
              </BottomSheetModalProvider>
          </Portal>
         
        {/* </View> */}
    </SafeAreaView>
    </KeyboardAvoidingView>
  ) 
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    fontSize: 24
  },
  input: {
    marginTop: 8,
    marginBottom: 10,
    borderRadius: 10,
    fontSize: 16,
    lineHeight: 20,
    padding: 8,
    backgroundColor: 'rgba(151, 151, 151, 0.25)',
  },
});
