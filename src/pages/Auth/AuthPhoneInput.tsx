import { StyleSheet, TextInput } from "react-native"
import { constant } from "../../utils/theme";
import { phoneFormat } from "../../utils/phoneFormat";
import { memo, useCallback, useMemo, useRef } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { setPhone } from "../../redux/slices/registerSlice";
import { useTheme } from "../../customHooks/ThemeHook";

export const AuthPhoneInput = memo(() => {
    const dispatch = useAppDispatch();
    const phoneRef = useRef(null);
    const {theme} = useTheme();
    const phone = useAppSelector(state => state.register.phone);

     const memoInputStyle = useMemo(() => {
       return {}
     }, [])
   
     const onChangeNumber = useCallback((e: string) => {
        dispatch(setPhone(e))
     }, []);

    return (
        <TextInput
            ref={phoneRef}
            placeholder="91 123 4567"
            style={[styles.input, memoInputStyle]}
            onChangeText={onChangeNumber}
            maxLength={11}
            autoFocus={true}
            value={phoneFormat(phone)}
            keyboardType="numeric"
            cursorColor={theme.textDark}
            selectionColor={theme.textDark}
            allowFontScaling={false}
        />
    )
})

const styles = StyleSheet .create({
    input: {
      fontFamily: constant.mPlusBold,
      fontSize: 32,
      flex: 1,
      paddingVertical: 0,
      lineHeight: constant.lineHeightExtra,
      paddingHorizontal: 8
    }
  });
  