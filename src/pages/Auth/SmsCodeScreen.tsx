// import React, {useState, useRef, useCallback, useEffect, useMemo} from 'react';
import {
  View,
  StyleSheet,
  BackHandler,
  ScrollView,
  TextInput,
  Text,
  ActivityIndicator,
} from 'react-native';
import { CustomButton, CustomTextComponent, MegaTitle, PageLayout } from '../../components';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useNavigation } from '@react-navigation/native';
import { MainStyle } from '../../utils/theme';
import { LeftChevronIcon } from '../../components/icons';
import { useAppDispatch, useAppSelector } from '../../redux/store';
import { useCallback, useState } from 'react';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { SmsCodeInput } from './SmsCodeInput';
import { prefix } from '../../utils/constants';
import { SmsExpiredCountDown } from './SmsExpiredCountDown';
import { registerSendSms, setIsScreenLoginModal, setTimerOffset } from '../../redux/slices/registerSlice';
import { useTheme } from '../../customHooks/ThemeHook';
import FullLoading from '../../components/layaouts/FullLoading';

const expiredSmsTime = 59000;

export const SmsCodeScreen = () => {
  const btnDisabled = useAppSelector(state => state.register.btnDisable);
  const phone = useAppSelector(state => state.register.phone);
  const timerOffset = useAppSelector(state => state.register.timeOffset);
  const dispatch = useAppDispatch();
  const btnLoading = useAppSelector(state => state.register.btnLoading);
  const smsLoading = useAppSelector(state => state.register.smsLoading);
  const smsError = useAppSelector(state => state.register.smsError);

  const {t} = useLocalization();
  const {theme} = useTheme();

  const handleGoBack = useCallback(() => {
    dispatch(setIsScreenLoginModal())
  }, [])

  const handleButton = useCallback(() => {
    // navigation.goBack();
  }, [])

  const [smsState, setSmsState] = useState({
    internalVal: null,
    focusInput: true,
    validate: true,
    active: !timerOffset ? false : true,
    loading: false,
    inpDisabled: false,
    alertModal: false,
    alertText: '',
    alertTitle: '',
  });

  const countFuncClose = useCallback(() => {
    setSmsState(prev => {
      return {...prev, active: true};
    });
    dispatch(setTimerOffset(null));
  }, []);

  const changeNumber = useCallback(() => {
    // navigation.goBack();
  }, [])

  const smsRepeat = useCallback(() => {
    const p = prefix + phone.replace(/\D/g, '');
    dispatch(registerSendSms({data: {phone: p, otp: '/dadhfbds'}}))
  }, [phone]);

  return (
    <PageLayout>
      <HeaderComponent title={''} onLeftIconPress={handleGoBack} leftIcon={<LeftChevronIcon stroke={'#000000'} />} />
       
      <ScrollView contentContainerStyle={[MainStyle.contentContainerStyle, MainStyle.p3]}>
        <MegaTitle style={MainStyle.mb1}>{t('smsWrite')}</MegaTitle>
        <CustomTextComponent size="large">+{prefix} {phone} {t('smsWriteSubText')}</CustomTextComponent>

        <View style={[MainStyle.mv4]}>
          <SmsCodeInput />
        </View>

        {
          smsError ? <CustomTextComponent style={[MainStyle.mb4]} size="large">
            <Text style={{color: theme.red}}>{t(smsError)}:</Text>
          </CustomTextComponent>

          : null
        }

        <View style={MainStyle.flexRow}>
          <CustomTextComponent style={MainStyle.mb1} size="large">{t('repeatSms')}: </CustomTextComponent>

          {
            timerOffset ? 
              <SmsExpiredCountDown
                timerOffset={timerOffset}
                closeTimer={countFuncClose}
                expiredTimer={expiredSmsTime}
            /> : 
            btnLoading
            ? <ActivityIndicator size="small" />
            : <CustomTextComponent size="large">
                <Text onPress={smsRepeat} style={{color: theme.red}}>{t('repeatAgain')}</Text>
              </CustomTextComponent>
          }
        </View>

        {
          !timerOffset ?  <View style={MainStyle.mt4}>
            <CustomTextComponent size="large">
              <Text onPress={changeNumber} style={{color: theme.red}}>{t('changeNumber')}</Text>
            </CustomTextComponent>
          </View> : null
        }
       
      </ScrollView>

      {smsLoading ? <FullLoading /> : null}
    </PageLayout>
  )
};
