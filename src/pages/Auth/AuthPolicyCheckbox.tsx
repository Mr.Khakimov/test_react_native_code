import { memo, useCallback } from "react";
import { MainStyle } from "../../utils/theme"
import { Checkbox, CustomTextComponent } from "../../components"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { Alert, Linking, Text } from "react-native";
import { useTheme } from "../../customHooks/ThemeHook";
import { isIos, urlPolicy } from "../../utils/constants";
import { setPolicySelect } from "../../redux/slices/registerSlice";

export const AuthPolicyCheckbox = memo(() => {
    const {t} = useLocalization();
    const {theme} = useTheme();
    const isChecked = useAppSelector(state => state.register.policySelect);
    const dispatch = useAppDispatch();

    const toggleCheckbox = useCallback(() => {
        dispatch(setPolicySelect());
    }, []);

    const handlePolicy = useCallback(async () => {
        const supported = await Linking.canOpenURL(urlPolicy);

        if(isIos) {
            if (supported) {
                await Linking.openURL(urlPolicy);
            } else {
                Alert.alert(`Don't know how to open this URL: ${urlPolicy}`);
            }
        } else {
            await Linking.openURL(urlPolicy);
        }
       
    }, []);

    return (
        <Checkbox toggleCheckbox={toggleCheckbox} isChecked={isChecked}>
            <CustomTextComponent style={[MainStyle.ml1, MainStyle.flex]} color="dark" size="medium">
                {t('agreeWith')}
                <Text onPress={handlePolicy} style={{color: theme.red}}>{t('appTerms')}</Text>
                {t('and')}
                <Text onPress={handlePolicy} style={{color: theme.red}}>{t('policyText')}</Text>
            </CustomTextComponent>
        </Checkbox>
    )
})