import React, { useCallback } from "react";
import { View } from "react-native";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { useNavigation } from "@react-navigation/native";
import { CustomButton, CustomTextComponent, MegaTitle, PageLayout } from "../../components";
import HeaderComponent from "../../components/layaouts/HeaderComponent";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { CancelIcon } from "../../components/icons";
import { MainStyle } from "../../utils/theme";
import { ScrollView } from "react-native-gesture-handler";
import { AuthPolicyCheckbox } from "./AuthPolicyCheckbox";
import { AuthPhoneInput } from "./AuthPhoneInput";
import { registerSendSms, setRegisterClear } from "../../redux/slices/registerSlice";
import { prefix } from "../../utils/constants";

export const AuthScreen = () => {
  const {t} = useLocalization();
  // const navigation = useNavigation();
  const btnDisabled = useAppSelector(state => state.register.btnDisable);
  const phone = useAppSelector(state => state.register.phone);
  const btnLoading = useAppSelector(state => state.register.btnLoading);
  const dispatch = useAppDispatch();

  const handleButton = useCallback(async () => {
    const p = prefix + phone.replace(/\D/g, ''); 
    dispatch(registerSendSms({phone: p, otp: '/dadhfbds'}))
  }, [phone]);

  const handleGoBack = useCallback(() => {
    // navigation.goBack();
    dispatch(setRegisterClear());
  }, []);

  return (
    <PageLayout>
      <HeaderComponent title={''} onLeftIconPress={handleGoBack} leftIcon={<CancelIcon stroke={'#000000'} />} />
       
      <ScrollView contentContainerStyle={[MainStyle.contentContainerStyle, MainStyle.p3]}>
        <MegaTitle style={MainStyle.mb1}>{t('login')}</MegaTitle>
        <CustomTextComponent size="large">{t('writeNumber')}</CustomTextComponent>

        <View style={[MainStyle.flexRow, MainStyle.mv4]}>
          <MegaTitle>+{prefix}</MegaTitle>
          <AuthPhoneInput />
        </View>

        <AuthPolicyCheckbox />
      </ScrollView> 

      <View style={MainStyle.p3}>
        <CustomButton loading={btnLoading} disabled={btnDisabled} onPress={handleButton} text={t('Получить код')} />
      </View>
    </PageLayout>
  );
}
