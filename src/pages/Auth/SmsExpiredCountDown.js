import React, {useEffect, useState} from 'react';
import { CustomTextComponent } from '../../components';

export const pad = (time, length) => {
  while (time?.length < length) {
    time = '0' + time;
  }
  return time;
};

export const timeFormat = time => {
  time = new Date(time);
  let h = pad(time.getUTCHours().toString(), 2);
  let m = pad(time.getUTCMinutes().toString(), 2);
  let s = pad(time.getUTCSeconds().toString(), 2);

  if (h >= 23 && m >= 59 && s > 0) {
    return '00:00';
  } else {
    return `${h < 1 ? '' : h + ':'}${m}:${h === 1 ? null : s}`;
  }
};

export const SmsExpiredCountDown = ({
  timerOffset,
  expiredTimer,
  closeTimer,
}) => {
  const [timeMilliSeconds, setTimeMilliSeconds] = useState(Date.now());

  useEffect(() => {
    let timeout = setTimeout(() => {
      setTimeMilliSeconds(Date.now());
    }, 1000);

    if (timeMilliSeconds - timerOffset > expiredTimer) {
      closeTimer();
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [closeTimer, expiredTimer, timeMilliSeconds, timerOffset]);

  return <CustomTextComponent textType='semiBold' color="dark" size="large"> {timeFormat(timerOffset + expiredTimer - timeMilliSeconds)}</CustomTextComponent>;
};
