import React from "react";
import { StyleSheet, View } from "react-native";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { AuthScreen } from ".";
import { SmsCodeScreen } from "./SmsCodeScreen";

export const AuthModal = () => {
  const dispatch = useAppDispatch();
  const isScreen = useAppSelector(state => state.register.isScreen);

  if(isScreen === 'auth') {
    return (
        <View style={styles.authModal}>
            <AuthScreen />
        </View>
      );
  }

  if(isScreen === 'sms') {
    return (
        <View style={styles.authModal}>
            <SmsCodeScreen />
        </View>
      );
  }

  return null
}

const styles = StyleSheet.create({
    authModal: {
        flex: 1,
        ...StyleSheet.absoluteFill,
        zindex: 10,
    }
});