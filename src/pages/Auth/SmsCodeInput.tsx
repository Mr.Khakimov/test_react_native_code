import { StyleSheet, TextInput } from "react-native"
import { constant } from "../../utils/theme";
import { memo, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { useTheme } from "../../customHooks/ThemeHook";
import { useNavigation } from "@react-navigation/native";
import { registerConfirmSms } from "../../redux/slices/registerSlice";
import { prefix } from "../../utils/constants";

export const SmsCodeInput = memo(() => {
    const dispatch = useAppDispatch();
    const phoneRef = useRef(null);
    const {theme} = useTheme();

    const phone = useAppSelector(state => state.register.phone);

    const [sms, setSms] = useState<string>('');

     const memoInputStyle = useMemo(() => {
       return {}
     }, [])
   
     const onChangeNumber = useCallback((e: string) => {
        setSms(e);
     }, []);

    const sendCode = () => {
      const p = prefix + phone.replace(/\D/g, '');
      dispatch(registerConfirmSms({data: {code: parseInt(sms), phone: p}}));
    }

    useEffect(() => {
      if (sms?.length > 4) {
        sendCode(sms);
      }
    }, [sms]);

    return (
        <TextInput
            ref={phoneRef}
            placeholder="•••••"
            style={[styles.input, memoInputStyle]}
            onChangeText={onChangeNumber}
            maxLength={5}
            autoFocus={true}
            value={sms}
            keyboardType="numeric"
            cursorColor={theme.textDark}
            selectionColor={theme.textDark}
            allowFontScaling={false}
        />
    )
})

const styles = StyleSheet .create({
    input: {
      fontFamily: constant.mPlusBold,
      fontSize: 32,
      flex: 1,
      paddingVertical: 0,
      lineHeight: constant.lineHeightExtra,
      paddingHorizontal: 8
    }
  });
  