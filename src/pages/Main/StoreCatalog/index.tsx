import React, { useCallback, useState } from "react";
import {  View, SafeAreaView } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation, useRoute } from "@react-navigation/native";
import { useLocalization } from "../../../customHooks/LocalizationHook";
import { MainStyle } from "../../../utils/theme";
import { TopAddressComponent } from "../TopAddressComponent";
import { IconButton, SearchButton, SectionTitle } from "../../../components";
import { FilterIcon, NotificationIcon, SearchIcon } from "../../../components/icons";
import CategoryList from "../CategyList";
import RecommendedList from "../../../components/RecommendedList";
import { flowerShopData, recommendedList } from "../../../MOCK/flowersShop";
import { cCakes, cFlowers, cSuv, cToys } from "../../../utils/images";
import { toyMarketData } from "../../../MOCK/toysMarkets";


const categoryData = [
  {
    id: '1',
    title: 'Цветы',
    gradient: ['#FFF2F5', '#FFD1DB'],
    imageSource: cFlowers,
  },
  {
    id: '2',
    title: 'Игрушки',
    gradient: ['#DEF7FF', '#BBE6FF'],
    imageSource: cToys,
  },
  {
    id: '3',
    title: 'Сладости',
    gradient: ['#FEFFDE', '#FFECA9'],
    imageSource: cCakes,
  },
  {
    id: '4',
    title: 'Сувениры',
    gradient: ['#DEFFE1', '#BBFFC2'],
    imageSource: cSuv,
  },
];


export const StoreCatalogScreen = () => {
  const navigation = useNavigation();
  const {t} = useLocalization();
  const route = useRoute();
  
  const [data, setData] = useState(
    route?.params?.category === 'flowers' 
      ? flowerShopData : route?.params?.category === 'toys' 
      ? toyMarketData : route?.params?.category === 'gifs' ? recommendedList : toyMarketData)

  const handleToNotification = useCallback(() => {
    navigation.navigate("Notification");
  }, []);

  const handleRecommendedAll = () => {

  }

  const handleSearch = () => {
    navigation.navigate("Search");
  }
  
  return (
    <SafeAreaView style={MainStyle.layout}>
      <View style={[MainStyle.flexRow, MainStyle.p3, MainStyle.aic]}>
        <TopAddressComponent />
        <IconButton onPress={handleToNotification} type="default" size="small">
            <NotificationIcon />
        </IconButton>
      </View>

      <ScrollView>
        <View style={[MainStyle.ph3, MainStyle.mb3, MainStyle.flexRow, MainStyle.aic]}>
          <View style={[MainStyle.flex]}>
            <SearchButton icon={<SearchIcon />} onPress={handleSearch} value={t("searchProductPlaceholder")}/>
         </View>
          <View style={MainStyle.ml3}>
            <IconButton onPress={handleToNotification} type="secondary" size="medium">
                <FilterIcon />
            </IconButton>
          </View>
        </View>

        {/* <View>
          <CategoryList />  
        </View>

        <View>
          <SectionTitle onPress={handleRecommendedAll} title={t("recommended")} />
          <RecommendedList data={recommendedList} />
        </View>

        <View>
          <SectionTitle onPress={handleRecommendedAll} title={t("flowers")} />
          <RecommendedList data={recommendedList} />
        </View> */}

        <View>
          <SectionTitle onPress={handleRecommendedAll} title={t("toys")} />
          <RecommendedList data={recommendedList} />
        </View>

        <View>
          <SectionTitle onPress={handleRecommendedAll} title={t("cakes")} />
          <RecommendedList data={recommendedList} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
