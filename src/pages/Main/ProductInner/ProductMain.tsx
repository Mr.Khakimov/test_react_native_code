import { StyleSheet, View, Image } from "react-native"
import { CustomTextComponent } from "../../../components"
import { MainStyle, constant } from "../../../utils/theme"
import { useTheme } from "../../../customHooks/ThemeHook"
import { useLocalization } from "../../../customHooks/LocalizationHook"
import { DeliveryCarIcon } from "../../../components/icons"
import { bonusStar } from "../../../utils/images"
import { PriceFormat } from "../../../utils/priceFormat"

export const ProductMain = ({product}) => {
    const {theme} = useTheme();
    const {t} = useLocalization();

    const colors = ['Белый', 'Желтый'];

    return (
        <View style={[MainStyle.flex, MainStyle.p3]}>
                <CustomTextComponent 
                    textType="bold"
                    color='dark'
                    size="big">
                       {product.name}
                </CustomTextComponent>

                <View style={[styles.price, {backgroundColor: theme.grey3}]}>
                    <CustomTextComponent 
                        textType="bold"
                        color='dark'
                        size="extraLarge">
                        {PriceFormat(product.price)} {t('currency')}
                    </CustomTextComponent>
                </View>

                <View style={[MainStyle.flexRow, MainStyle.aic, MainStyle.mb3]}>
                    <DeliveryCarIcon fill={theme.textDark} />
                    <CustomTextComponent
                        style={[MainStyle.ph2]} 
                        textType="bold"
                        color='dark'
                        size="extraLarge">
                        {t('delivery')} {product.deliveryTimeMinute ? product.deliveryTimeMinute : 40} {t('minute')}
                    </CustomTextComponent>
                </View>

                <View style={[MainStyle.flexRow, MainStyle.aic, MainStyle.mb4]}>
                    <Image source={bonusStar} style={styles.bonusImg}></Image>
                    <CustomTextComponent
                        style={[MainStyle.ph2]} 
                        textType="bold"
                        color='dark'
                        size="extraLarge">
                        Получите {product.bonus ? product.bonus : 5800} сум бонусов
                    </CustomTextComponent>
                </View>

                <View style={MainStyle.line}></View>

                <CustomTextComponent 
                    style={MainStyle.mt3}
                    textType="bold"
                    color='dark'
                    size="medium">
                        {t('compound')}
                </CustomTextComponent>

                <CustomTextComponent 
                    textType="regular"
                    color='dark'
                    size="medium">
                    Хризантема сантини - 25 {t('pair')}.
                </CustomTextComponent>

                <CustomTextComponent 
                    style={MainStyle.mt3}
                    textType="bold"
                    color='dark'
                    size="medium">
                        {t('color')}
                </CustomTextComponent>

                <CustomTextComponent 
                    textType="regular"
                    color='dark'
                    size="medium">
                    {colors.map(el => el + ', ')} 
                </CustomTextComponent>

                <CustomTextComponent 
                    style={MainStyle.mt3}
                    textType="bold"
                    color='dark'
                    size="medium">
                        {t('size')}
                </CustomTextComponent>

                <CustomTextComponent 
                    textType="regular"
                    color='dark'
                    size="medium">
                    {t('description')} 60 см        {t('description')} 60 см
                </CustomTextComponent>

                <CustomTextComponent 
                    style={MainStyle.mt3}
                    textType="bold"
                    color='dark'
                    size="medium">
                        {t('description')}
                </CustomTextComponent>

                <CustomTextComponent 
                    textType="regular"
                    color='dark'
                    size="medium">
                    Хризантема кустовая белая - 25 шт. Упаковка крафтАтласная лента. Цвет букета может быть незначительно изменен, в зависимости от цветка в наличии.
                </CustomTextComponent>
                
        </View>
        
    )
}


const styles = StyleSheet.create({
    price: {
        marginVertical: constant.space3,
        marginRight: 'auto',
        padding: constant.space2,
        borderRadius: constant.space2,
    },
    bonusImg: {
        width: 24,
        height: 24,
    }
});