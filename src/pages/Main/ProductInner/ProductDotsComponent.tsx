import { View , StyleSheet, Animated} from "react-native"
import { useTheme } from "../../../customHooks/ThemeHook";
import { width } from "../../../utils/constants";
import { constant } from "../../../utils/theme";

export type ProductDotsComponentType = {
    data: any;
    scrollX: any;
}

export const ProductDotsComponent = ({data, scrollX}: ProductDotsComponentType) => {
    const {theme} = useTheme()    

    return (
        <View style={style.wrapper}>
           {
            data.map((_, i) => {
                const inputRange = [(i - 1) * width, i * width, (i + 1) * width];

                const scale = scrollX.interpolate({
                    inputRange,
                    outputRange: [1, 1.6, 1],
                    extrapolate: 'clamp'
                });

                const backgroundColor = scrollX.interpolate({
                    inputRange,
                    outputRange: [theme.textWhite, theme.textDark, theme.textWhite],
                    extrapolate: 'clamp'
                });

                return <Animated.View key={`indicator-${i}`} style={[style.dots, {backgroundColor: backgroundColor, transform: [{scale}]}]}></Animated.View>
            })
           }
        </View>
    )
}

const style = StyleSheet.create({
    wrapper: {
        position: "absolute",
        bottom: constant.space2,
        right: 0,
        left: 0,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dots: {
        width: 6,
        height: 6,
        borderRadius: 50,
        marginRight: 6,
    }
});