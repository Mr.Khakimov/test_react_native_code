import React, { useCallback, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { CustomButton, CustomTextComponent, IconButton } from '../../../components';
import { constant, lightTheme } from '../../../utils/theme';
import { useLocalization } from '../../../customHooks/LocalizationHook';
import { useTheme } from '../../../customHooks/ThemeHook';
import { MinusIcon, PlusIcon } from '../../../components/icons';
import { PriceFormat } from '../../../utils/priceFormat';
import { width } from '../../../utils/constants';
import { useAppDispatch, useAppSelector } from '../../../redux/store';
import { addProduct, removeProduct, updateProduct } from '../../../redux/slices/basketSlice';
import { useNavigation } from '@react-navigation/native';

interface CounterWithPriceProps {
  initialCount: number;
  price: number;
  storeId: number | string;
  id: number | string;
}

const max = 98;
const min = 1;

export const CounterWithPrice: React.FC<CounterWithPriceProps> = ({
  price,
  storeId,
  id,
}) => {
  const {t} = useLocalization();
  const {theme} = useTheme();
  const onBasketCount = useAppSelector(state => state.basket.products.find(el => el.id === id)?.count);
  const onBas = useAppSelector(state => state.basket.products);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(onBasketCount ? onBasketCount : 1);
  const dispatch = useAppDispatch();
  const navigation = useNavigation();

  const handleIncrement = () => {
    setCount(count + 1);
  };

  const handleDecrement = () => {
    if(onBasketCount) {
      if (count > 0) {
        setCount(count - 1);
      }
    } else {
      if (count > 1) {
        setCount(count - 1);
      }
    }
  };

  const buyProduct = useCallback(() => {
    setLoading(true);
    if(onBasketCount) {
      if(count === 0) {
        dispatch(removeProduct(id))
        setCount(1);
      } else {
        dispatch(updateProduct({id, storeId, count}))
      }
    }else {
      if(count) {
        dispatch(addProduct({id, storeId, count}))
      }
    }
    setTimeout(() => {
      setLoading(false);
    }, 200)

    navigation.goBack();
  }, [count, onBasketCount]);
  
  return (
    <View style={styles.container}>
      <View style={[styles.counter, {backgroundColor: theme.grey3}]}>
        <IconButton disabled={count < min} onPress={handleDecrement} type={'default'} size="medium">
          <MinusIcon stroke={count < min ? theme.textGray : theme.textDark}  />
        </IconButton>
        <CustomTextComponent style={styles.counterText} textType='bold' color={'dark'} size='extraLarge'>{count}</CustomTextComponent>
        <IconButton disabled={count > max} onPress={handleIncrement} type={'default'} size="medium">
          <PlusIcon stroke={count > max ? theme.textGray : theme.textDark}  />
        </IconButton>
      </View>
      
      <CustomButton
        style={styles.btn}
        disabled={(!count ? onBasketCount ? false : false : !count) || loading}
        onPress={buyProduct}
        loading={loading}
        type={onBasketCount ? count ? "success" : "danger" : "success"}
        size='medium'>
         <CustomTextComponent 
          color={'white'}
          size='large'
          textType='bold'>
            {count ? onBasketCount ? t('ready') : t('add') : t('remove')}
        </CustomTextComponent>
        <CustomTextComponent 
          color={'white'}
          size='large'
          textType='bold'>
            {PriceFormat(price * count)} {t('currency')}
        </CustomTextComponent>
      </CustomButton>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderTopColor: lightTheme.grey3,
    borderTopWidth: 1,
  },
  counter: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: constant.space2,
    borderRadius: constant.space2
  },
  counterText: {
    width: 25,
    textAlign: 'center'
  },
  btn: {
    width: width / 1.7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: constant.space2,
  },
  button: {
    width: 32,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#eee', // Customize button background color
    borderRadius: 16,
  },
  count: {
    fontSize: 18,
  },
  cardButton: {
    backgroundColor: 'blue', // Customize card button background color
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  price: {
    color: 'white', // Customize price text color
    fontSize: 16,
    marginRight: 8,
  },
});
