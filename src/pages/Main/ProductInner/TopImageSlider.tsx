import React, { useCallback, useRef } from "react";
import { FlatList, StyleSheet, View, Animated, NativeSyntheticEvent, NativeScrollEvent} from "react-native";
import { useTheme } from "../../../customHooks/ThemeHook";
import { width } from "../../../utils/constants";
import { MainStyle, constant, lightTheme } from "../../../utils/theme";
import { ProductDotsComponent } from "./ProductDotsComponent";
import { CustomTextComponent, TagComponent } from "../../../components";
import { useLocalization } from "../../../customHooks/LocalizationHook";
import { StarIcon } from "../../../components/icons";
import { ProductHeaderComponent } from "./ProductHeaderComponent";

const data = [
    {id: 1, img: 'https://t4.ftcdn.net/jpg/01/67/26/17/360_F_167261700_YAbOGlysBYOJ7NiHWyeCKvPSETH6UG8t.jpg'},
    {id: 3, img: 'https://www.thedenverear.com/wp-content/uploads/2020/01/Thumbnail.png'},
    {id: 4, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKXHszSyzIpD6ukCIfcPEUXYMJvg4omSgJa7ZPnbBigPwO3VWBrDqboZW0jZmRoFUUq8s&usqp=CAU'},
]

export const TopImageSlider = ({product}) => {
    const scrollX = useRef(new Animated.Value(0)).current;
    const flatList = useRef<FlatList>(null);
    const {theme} = useTheme();
    const {t} = useLocalization();

    const onScroll = useCallback(
        Animated.event<NativeSyntheticEvent<NativeScrollEvent>>(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
            { useNativeDriver: false }
    ),[]);

    const renderItem = useCallback(({item}:any) => {
        return (
            <Animated.View style={[style.item]}>
                <Animated.Image 
                    style={[MainStyle.img]} 
                    source={{uri: item.img}} />
            </Animated.View>
        )
    }, []);

    return (
        <View style={[style.wrapper]}>
            <ProductHeaderComponent />

            <Animated.FlatList
                ref={flatList} 
                data={data}
                horizontal={true}
                pagingEnabled={true}
                scrollEventThrottle={32}
                decelerationRate={0}
                snapToInterval={width}
                onScroll={onScroll}
                bounces={false}
                showsHorizontalScrollIndicator={false}
                renderItem={renderItem}
                contentContainerStyle={[MainStyle.contentContainerStyle]}
                keyExtractor={(item) => String(item.id)}
            />

            <ProductDotsComponent data={data} scrollX={scrollX} />

            <View style={style.size}>
                <CustomTextComponent color={'white'} size='large' textType='bold'>
                   60x60 {t('sm')}
                </CustomTextComponent>
            </View>

            <View style={style.reviews}>
                <StarIcon />
                <CustomTextComponent style={MainStyle.mh1} color={'dark'} size='medium' textType='bold'>
                    {product.rate}
                </CustomTextComponent>
                <TagComponent backgroundColor={theme.grey3} textColor="dark" textType="regular" size="medium" text={`${product.reviewsCount} ${t('reviews')}`} />
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    wrapper: {
        height: 200,
        position: 'relative',
    },
    item: {
        width,
    },
    img: {
        width: '100%',
        height: '100%',
        objectFit: 'contain'
    },
    size: {
        position: 'absolute',
        bottom: constant.space2,
        right: constant.space2,
        backgroundColor: lightTheme.textDark,
        padding: constant.space1,
        borderRadius:  constant.space2,
    },
    reviews: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        position: 'absolute',
        bottom: 0,
        minWidth: 88,
        left: constant.space2,
        backgroundColor: lightTheme.textWhite,
        padding: constant.space1,
        borderTopEndRadius:  constant.space2,
        borderTopStartRadius:  constant.space2,
    },
});