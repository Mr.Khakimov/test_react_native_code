import { useCallback, useState } from "react"
import { SectionTitle } from "../../../components"
import { useLocalization } from "../../../customHooks/LocalizationHook";
import { StyleSheet, View } from "react-native";
import { MainStyle } from "../../../utils/theme";
import ProductItem from "../ProductItem";
import { ordersData } from "../../../MOCK/otherProducts";
import { width } from "../../../utils/constants";

export const OtherProducts = () => {
    const {t} = useLocalization();

    const [data, setData] = useState(ordersData.slice(1, 5));

    const handleOtherAll = useCallback(() => {
        
    }, []);

    const addToCard = () => {
        
    }

    const handleCategoryPress = () => {

    }

    return (
        <View style={MainStyle.pt3}>
            <SectionTitle onPress={handleOtherAll} title={t("otherProducts")} />

            <View style={styles.row}>
                {
                    data.map(el => {   
                        return (
                            <View key={el.id} style={styles.product}>
                                <ProductItem
                                    key={el.id}
                                    title={el.name}
                                    imageSource={el.img}
                                    price={el.price}
                                    deliveryTime={el.deliveryTime}
                                    rate={el.rate}
                                    otherProduct={true}
                                    buyProduct={() => addToCard(el.id)}
                                    onPress={() => handleCategoryPress(el.id)}
                                />
                            </View>
                        )
                    })
                }
            </View>
            
           
        </View>
    )
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: 8,
    },
    product: {
        width: (width / 2) - 8,
        paddingHorizontal: 4,
        marginBottom: 8,
    }
});