import React, { useEffect } from "react";
import {  SafeAreaView, View , ScrollView, StyleSheet} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useLocalization } from "../../../customHooks/LocalizationHook";
import { MainStyle } from "../../../utils/theme";
import { ProductMain } from "./ProductMain";
import { TopImageSlider } from "./TopImageSlider";
import { CounterWithPrice } from "./CounterWithPrice";
import { ProductHeaderComponent } from "./ProductHeaderComponent";
import { Divider } from "../../../components/Divider";
import { Reviews } from "../../Reviews/Reviews";
import { OtherProducts } from "./OtherProducts";
import { ToBasketBtn } from "../../../components/ToBasketBtn";
import { isIos } from "../../../utils/constants";
import { useFindProduct } from "../../../customHooks/ProductFilterHook";
import { useAppDispatch } from "../../../redux/store";
import { showBottomBasketLink } from "../../../redux/slices/basketSlice";
import { Portal } from "@gorhom/portal";
import {getFocusedRouteNameFromRoute} from '@react-navigation/native';

export const ProductInnerScreen = ({route, navigation}) => {
  const id = route.params.id;
  const product = useFindProduct(id);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(showBottomBasketLink(true));
  }, []);

  // React.useLayoutEffect(() => {
  //   const routeName = getFocusedRouteNameFromRoute(route);
  //   console.log("routeName", route.name);
    
  //   if (route.name === 'ProductInfoPage') {
  //     navigation.setOptions({tabBarVisible: false});
  //   } else {
  //     navigation.setOptions({tabBarVisible: true});
  //   }
  // }, [navigation, route]);

  const handleBasketLink = () => {
    navigation.goBack();
    navigation.navigate("BasketScreen");
    dispatch(showBottomBasketLink(false))
  }
  
  return (
    <SafeAreaView style={MainStyle.layout}>
      
      <ScrollView contentContainerStyle={styles.scrollView}>

        <TopImageSlider product={product} />

        <ProductMain product={product} />

        <Divider /> 

        <Reviews productId={id} count={product?.reviewsCount} />

        <Divider /> 

        <OtherProducts />

      </ScrollView>
          
      <CounterWithPrice id={id} storeId={product.storeId} price={parseFloat(product.price)} />

      {isIos ? <ToBasketBtn handleBasketLink={handleBasketLink} style={styles.toBasketBtn} /> : null}

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  scrollView: {paddingBottom: 70},
  toBasketBtn: {bottom: 100}
});