import React from "react"
import { Alert, Share, StyleSheet, View } from "react-native"
import { IconButton } from "../../../components"
import { CancelIcon, LeftChevronIcon, LikeIcon, ShareIcon } from "../../../components/icons"
import { useCallback } from "react"
import { useNavigation } from "@react-navigation/native"
import { MainStyle, constant } from "../../../utils/theme"

export const ProductHeaderComponent = () => {
    const navigation = useNavigation();
    const goBack = useCallback(() => {
        navigation.goBack();
    }, []);

    const handleShare = useCallback(async () => {
      try {
        const result = await Share.share({
          message:
            'Букет из 25 хризантем сантини, Хризантема кустовая белая - 25 шт. Упаковка крафтАтласная лента. Цвет букета может быть незначительно изменен, в зависимости от цветка в наличии.',
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error: any) {
        Alert.alert(error.message);
      }
    }, []);

    const handleLike = useCallback(() => {

    }, []);

    return (
      <View style={styles.wrapper}>
        <IconButton onPress={goBack} type={'white'} size="small">
          <CancelIcon height={16} width={16} />
        </IconButton>

        <View style={styles.rightWrapper}>
          <IconButton onPress={handleLike} style={MainStyle.mr2} type={'white'} size="small">
            <LikeIcon height={16} width={16} />
          </IconButton>
          <IconButton onPress={handleShare} type={'white'} size="small">
            <ShareIcon height={20} width={20} />
          </IconButton>
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: 60,
        zIndex: 10,
        flexDirection: 'row',
        padding: constant.space3,
        justifyContent: 'space-between'
    },
    rightWrapper: {
      flexDirection: 'row',
    }
});