import React from 'react';
import {FlatList, View } from 'react-native';
import CategoryItem from './CategoryItem';
import { cCakes, cFlowers, cSuv, cToys } from '../../utils/images';
import { MainStyle } from '../../utils/theme';
import { useNavigation } from '@react-navigation/native';

const categoryData = [
  {
    id: '1',
    title: 'Цветы',
    gradient: ['#FFF2F5', '#FFD1DB'],
    imageSource: cFlowers,
    category: "flowers",
  },
  {
    id: '2',
    title: 'Игрушки',
    gradient: ['#DEF7FF', '#BBE6FF'],
    imageSource: cToys,
    category: "toys",
  },
  {
    id: '3',
    title: 'Сладости',
    gradient: ['#FEFFDE', '#FFECA9'],
    imageSource: cCakes,
    category: "cakes",
  },
  {
    id: '4',
    title: 'Сувениры',
    gradient: ['#DEFFE1', '#BBFFC2'],
    imageSource: cSuv,
    category: "gifs",
  },
];

const CategoryList: React.FC = () => {
  const navigation = useNavigation()

  const renderItem = ({ item } :any) => (
    <CategoryItem
      title={item.title}
      imageSource={item.imageSource}
      gradient={item.gradient}
      onPress={() => handleCategoryPress(item.category)}
    />
  );

  const handleCategoryPress = (category: string) => {
    navigation.navigate('ProductPage', {category: category});
  };

  return (
    
      <FlatList
          data={categoryData}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{...MainStyle.pl2, flexGrow: 1}}
        />
      
  );
};

export default CategoryList;