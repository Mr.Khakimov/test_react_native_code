import React from 'react';
import {FlatList } from 'react-native';
import { MainStyle } from '../../utils/theme';
import { useNavigation } from '@react-navigation/native';
import ProductItem from './ProductItem';
import { ordersData } from '../../MOCK/otherProducts';

const ProductList: React.FC = ({storeId}) => {
  const navigation = useNavigation()

  const renderItem = ({ item } :any) => (
    <ProductItem
      title={item.name}
      imageSource={item.img}
      gradient={item.gradient}
      price={item.price}
      rate={item.rate}
      buyProduct={() => addToCard(item.id)}
      onPress={() => handleCategoryPress(item.id)}
    />
  );

  const handleCategoryPress = (categoryId: string) => {
    navigation.navigate('ProductInfoPage', {id: categoryId});
  };

  const addToCard = (categoryId: string) => {
    navigation.navigate('ProductInfoPage', {id: categoryId});
    console.log(`Add to card ${categoryId} pressed`);
  }

  return (
      <FlatList
        data={ordersData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={MainStyle.pl2}
      />
  );
};

export default ProductList;