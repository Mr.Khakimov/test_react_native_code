import { Image, StyleSheet, Text, View } from "react-native"
import { CustomButton, CustomTextComponent, IconButton } from "../../components"
import { ArrowRightDarkIcon, FilterIcon, RightChevronIcon } from "../../components/icons"
import { MainStyle, constant } from "../../utils/theme"
import { bonusStar } from "../../utils/images"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { ScrollView } from "react-native-gesture-handler"

export const AllMarkets = () => {
    const onPress = () => {}
    const onButtonPress = () => {}

    const {t} = useLocalization();

    return (
        <>
        <ScrollView horizontal style={[MainStyle.ph3, MainStyle.pb3, MainStyle.flexRow]}>
            <IconButton style={MainStyle.mr1} onPress={onPress} type="secondary" size="large">
                <FilterIcon />
            </IconButton>

            <View style={MainStyle.mr1}>
                <CustomButton type="secondary" onPress={onButtonPress}>
                    <View style={styles.btnWrapper}>
                        <CustomTextComponent textType="regular" size="large" color="dark">
                            <View style={styles.imgWrapper}>
                                <Image style={styles.bonusImg} source={bonusStar} />
                            </View>
                            <CustomTextComponent textType="regular" size="large" color="dark">{t('rating')}</CustomTextComponent>
                        </CustomTextComponent>
                        <RightChevronIcon />
                    </View>
                </CustomButton>
            </View>

            <View style={MainStyle.mr1}>
                <CustomButton type="secondary" onPress={onButtonPress}>
                    <View style={styles.btnWrapper}>
                        <CustomTextComponent textType="regular" size="large" color="dark">{t('sales')}</CustomTextComponent>
                        <RightChevronIcon />
                    </View>
                </CustomButton>
            </View>

            <View style={MainStyle.mr1}>
                <CustomButton type="secondary" onPress={onButtonPress}>
                    <View style={styles.btnWrapper}>
                        <CustomTextComponent textType="regular" size="large" color="dark">{t('price')}</CustomTextComponent>
                        <RightChevronIcon />
                    </View>
                </CustomButton>
            </View>

            <View style={MainStyle.mr1}>
                <CustomButton type="secondary" onPress={onButtonPress}>
                    <View style={styles.btnWrapper}>
                        <CustomTextComponent textType="regular" size="large" color="dark">{t('deliveryFilter')}</CustomTextComponent>
                        <RightChevronIcon />
                    </View>
                </CustomButton>
            </View>
        </ScrollView>

        <ScrollView>
            
        </ScrollView>
        </>
        
    )
}

const styles = StyleSheet.create({
    btnWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: constant.space2,
    },
    imgWrapper: {
        width: 18,
        height: 18,
        marginBottom: -4,
    },
    bonusImg: {
        width: '100%',
        height: '100%',
        // marginBottom: -4,
        // marginRight: 4,
    }
});