import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, ImageSourcePropType } from 'react-native';
import { constant } from '../../utils/theme';
import { CustomTextComponent } from '../../components';
import LinearGradient from 'react-native-linear-gradient';

interface ProductCategoryItemProps {
  title: string;
  imageSource: ImageSourcePropType; // Replace with the appropriate image source type
  onPress: () => void;
}

const ProductCategoryItem: React.FC<ProductCategoryItemProps> = ({ title, imageSource, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
        <View style={styles.imageWrapper}>
          <Image source={imageSource} style={styles.image} />
        </View>

        <View style={styles.textContainer}>
          <CustomTextComponent textType="bold" size="small" color="dark">{title}</CustomTextComponent>
        </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginRight: 8,
    // borderRadius: 18,
    width: 76,
    // height: 74,
    alignItems: 'center',
  },
  imageWrapper: {
    position: 'relative',
    borderRadius: 18,
    width: 76,
    height: 76,
    marginBottom: constant.space1,
    backgroundColor: '#f1f2f3',
    overflow: 'hidden',
  },
  image: {
    position: 'absolute',
    bottom: -5,
    left: 0,
    right: 0,
    width: '100%',
    height: 68,
    objectFit: 'contain'
  },
  textContainer: {
    marginBottom: 6,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: constant.space1,
  },
});

export default ProductCategoryItem;