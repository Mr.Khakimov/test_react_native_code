import React, { useCallback, useEffect } from "react"
import { PermissionsAndroid, View } from "react-native"
import { CustomTextComponent } from "../../components"
import { MainStyle } from "../../utils/theme"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { isIos } from "../../utils/constants"
import Geolocation from 'react-native-geolocation-service';

export const handlePermission = () => {
  (async function loc() {
    if (isIos) {
      Geolocation.requestAuthorization('always')
        .then(r => {
          // locServiceStop.then(r => watchGeolocationStart());
          // store.dispatch(locationPermissions(true));
          console.log('location success', r);
        })
        .catch(e => {
          console.log('location err', e);
        });
      console.log('location isIos', isIos);
      return true;
    }
    let locCheck = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (locCheck === PermissionsAndroid.RESULTS.GRANTED) {
      // locServiceStop.then(r => watchGeolocationStart());
      // store.dispatch(locationPermissions(true));
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'permissionLocationTitle',
            message: 'permissionLocationTxt',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // locServiceStop.then(r => watchGeolocationStart());
          // store.dispatch(locationPermissions(true));
        } else {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'permissionLocationTitle',
              message: 'permissionLocationTxt',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            // store.dispatch(locationPermissions(true));
          } else {
            // navigation.navigate('LocationScreen');
            // openAppSetting();
            // store.dispatch(locationPermissions(false));
          }
        }
      } catch (err) {
        console.log('handle permission err', err);
        // store.dispatch(locationPermissions(false));
      }
    }
  })();
};


export const TopAddressComponent = () => {
    const {t} = useLocalization();

    useEffect(() => {
      handlePermission();
    }, []);
    
    return (
        <View style={[MainStyle.pr3, MainStyle.flex]}>
          <CustomTextComponent style={{marginBottom: 4}} textType="regular" size="medium" color="gray">{t('deliveryAddr')}</CustomTextComponent>
          <CustomTextComponent textType="bold" size="large" color="dark">ул. Мустакиллик, 29</CustomTextComponent>    
        </View>
    )
}