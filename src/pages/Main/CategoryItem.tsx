import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, ImageSourcePropType } from 'react-native';
import { constant } from '../../utils/theme';
import { CustomTextComponent } from '../../components';
import LinearGradient from 'react-native-linear-gradient';

interface CategoryItemProps {
  title: string;
  imageSource: ImageSourcePropType;
  onPress: () => void;
  gradient: Array<string>;
}

const CategoryItem: React.FC<CategoryItemProps> = ({ title, imageSource, gradient = ['#FFF2F5', '#FFD1DB'], onPress }) => {
  return (
      <TouchableOpacity onPress={onPress} style={styles.container}>
        <LinearGradient style={styles.linearGradient} colors={gradient} start={{x: 1, y: 0}} end={{x: 1, y: 1}}>
          <View style={styles.textContainer}>
            <CustomTextComponent textType="bold" size="large" color="dark">{title}</CustomTextComponent>
          </View>
          <View style={styles.imageWrapper}>
            <Image source={imageSource} style={styles.image} />
          </View>
        </LinearGradient>
      </TouchableOpacity>
   
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginRight: 8,
    borderRadius: 18,
    width: 104,
    height: 120,
  },
  linearGradient: {
    borderRadius: 18,
    width: 104,
    height: 120,
    paddingTop: constant.space2,
    paddingHorizontal: constant.space2,
  },
  imageWrapper: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center'
  },
  image: {
    width: '100%',
    height: 78,
    objectFit: 'contain'
  },
  textContainer: {
    // alignItems: 'center',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default CategoryItem;