import React from 'react';
import {FlatList } from 'react-native';
import CategoryItem from './CategoryItem';
import { fw_1, fw_2, fw_3, fw_4 } from '../../utils/images';
import { MainStyle } from '../../utils/theme';
import { useNavigation } from '@react-navigation/native';
import ProductCategoryItem from './ProductCategoryItem';

const categoryData = [
  {
    id: '1',
    title: 'Букеты',
    imageSource: fw_1,
  },
  {
    id: '2',
    title: 'Цветы в коробке',
    imageSource: fw_2,
  },
  {
    id: '3',
    title: 'Цветы в корзине',
    imageSource: fw_3,
  },
  {
    id: '4',
    title: 'Цветы в горшках',
    imageSource: fw_4,
  },
  {
    id: '5',
    title: 'Цветы в коробке',
    imageSource: fw_2,
  },
  {
    id: '6',
    title: 'Цветы в корзине',
    imageSource: fw_1,
  },
];

const ProductCategoryList: React.FC = () => {
  const navigation = useNavigation()

  const renderItem = ({ item } :any) => (
    <ProductCategoryItem
      title={item.title}
      imageSource={item.imageSource}
      onPress={() => handleCategoryPress(item.id)}
    />
  );

  const handleCategoryPress = (categoryId: string) => {
    navigation.navigate('ProductPage');
    console.log(`Category ${categoryId} pressed`);
  };

  return (
      <FlatList
        data={categoryData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={MainStyle.pl2}
      />
  );
};

export default ProductCategoryList;