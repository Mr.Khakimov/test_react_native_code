import React from "react";
import {  View, SafeAreaView, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { CustomButton, CustomInput,  CustomTextComponent,  CustomTitle,  IconButton, SectionTitle, TagComponent } from "../../components";
import { FavoriteIcon, LikeIcon, FilterIcon, LeftChevronIcon, NotificationIcon, SearchIcon } from "../../components/icons";
import { MainStyle, constant, lightTheme } from "../../utils/theme";
import CategoryList from "./CategyList";
import { ScrollView } from "react-native-gesture-handler";
import { TopAddressComponent } from "./TopAddressComponent";
import { useLocalization } from "../../customHooks/LocalizationHook";
import RecommendedList from "../../components/RecommendedList";
import HeaderComponent from "../../components/layaouts/HeaderComponent";
import CustomTitleComponent from "../../components/TitleComponent";
import { useTheme } from "../../customHooks/ThemeHook";
import { bonusStar } from "../../utils/images";
import { width } from "../../utils/constants";
import ProductCategoryList from "./ProductCategoryList";
import ProductList from "./ProductList";
import { DeliveryAlertComponent } from "./DeliveryAlertComponent";
import { useFindStore } from "../../customHooks/ProductFilterHook";

const dataCategory = ["Цветы", "Букеты", "Подарки"];

export const StoreScreen = ({navigation, route}) => {
  const {t} = useLocalization();
  const {theme} = useTheme();
  const {storeId} = route.params;

  const store = useFindStore(storeId);

  const handleToNotification = () => {
    // navigation.navigate('TaskPageThree');
  }

  const handleRecommendedAll = () => {
    navigation.navigate('StoreCatalog')
  }  

  const goBack = () => {
    navigation.goBack();
  }

  const handleNotification = () => {
    // navigation.navigate("NatificationScreen")
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
      <View>
        <HeaderComponent 
          leftIcon={<LeftChevronIcon />}
          onLeftIconPress={goBack}
          title={
            <>
              <TopAddressComponent />
            </>
          }
          rightIcon={<NotificationIcon />}
          onRightIconPress={handleNotification}
          />
      </View>

      <ScrollView>

          <View style={[MainStyle.p3, MainStyle.jsc, MainStyle.aic]}>
            <View style={styles.imgWrapper}>
              <Image source={typeof store.img === "number" ? store.shopIcon : {uri: store.img}} style={{width: 100, height: 100}}></Image>
            </View>
            <CustomTitle style={MainStyle.pb1}>{store.title}</CustomTitle>
            <View style={[MainStyle.rowBetween, MainStyle.mb1]}>

              {
                dataCategory.map((el, index) => <View key={el} style={{paddingHorizontal: 4, borderRightColor: theme.gray, borderRightWidth: index === dataCategory.length - 1 ? 0 : 1}}>
                  <CustomTextComponent size="small" color="gray">{el}</CustomTextComponent>
                </View>)
              }
              
            </View>

            <View style={[MainStyle.rowBetween, MainStyle.flex]}>
              <TagComponent
                key={'112'}
                text={<>
                   <Image style={{width: 16, height: 16, marginBottom: -2}} source={bonusStar} />
                    <CustomTextComponent size="large" textType="semiBold" color="white">
                      {store.rate}
                    </CustomTextComponent>
                </>} 
                backgroundColor={theme.primaryColor} 
                textColor="white"
              />
            
                
                <TagComponent
                  key={'113'}
                  style={[MainStyle.mh1]} 
                  textType="semiBold"  
                  text={t("delivery")} 
                  backgroundColor={theme.green} 
                  textColor="white"
                /> 
          
              <TagComponent
                key={'114'}
                textType="semiBold"  
                text={'3% бонуса'} 
                backgroundColor={theme.textDark} 
                textColor="white"
              />
            </View>

          </View>

          <View style={[MainStyle.rowBetween, MainStyle.ph3, MainStyle.mv1]}>
            <View style={[MainStyle.flex]}>
              <CustomInput icon={<SearchIcon />} placeholder={t("searchFromMarket")}></CustomInput>
            </View>
            <View style={MainStyle.ml1}>
              <IconButton onPress={handleToNotification} type="secondary" size="medium">
                  <LikeIcon width="24" height="24" />
              </IconButton>
            </View>
          </View>

          <ProductCategoryList />

          <SectionTitle key={'1222'} onPress={handleRecommendedAll} title={t("Букеты")} />
          <ProductList key={'23423'} storeId={storeId} />
          <SectionTitle key={'1212'} onPress={handleRecommendedAll} title={t("Букеты")} />
          <ProductList key={'123123'} storeId={storeId} />

      </ScrollView>

      <DeliveryAlertComponent />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  imgWrapper: {
    width: 100,
    height: 100,
    backgroundColor: lightTheme.grey3,
    borderRadius: 50,
    overflow: 'hidden',
    marginBottom: constant.space1
  }
});
