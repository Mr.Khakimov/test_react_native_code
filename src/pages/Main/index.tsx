import React, { useCallback, useEffect } from "react";
import {  View, SafeAreaView } from "react-native";
import { CustomInput,  IconButton, SearchButton, SectionTitle } from "../../components";
import { FilterIcon, NotificationIcon, SearchIcon } from "../../components/icons";
import { MainStyle } from "../../utils/theme";
import CategoryList from "./CategyList";
import { ScrollView } from "react-native-gesture-handler";
import { TopAddressComponent } from "./TopAddressComponent";
import { useLocalization } from "../../customHooks/LocalizationHook";
import RecommendedList from "../../components/RecommendedList";
import { getFocusedRouteNameFromRoute, useNavigation, useRoute } from "@react-navigation/native";
import { flowerShopData, recommendedList } from "../../MOCK/flowersShop";
import { Portal } from "@gorhom/portal";
import { ToBasketBtn } from "../../components/ToBasketBtn";
import { showBottomBasketLink } from "../../redux/slices/basketSlice";
import { useAppDispatch } from "../../redux/store";
import { toyMarketData } from "../../MOCK/toysMarkets";

export const MainScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const {t} = useLocalization();
  const dispatch = useAppDispatch();

  const handleToNotification = useCallback(() => {
    navigation.navigate("Notification");
  }, []);

  const handleRecommendedAll = (value) => {
    navigation.navigate('ShopsScreen', {value})
  }

  const handleSearch = () => {
    navigation.navigate("Search");
  }

  const handleBasketLink = useCallback(() => {
    navigation.navigate("BasketScreen");
    dispatch(showBottomBasketLink(false))
  }, [])

  useEffect(() => {
    dispatch(showBottomBasketLink(true))
  }, [])

  React.useEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    console.log("routeName", route.name);
    navigation.setOptions({tabBarStyle: { position: 'absolute' }});
    if (route.name === 'ProductInfoPage') {
      navigation.setOptions({tabBarVisible: false});
    } else {
      navigation.setOptions({tabBarVisible: true});
    }
  }, [navigation, route]);
  
  return (
    <SafeAreaView style={MainStyle.layout}>
      <View style={[MainStyle.flexRow, MainStyle.p3, MainStyle.aic]}>
        <TopAddressComponent />
        <IconButton onPress={handleToNotification} type="default" size="small">
            <NotificationIcon />
        </IconButton>
      </View>

      <ScrollView contentContainerStyle={MainStyle.bottomPadding}>
        <View style={[MainStyle.ph3, MainStyle.mb3, MainStyle.flexRow, MainStyle.aic]}>
          <View style={[MainStyle.flex]}>
            <SearchButton icon={<SearchIcon />} onPress={handleSearch} value={t("searchProductPlaceholder")}/>
         </View>
          <View style={MainStyle.ml3}>
            <IconButton onPress={handleToNotification} type="secondary" size="medium">
                <FilterIcon />
            </IconButton>
          </View>
        </View>

        <View>
          <CategoryList />  
        </View>

        <View>
          <SectionTitle value="recommended" onPress={handleRecommendedAll} title={t("recommended")} />
          <RecommendedList data={recommendedList} />
        </View>

        <View>
          <SectionTitle value="flowers" onPress={handleRecommendedAll} title={t("flowers")} />
          <RecommendedList  data={flowerShopData.slice(0, 3)} />
        </View>

        <View>
          <SectionTitle value="toys" onPress={handleRecommendedAll} title={t("toys")} />
          <RecommendedList data={toyMarketData.slice(0, 4)} />
        </View>

        {/* <View>
          <SectionTitle onPress={handleRecommendedAll} title={t("toys")} />
          <RecommendedList data={toyMarketData.slice(0, 7)} />
        </View> */}
      </ScrollView>

      <Portal hostName="BasketHost">
        <ToBasketBtn handleBasketLink={handleBasketLink} />
      </Portal>
    </SafeAreaView>
  );
}
