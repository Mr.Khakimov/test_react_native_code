import React, { useMemo, useState } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, ImageSourcePropType } from 'react-native';
import { MainStyle, constant, lightTheme } from '../../utils/theme';
import { CustomButton, CustomTextComponent, IconRoundedButton } from '../../components';
import { width } from '../../utils/constants';
import { LikeFilledIcon, LikeIcon, StarIcon, TimeIcon} from '../../components/icons';
import { useTheme } from '../../customHooks/ThemeHook';

interface CategoryItemProps {
  title: string;
  imageSource: ImageSourcePropType;
  onPress: () => void;
  buyProduct?: () => void;
  gradient: Array<string>;
  otherProduct: boolean;
  deliveryTime: number | string;
  rate: number | string;
  price: number | string;
}

const ProductItem: React.FC<CategoryItemProps> = ({ title, deliveryTime, rate = '4.9', price, otherProduct = false, onPress, buyProduct = () => {onPress()}, imageSource }) => {
    const [liked, setLiked] = useState(false);
    const {theme} = useTheme()

    const productPriceMemo = useMemo(() => {
      return {
        backgroundColor: otherProduct ? 'transparent' : theme.grey3
      }
    }, [])

    const productContainerMemo = useMemo(() => {
      return {
        width: otherProduct ? '100%' : width / 2.7
      }
    }, []);

    return (
        <TouchableOpacity onPress={onPress} style={[styles.container, productContainerMemo]}>
            <View style={styles.img}>
                <Image source={{uri: imageSource}} style={MainStyle.img} />
            </View>

            <View style={styles.main}>
                <View style={styles.reviews}>
                    <View style={styles.starIcon}><StarIcon /></View>
                    <CustomTextComponent color="dark" textType='bold'>{rate}</CustomTextComponent>
                </View>

                <CustomTextComponent numberOfLines={2} color="dark">{title}</CustomTextComponent>
                
                <View>
                  {
                    otherProduct 
                      ? 
                      
                      <View style={[styles.priceOtherProduct]}>
                          <View style={styles.priceOtherProductDelivery}>
                            <TimeIcon />
                            <CustomTextComponent textType='regular' color="gray"> ≈{deliveryTime} часа</CustomTextComponent> 
                          </View>
                          
                          <CustomTextComponent textType='bold' color="dark">{price} сум</CustomTextComponent>
                      </View>
                      
                      : 

                      <View style={[styles.price, productPriceMemo]}>
                          <CustomTextComponent textType='bold' color="dark">{price} сум</CustomTextComponent>
                      </View>

                  }

                </View>                
                {
                  !otherProduct ? <CustomButton onPress={buyProduct} type='success' size='medium' text={'Купить'}></CustomButton> : null
                }
            </View>

            <IconRoundedButton 
                style={styles.likeBtn} 
                onPress={() => {
                    setLiked(!liked)
                }}
                size="small">
                {!liked ? <LikeIcon /> : <LikeFilledIcon />}
            </IconRoundedButton>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 16,
    borderWidth: 1,
    borderColor: lightTheme.grey3,
    marginRight: constant.space1,
    overflow: 'hidden',
  },
  priceOtherProduct: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: constant.space1,
  },
  priceOtherProductDelivery: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  main: {
    position: 'relative',
    padding: constant.space2,
    justifyContent: 'center',
  },
  img: {
    height: 127,
    alignItems: 'center',
    backgroundColor: lightTheme.grey3,
  },
  price: {
    padding: constant.space1,
    borderRadius: constant.space2,
    marginVertical: constant.space1,
  },
  likeBtn: {
    position: 'absolute',
    top: constant.space1,
    right: constant.space1,
  },
  starIcon: {
    marginRight: 4,
    marginTop: 1.5,
  },
  reviews: {
    position: 'absolute',
    top: -28,
    height: 48,
    left: constant.space1,
    padding: constant.space1,
    backgroundColor: lightTheme.textWhite,
    borderTopEndRadius: constant.space2,
    borderTopStartRadius: constant.space2,
    flexDirection: 'row',
  },
});

export default ProductItem;