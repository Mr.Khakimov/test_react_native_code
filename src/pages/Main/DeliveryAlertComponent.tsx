import React from "react";
import {View, StyleSheet} from 'react-native';
import { CustomTextComponent } from "../../components";
import { ChevronRightWhiteIcon, DeliveryCarIcon } from "../../components/icons";
import { useTheme } from "../../customHooks/ThemeHook";
import { MainStyle, constant } from "../../utils/theme";
import { useLocalization } from "../../customHooks/LocalizationHook";

export const DeliveryAlertComponent = () => {
    const {theme} = useTheme();
    const {t} = useLocalization();
    
    return (
        <View style={[styles.container, {backgroundColor: theme.textDark}]}>
            <DeliveryCarIcon />
          
            <View style={[MainStyle.flex, MainStyle.ph3]}>
                <CustomTextComponent textType="bold" size="large" color='white'>{t('delivery')} 30-40 {t('minute')}</CustomTextComponent>
                <CustomTextComponent color='white'>{t('minimalOrderFrom')} 100 000 {t("currency")}</CustomTextComponent>
            </View>
           
            <ChevronRightWhiteIcon />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: constant.space2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
});