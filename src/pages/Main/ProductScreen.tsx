import React, { useState } from "react";
import {  View, SafeAreaView } from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native";
import { CustomInput,  IconButton, SectionTitle } from "../../components";
import { FilterIcon, LeftChevronIcon, NotificationIcon, SearchIcon } from "../../components/icons";
import { MainStyle } from "../../utils/theme";
import CategoryList from "./CategyList";
import { ScrollView } from "react-native-gesture-handler";
import { TopAddressComponent } from "./TopAddressComponent";
import { useLocalization } from "../../customHooks/LocalizationHook";
import RecommendedList from "../../components/RecommendedList";
import HeaderComponent from "../../components/layaouts/HeaderComponent";
import ProductCategoryList from "./ProductCategoryList";
import { width } from "../../utils/constants";
import { AllMarkets } from "./AllMarkets";
import { flowerShopData, recommendedList } from "../../MOCK/flowersShop";
import { toyMarketData } from "../../MOCK/toysMarkets";

export const ProductScreen = () => {
  const {t} = useLocalization();
  const navigation = useNavigation();
  const route = useRoute();
  
  const [data, setData] = useState(
    route?.params?.category === 'flowers' 
      ? flowerShopData : route?.params?.category === 'toys' 
      ? toyMarketData : route?.params?.category === 'gifs' ? recommendedListt : toyMarketData)


  const handleRecommendedAll = () => {

  }

  const goBack = () => {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
        <HeaderComponent titleStyle={MainStyle.tac} leftIcon={<LeftChevronIcon />} onLeftIconPress={goBack} title={t(route?.params?.category)} />

        <ScrollView contentContainerStyle={MainStyle.bottomPadding}>
            {route?.params?.category === 'flowers' ? <ProductCategoryList /> : null}
            <View>
                <SectionTitle onPress={handleRecommendedAll} title={t("recommended")} />
                <RecommendedList data={recommendedList} />
            </View>

            <View>
                <SectionTitle title={t("Все магазины")} />
                <AllMarkets></AllMarkets>
                <RecommendedList data={data} containerWidth={width * 0.8}  vertical={false} />
            </View>
        </ScrollView>
    </SafeAreaView>
  );
}
