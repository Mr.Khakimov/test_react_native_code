import React from 'react';
import {useSelector, useDispatch} from "react-redux";
import { Text, View, Button, StyleSheet } from "react-native";
import { useAppSelector } from "../../redux/store";
import { setDarkTheme } from "../../redux/slices/themeSlice";
import EmptyPage from "../../components/EmptyPageComponent";

export const SettingsScreen = () => {
  const count = useSelector(state => state.count) // getting the counter value
  const dispatch = useDispatch();
  const isDarkTheme = useAppSelector((state) => state.theme);

  return <EmptyPage title={'Sozlamalar hozircha mavjud emas'} />
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    fontSize: 24
  }
});
