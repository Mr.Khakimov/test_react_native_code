export type Item = {
    id: string | number;
    title: string;
    text: string;
    img: ImageSourcePropType,
};

export type DotsComponentType = {
    data: Item[];
    scrollX: any;
}

export type WelcomeItemType = {
    item: any;
    scrollX: Animated.Value;
    index: number;
};