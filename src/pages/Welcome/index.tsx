import React, { useCallback, useEffect, useRef, useState } from "react";
import {IconButton} from "../../components";
import {SafeAreaView} from "react-native-safe-area-context";
import {ArrowRightIcon} from "../../components/icons";
import {FlatList, StyleSheet, View, Animated, NativeSyntheticEvent, NativeScrollEvent} from "react-native";
import {WelcomeItem} from "./WelcomeItem";
import {DotsComponent} from "./DotsComponent";
import { constant } from "../../utils/theme";
import { width } from "../../utils/constants";
import { Item } from "./type";
import { useNavigation } from "@react-navigation/native";
import { useTheme } from "../../customHooks/ThemeHook";
import { useAppDispatch } from "../../redux/store";
import { toggleOnboarding } from "../../redux/slices/settingsSlice";

const welcome1 = require('../../../assets/images/welcomeOne.png');
const welcome2 = require('../../../assets/images/welcomeTwo.png');
const welcome3 = require('../../../assets/images/welcomeThird.png');
const welcome4 = require('../../../assets/images/welcomeFour.png');

const data: Item[] = [
    {id: 1, title: 'welcomeOneTitle', text: 'welcomeOneText', img: welcome1},
    {id: 2, title: 'welcomeTwoTitle', text: 'welcomeTwoText', img: welcome2},
    {id: 3, title: 'welcomeThreeTitle', text: 'welcomeTreeText', img: welcome3},
    {id: 4, title: 'welcomeFourTitle', text: 'welcomeFourText', img: welcome4},
]

export const WelcomeScreen = () => {
    const scrollX = useRef(new Animated.Value(0)).current;
    const flatList = useRef<FlatList>(null);
    const [activeIndex, setActiveIndex] = useState(0);
    const navigation = useNavigation();
    const {theme} = useTheme();
    const dispatch = useAppDispatch();

    const onScroll = useCallback(
        Animated.event<NativeSyntheticEvent<NativeScrollEvent>>(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
            { useNativeDriver: false }
    ),[]);

    const handleScroll = useCallback((event: NativeSyntheticEvent<NativeScrollEvent>) => {
        const xOffset = event.nativeEvent.contentOffset.x;
        const index = Math.round(xOffset / width);
        setActiveIndex(index);
    }, []);

    const handleNext = () => {
        if(activeIndex === data.length - 1) {
            navigation.navigate('Main');
            dispatch(toggleOnboarding(false));
            return;
        } else {
            setActiveIndex(activeIndex + 1);
        }
    }

    useEffect(() => {
        flatList.current?.scrollToIndex({
            index: activeIndex,
            animated: true,
        })

    }, [activeIndex])

    return (
        <SafeAreaView style={[style.wrapper, {backgroundColor: theme.bgColor}]}>
            <Animated.FlatList
                      ref={flatList} 
                      data={data}
                      horizontal={true}
                      pagingEnabled={true}
                      scrollEventThrottle={32}
                      decelerationRate={0}
                      snapToInterval={width}
                      onScroll={onScroll}
                      onMomentumScrollEnd={handleScroll}
                      bounces={false}
                      showsHorizontalScrollIndicator={false}
                      renderItem={({item, index}) => <WelcomeItem item={item} scrollX={scrollX} index={index} active={activeIndex} />}
                      keyExtractor={(item: Item) => String(item.id)}
            />

            <View style={style.bottomWrapper}>

                <DotsComponent data={data} scrollX={scrollX} />

                <IconButton onPress={handleNext} type="primary" size="large">
                    <ArrowRightIcon fill={undefined} size={undefined} />
                </IconButton>
            </View>
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexGrow: 1,
    },
    bottomWrapper: {
        paddingHorizontal: constant.space2,
        paddingTop: constant.space2,
        paddingBottom: constant.space2,
        flexDirection: 'row',
    }
});