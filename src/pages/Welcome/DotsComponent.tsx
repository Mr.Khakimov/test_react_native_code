import { View , StyleSheet, Animated} from "react-native"
import { DotsComponentType } from "./type";
import { useTheme } from "../../customHooks/ThemeHook";
import { width } from "../../utils/constants";

export const DotsComponent = ({data, scrollX}: DotsComponentType) => {
    const {theme} = useTheme()    

    return (
        <View style={style.wrapper}>
           {
            data.map((_, i) => {
                const inputRange = [(i - 1) * width, i * width, (i + 1) * width];

                const scale = scrollX.interpolate({
                    inputRange,
                    outputRange: [1, 1.6, 1],
                    extrapolate: 'clamp'
                });

                const opacity = scrollX.interpolate({
                    inputRange,
                    outputRange: [0.2, 1, 0.2],
                    extrapolate: 'clamp'
                });

                return <Animated.View key={`indicator-${i}`} style={[style.dots, {backgroundColor: theme.textDark, opacity, transform: [{scale}]}]}></Animated.View>
            })
           }
        </View>
    )
}

const style = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    dots: {
        width: 6,
        height: 6,
        borderRadius: 50,
        marginRight: 6,
    }
});