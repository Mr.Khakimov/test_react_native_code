import React, {memo, useEffect, useRef, useState} from "react";
import {Image, StyleSheet, View, Animated} from "react-native";
import {CustomTextComponent, MegaTitle} from "../../components";
import {useLocalization} from "../../customHooks/LocalizationHook";
import {height, width, isIos} from "../../utils/constants";
import { constant } from "../../utils/theme";
import { WelcomeItemType } from "./type";
import {beauty, clothes, souvenir, toys, cakes, flowers} from '../../utils/images';

export const WelcomeItem = memo(({item, scrollX, index} : WelcomeItemType) => {
    const {t} = useLocalization();
    
    const inputRange = [(index - 1) * width, index * width, (index + 1) * width];

    const scale = scrollX.interpolate({
        inputRange,
        outputRange: [0.3, 1, 0.3],
        extrapolate: 'clamp'
    });

    const translateX = scrollX.interpolate({
        inputRange,
        outputRange: [width * 0.6, 0, -width * 0.6],
        extrapolate: 'clamp'
    });

    const translateY = scrollX.interpolate({
        inputRange,
        outputRange: [50, 0, 100],
        extrapolate: 'clamp'
    });

    return (
        <View style={style.wrapper}>
         <Animated.View style={[style.imgWrapper, {transform: [{scale}, {translateX}]}]}>
             <Image style={style.img} source={item.img} />

         </Animated.View>

         {
            index === 0 ? <>
                <Animated.Image style={[style.imgAbs, style.imgAbs11, {transform: [{translateX}]}]} source={toys} />
                {!isIos ? <Animated.Image style={[style.imgAbs, style.imgAbs12, {transform: [{translateX}, {scale}]}]} source={cakes} /> : null}
                <Animated.Image style={[style.imgAbs, style.imgAbs13, {transform: [{translateX}]}]} source={flowers} />
            </> : null
         }

         {
            index === 1 ? <>
                <Animated.Image style={[style.imgAbs, style.imgAbs2, {transform: isIos ? [{translateY: translateY}] : [{translateX: translateX}, {scale}]}]} source={cakes} />
                <Animated.Image style={[style.imgAbs, style.imgAbs3, {transform: [{scale}, {translateX}]}]} source={souvenir} />
            </> : null
         }

        {
            index === 2 ? <>
                <Animated.Image style={[style.imgAbs, style.imgAbs4, {transform: [{scale}, {translateX}]}]} source={beauty} />
            </> : null
         }

        {
            index === 3 ? <>
                <Animated.Image style={[style.imgAbs, style.imgAbs5,, {transform: [{scale}, {translateX}]}]} source={clothes} />
            </> : null
         }
        
         <Animated.View style={[style.textWrapper, {transform: [{translateX}]}]}>
             <MegaTitle style={style.title}>{t(item.title)}</MegaTitle>
             <CustomTextComponent size="medium" color='gray'>{t(item.text)}</CustomTextComponent>
         </Animated.View>
        </View>
    )
})

const style = StyleSheet.create({
    wrapper: {
        position: 'relative',
        flex: 0.6, 
        width,
    },
    imgWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: constant.space2,
    },
    textWrapper: {
        padding: constant.space2
    },
    title: {
        marginBottom: constant.space2, 
        width: '65%'
    },
    img: {
        width: '100%',
        height: '100%',
        objectFit: 'contain'
    },
    imgAbs: {
        height: 120,
        width: 104,
        position: 'absolute'
    },
    imgAbs11: {
        top: 20,
        right: 16,
    },
    imgAbs12: {
        top: height * 0.45,
        right: -40,
    },
    imgAbs13: {
        top: height * 0.35,
        left: 25,
    },
    imgAbs2: {
        top: height * 0.35,
        left:-70,
    },
    imgAbs3: {
        top: height * 0.4,
        right: 24,
    },
    imgAbs4: {
        top: height * 0.11,
        left: 16,
    },
    imgAbs5: {
        top: height * 0.15,
        left: 16,
    },
})
