import { ProfileScreen } from "./Profile";
import { MainScreen } from "./Main";
import { ProductScreen } from "./Main/ProductScreen";
import { ProductInnerScreen } from "./Main/ProductInner";
import { StoreScreen } from "./Main/StoreScreen";
import { BoardsScreen } from "./Boards";
import { SettingsScreen } from "./Settings";
import { AuthScreen } from "./Auth";
import {SmsCodeScreen} from './Auth/SmsCodeScreen'
import { SingleTaskScreen } from "./SingleTask";
import { WelcomeScreen } from "./Welcome";
import { FavoritesScreen } from "./Favorites";
import { EventsScreen } from "./Events";
import { EventsCreateScreen } from "./Events/EventsCreateScreen";
import { MyOrdersScreen } from "./MyOrders";
import { SearchScreen } from "./Search";
import {ReviewsScreen} from "./Reviews"
import {StoreCatalogScreen} from './Main/StoreCatalog'
import {BasketScreen} from './Basket'
import { ShopsScreen } from "./Shop";
import {CommentScreen} from './Comment'
import {UsePromocodeScreen} from './UsePromocode';
import {DeliveryScreen} from './Delivery';

export { 
    ProfileScreen, 
    EventsScreen, 
    MyOrdersScreen, 
    MainScreen, 
    BoardsScreen, 
    FavoritesScreen, 
    SettingsScreen, 
    AuthScreen, 
    SmsCodeScreen,
    SingleTaskScreen, 
    WelcomeScreen,
    ProductScreen,
    ProductInnerScreen,
    StoreScreen,
    EventsCreateScreen,
    SearchScreen,
    ReviewsScreen,
    StoreCatalogScreen,
    BasketScreen,
    ShopsScreen,
    CommentScreen,
    UsePromocodeScreen,
    DeliveryScreen
}
