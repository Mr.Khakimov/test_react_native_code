import { useCallback, useState } from "react";
import { SafeAreaView, StyleSheet, TextInput, View } from "react-native"
import HeaderComponent from "../../components/layaouts/HeaderComponent"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { MainStyle, constant, lightTheme } from "../../utils/theme";
import { LeftChevronIcon } from "../../components/icons";
import { useFocusEffect } from "@react-navigation/native";
import { CustomTextComponent } from "../../components";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { setDeliveryOptions, showBottomBasketLink } from "../../redux/slices/basketSlice";
import {CommentScreenProps} from "../../navigation/navigationTypes";

const maxTextSize = 400;

export const CommentScreen = ({navigation, route} : CommentScreenProps) => {
    const {t} = useLocalization();
    const [value, setValue] = useState('');
    const [error, setError] = useState(false);
    const dispatch = useAppDispatch();
    const deliveryComment = useAppSelector(state => state.basket.deliveryComment);
    const [isAddressComment] = useState(route.params?.title === 'addressComment');

    const postcardText = useAppSelector(state => state.basket.postcardText);
    const addPostcard = useAppSelector(state => state.basket.addPostcard);

    console.log('deliveryComment', deliveryComment);

    useFocusEffect(
        useCallback(() => {
            setTimeout(() => {
                dispatch(showBottomBasketLink(false));
            }, 5)

            return () => dispatch(showBottomBasketLink(true));
        }, [])
    );

    const goBack = useCallback(() => {
        navigation.goBack();
    }, [])

    const onChangeText = useCallback((e: string) => {
        if(e.length < 401) {
            if(isAddressComment) {
              dispatch(setDeliveryOptions({key: "deliveryComment", value: e}))
            } else {
              dispatch(setDeliveryOptions({key: "postcardText", value: e}))
            }
        } else {

        }
    }, [isAddressComment])

    console.log('isAddressComment', route.params?.title, isAddressComment);


    return (
        <SafeAreaView style={MainStyle.layout}>
            <HeaderComponent
                leftIcon={<LeftChevronIcon />}
                onLeftIconPress={goBack}
                title={isAddressComment ? t('comment') : t('writeBasketText')}
            />

            <View style={MainStyle.p3}>
                <View style={[!isAddressComment && styles.inputWrapper]}>
                    <View style={styles.flex}>
                        <TextInput
                            value={isAddressComment ? deliveryComment : postcardText}
                            multiline={true}
                            onChangeText={onChangeText}
                            autoFocus={true}
                            placeholder={isAddressComment ? t('orderComment') : t('writeBasketText')}
                            style={styles.input} />
                    </View>

                    {
                        !isAddressComment &&
                        <CustomTextComponent style={MainStyle.tar} textType="regular" color="gray">
                            { postcardText.length } /
                            { maxTextSize }
                        </CustomTextComponent>
                    }
                </View>

            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    inputWrapper: {
        borderWidth: 2,
        borderColor: lightTheme.gray,
        borderRadius: constant.space3,
        paddingHorizontal: constant.space3,
        paddingVertical: constant.space2,
        minHeight: 200,
    },
    flex: {
        flex: 1,
    },
    input: {
        // padding: 0,
        // minHeight: 200,
        ...MainStyle.inputStyle,
    }
});
