import React from 'react';
import { SafeAreaView } from "react-native";
import { PlusIcon } from "../../components/icons";
import { EmptyLayout } from '../../components';
import { favEmpty } from '../../utils/images';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { MainStyle } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useNavigation } from '@react-navigation/native';
import { useAppDispatch } from '../../redux/store';
import { setIsScreenLoginModal } from '../../redux/slices/registerSlice';

const favotireData = [];

export const MyOrdersScreen = () => {
  const {t} = useLocalization();
  const dispatch = useAppDispatch();
  const navigation = useNavigation();

  const onRightIconPress = () => {
    console.log('handleright icon');
  }

  const handleButton = () => {
    dispatch(setIsScreenLoginModal());
    // navigation.navigate('AuthScreen');
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
      <HeaderComponent rightIconBg='secondary' title={t('myOrders')} rightIcon={<PlusIcon />} onRightIconPress={onRightIconPress}></HeaderComponent>
      {favotireData.length 
        ? <>
        
        </> : 
        <EmptyLayout 
          imageSource={favEmpty} 
          title={t('ordersEmptyTitle')} 
          text={t('ordersEmptyText')}
          buttonText={t('goToMain')}
          onButtonPress={handleButton}
        />
      }
    </SafeAreaView>
  ) 
}