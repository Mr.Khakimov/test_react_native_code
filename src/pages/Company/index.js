import React from 'react';
import { StyleSheet } from "react-native";
import EmptyPage from "../../components/EmptyPageComponent";
import { CompanyIcon } from "../../components/icons";

export const CompanyScreen = () => {
  return <EmptyPage title={'Hozirda kompaniyalar mavjud emas'} icon={<CompanyIcon size={96} />} />
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    fontSize: 24
  }
});
