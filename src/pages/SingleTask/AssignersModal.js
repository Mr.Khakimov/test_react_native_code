import TagComponentEditable from "../../components/TagComponentEditable";
import ScrollListWithSearch from "../../components/ScrollListWithSearch";
import React, { useState } from "react";

export const AssignersModal = () => {
  const [data, setData] = useState([
    {id: 1, name: 'Hakimov H'},
    {id: 2, name: 'Karimov I'},
    {id: 3, name: 'anorboev a'},
    {id: 4, name: 'John Doe'},
    {id: 5, name: 'Smith Will'},
  ]);

  const onAddTag = text => {
    setData([{id: Math.random(), name: text}, ...data]);
  };

  return (
    <>
      <TagComponentEditable
        tags={[{id: 1, name: 'etatf', bg: '#002'}]}
        onAddTag={onAddTag}
      />
      <ScrollListWithSearch
        data={data}
        activeData={[ {id: 1, name: 'Hakimov H'},
          {id: 2, name: 'Karimov I'}]}
      />
    </>
  )
}
