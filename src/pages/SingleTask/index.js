import React, {useCallback, useMemo, useRef, useState} from 'react';
import {StyleSheet, SafeAreaView, View, Text} from 'react-native';
import {TaskFields} from './TaskFields';
import {BottomSheetModal, BottomSheetModalProvider} from '@gorhom/bottom-sheet';
import CustomModal from '../../components/CustomModal';
import {Assigners} from '../../components/Assigners';
import TagComponentEditable from '../../components/TagComponentEditable';
import ScrollListWithSearch from '../../components/ScrollListWithSearch';
import {TagsModal} from './TagsModal';
import {AssignersModal} from './AssignersModal';
import { StatusModal } from "./StatusModal";
import { PriorityModal } from "./PriorityModal";

export const SingleTaskScreen = ({navigation, route}) => {
  const {params} = route;

  const [modal, setModal] = useState({
    type: 'assigners',
    show: false,
  });

  const bottomSheetModalRef = useRef(null);

  // variables
  const snapPoints = useMemo(() => ['25%', '50%'], []);

  // callbacks
  const handlePresentModalPress = useCallback(() => {
    console.log('asdasd');
    bottomSheetModalRef.current?.present();
  }, []);
  const handleSheetChanges = useCallback(index => {
    console.log('handleSheetChanges', index);
  }, []);

  const handleClose = useCallback(() => {
    navigation.goBack();
  }, []);

  const changeDescription = () => {
    console.log('description changed');
  };

  const modalClose = () => {
    setModal({
      type: '',
      show: false,
    });
  };

  const modalShow = type => {
    setModal({
      type: type,
      show: true,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <TaskFields handleClose={handleClose} handleOpenSheet={modalShow} />

      <BottomSheetModalProvider>
        <BottomSheetModal
          ref={bottomSheetModalRef}
          index={1}
          snapPoints={snapPoints}
          onChange={handleSheetChanges}>
          <View style={styles.contentContainer}>
            <Text>Awesome 🎉</Text>
          </View>
        </BottomSheetModal>
      </BottomSheetModalProvider>

      <CustomModal
        visible={modal.show}
        content={
          modal.type === 'tags' ? (
            <TagsModal />
          ) : modal.type === 'assigners' ? (
            <AssignersModal />
          ) : modal.type === 'date' ? (
            <AssignersModal />
          ) : modal.type === 'priority' ? (
            <PriorityModal />
          ) : modal.type === 'status' ? (
            <StatusModal />
          ) : (
            <Text>Tanlanmagan</Text>
          )
        }
        onCloseX={modalClose}
        title={
          modal.type === 'tags'
            ? 'Teglar'
            : modal.type === 'assigners'
            ? 'Bajaruvchilar'
            : modal.type === 'date'
            ? 'Muddat'
            : modal.type === 'status'
            ? 'Holaty'
            : modal.type === 'priority'
            ? 'Muhimligi'
            : 'Task Name'
        }
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
