import React, {useState} from 'react';
import {Tag} from '../../components/TagComponent';
import {View} from 'react-native';
import { DoneIcon } from "../../components/icons";
import {ListItemWithIcon} from '../../components/ListItemWithIcon';

export const PriorityModal = () => {
  const [data] = useState([
    {id: 1, name: 'Muhim'},
    {id: 2, name: "O'taa Muhim"},
    {id: 3, name: 'Nihoyatda Muhim'},
  ]);

  const [isActive, setIsActive] = useState(2);

  return (
    <>
      {data.map(el => {
        return (
          <View style={{height: 50}}>
            <ListItemWithIcon
              title={<Tag bg={'#632754'} label={el.name}></Tag>}
              onPress={() => setIsActive(el.id)}
              deskIsText={true}
              rightContent={el.id === isActive ? <DoneIcon fill={'#56da1c'} /> : null}
            />
          </View>


        );
      })}
    </>
  );
};
