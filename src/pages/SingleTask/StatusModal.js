import React, {useState} from 'react';
import {Tag} from '../../components/TagComponent';
import {View} from 'react-native';
import { DoneIcon } from "../../components/icons";
import {ListItemWithIcon} from '../../components/ListItemWithIcon';

export const StatusModal = () => {
  const [data] = useState([
    {id: 1, name: 'yangi'},
    {id: 2, name: "ish jarayonida"},
    {id: 3, name: 'bajarildi'},
  ]);

  const [isActive, setIsActive] = useState(1);

  return (
    <>
      {data.map(el => {
        return (
          <View style={{height: 50}} key={el.id}>
            <ListItemWithIcon
              title={<Tag bg={'#632754'} label={el.name}></Tag>}
              onPress={() => setIsActive(el.id)}
              deskIsText={true}
              rightContent={el.id === isActive ? <DoneIcon fill={'#56da1c'} /> : null}
            />
          </View>


        );
      })}
    </>
  );
};
