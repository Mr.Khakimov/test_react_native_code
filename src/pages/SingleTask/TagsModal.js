import TagComponentEditable from "../../components/TagComponentEditable";
import ScrollListWithSearch from "../../components/ScrollListWithSearch";
import React, { useState } from "react";

export const TagsModal = () => {
  const [data, setData] = useState([
    {id: 1, name: 'Mobile'},
    {id: 2, name: 'Ildam'},
    {id: 3, name: 'App'},
  ]);

  const onAddTag = text => {
    setData([{id: Math.random(), name: text}, ...data]);
  };

  return (
    <>
      <TagComponentEditable
        tags={[{id: 1, name: 'etatf', bg: '#002'}]}
        onAddTag={onAddTag}
      />
      <ScrollListWithSearch
        data={data}
        activeData={[ {id: 1, name: 'Hakimov H'},
          {id: 2, name: 'Karimov I'}]}
      />
    </>
  )
}
