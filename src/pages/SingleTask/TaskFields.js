import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import CircleButton from "../../components/CircleButton";
import {
  AssigmentIcon,
  BeenhereIcon,
  CalendarIcon,
  CloseIcon,
  EmergencyIcon,
  LocalIcon,
  PeopleIcon,
} from "../../components/icons";
import { SectionTitle } from "../../components/SectionTitle";
import { ListItemWithIcon } from "../../components/ListItemWithIcon";
import { StyleSheet, View } from "react-native";
import { Tag } from "../../components/TagComponent";

export const TaskFields = ({ handleClose, handleOpenSheet}) => {
  return (
    <ScrollView>

      <CircleButton icon={<CloseIcon />} onPress={handleClose} />

      <SectionTitle title={'TaskName'} />

      <ListItemWithIcon
        leftContent={<PeopleIcon />}
        title={'Bajaruvchilar'}
        description={
          <>
            <View style={styles.row}>
              <Tag label={'Hakimov H'} bg={'#ab0764'} />
              <Tag label={'Ermator I'} bg={'#791609'} />
              <Tag label={'Anorboev H'} bg={'#167abb'} />
              <Tag label={'Anorboev H'} bg={'#167abb'} />
              <Tag label={'Anorboev H'} bg={'#167abb'} />
              <Tag label={'Anorboev H'} bg={'#167abb'} />
            </View>
          </>
        }
        onPress={() => handleOpenSheet('assigners')}
        divider
      />
      <ListItemWithIcon
        leftContent={<AssigmentIcon />}
        title={'Muallif'}
        deskIsText={true}
        description={'Hakimov Hikmatulloh'}
        disabled={true}
        divider
      />
      <ListItemWithIcon
        leftContent={<EmergencyIcon />}
        title={'Holati'}
        onPress={() => handleOpenSheet('status')}
        description={
          <>
            <View style={styles.row}>
              <Tag label={'Ish jarayonida'} bg={'#7749d5'} />
            </View>
          </>
        }
        divider
      />
      <ListItemWithIcon
        leftContent={<CalendarIcon />}
        title={'Muddati'}
        description={'02.14.2023'}
        onPress={() => handleOpenSheet('date')}
        deskIsText={true}
        disabled={true}
        divider
      />

      <ListItemWithIcon
        leftContent={<LocalIcon />}
        title={'Teglar'}
        onPress={() => handleOpenSheet('tags')}
        description={
          <>
            <View style={styles.row}>
              <Tag label={'test'} bg={'#ab0764'} />
              <Tag label={'test'} bg={'#791609'} />
              <Tag label={'test'} bg={'#167abb'} />
            </View>
          </>
        }
        divider
      />
      <ListItemWithIcon
        leftContent={<BeenhereIcon />}
        title={'Muhimligi'}
        onPress={() => handleOpenSheet('priority')}
        description={
          <>
            <View style={styles.row}>
              <Tag label={'Yuqori'} bg={'#9a743a'} />
            </View>
          </>
        }
        divider
      />

      {/*<ListItemWithIcon*/}
      {/*  leftContent={<MailIcon />}*/}
      {/*  title={'Sharh'}*/}
      {/*  description={*/}
      {/*    <>*/}
      {/*      <InputComponent value={'lotemdsaf dsf'} placeholder={'sharh qoldirish'} multiline onInputChange={changeDescription} />*/}
      {/*    </>*/}
      {/*  }*/}
      {/*  divider*/}
      {/*/>*/}

      {/*<Assigners />*/}
    </ScrollView>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: "wrap",
  },
  title: {
    fontSize: 24,
    color: '#000',
    marginBottom: 12,
  },
  text: {
    fontSize: 14,
    color: '#000',
    marginBottom: 8,
  },
  board: {
    paddingVertical: 12,
    backgroundColor: '#E0E8EF',
  },
  column: {
    backgroundColor: '#F8FAFB',
    marginHorizontal: 6,
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderRadius: 4,
  },
  columnHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 16,
  },
  columnName: {
    fontWeight: 'bold',
  },
  card: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#F6F7FB',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 24,
    paddingVertical: 16,
    marginBottom: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  addCard: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(233, 233, 233)',
    borderRadius: 4,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#F5F6F8',
  },
});
