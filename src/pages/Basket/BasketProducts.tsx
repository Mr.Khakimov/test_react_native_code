import { useAppSelector } from "../../redux/store"
import { BasketProduct } from "./BasketProduct";
import React from "react";

interface BasketProductsProps {}

export const BasketProducts: React.FC<BasketProductsProps> = () => {
    const basket = useAppSelector(state => state.basket.products);

    if (basket?.length) {
        return (
            <>
                {basket.map(el => (
                    <BasketProduct key={el.id} item={el} />
                ))}
            </>
        );
    }

    return null;
};
