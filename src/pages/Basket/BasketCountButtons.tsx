import { StyleSheet, View } from "react-native"
import { CustomTextComponent, IconButton } from "../../components";
import { MinusIcon, PlusIcon } from "../../components/icons";
import { useCallback, useState } from "react";
import { addProduct, removeProduct, updateProduct } from "../../redux/slices/basketSlice";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { useTheme } from "../../customHooks/ThemeHook";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { useNavigation } from "@react-navigation/native";

const max = 98;
const min = 1;

export const BasketCountButtons = ({item}) => {

  const {id, storeId} = item;

  const {t} = useLocalization();
  const {theme} = useTheme();
  const onBasketCount = useAppSelector(state => state.basket.products.find(el => el.id === id)?.count);
  const onBas = useAppSelector(state => state.basket.products);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(onBasketCount ? onBasketCount : 1);
  const dispatch = useAppDispatch();
  const navigation = useNavigation();

  const handleIncrement = () => {
    setCount(count + 1);
    dispatch(updateProduct({id, storeId, count: count + 1}))
  };

  const handleDecrement = () => {
    if (count > 0) {
        if (count > 1) {
            setCount(count - 1);
            dispatch(updateProduct({id, storeId, count: count - 1}))
        } else {
            if(onBas.length === 1) {
                navigation.goBack();
            }
            setCount(0);
            dispatch(removeProduct(id))
        }
    }
  };

  const buyProduct = useCallback(() => {
    setLoading(true);
    if(onBasketCount) {
      if(count === 0) {
        dispatch(removeProduct(id))
        setCount(1);
      } else {
        dispatch(updateProduct({id, storeId, count}))
      }
    }else {
      if(count) {
        dispatch(addProduct({id, storeId, count}))
      }
    }
    setTimeout(() => {
      setLoading(false);
    }, 200)

    navigation.goBack();
  }, [count, onBasketCount]);


    return (
        <View style={[styles.btn, {backgroundColor: theme.grey3}]}>
            <IconButton disabled={count < min} onPress={handleDecrement} type={'default'} size="small">
                <MinusIcon stroke={count < min ? theme.textGray : theme.textDark}  />
            </IconButton>
            <CustomTextComponent style={styles.counterText} textType='bold' color={'dark'} size='medium'>{count}</CustomTextComponent>
            <IconButton disabled={count > max} onPress={handleIncrement} type={'default'} size="small">
                <PlusIcon stroke={count > max ? theme.textGray : theme.textDark}  />
            </IconButton>    
        </View>
    )
}

const styles = StyleSheet.create({
    btn: {
        width: 75,
        height: 32,
        borderRadius: 10,
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
    },
    counterText: {
        flexGrow: 1,
        textAlign: 'center',
    },

});
