import React, {FC, useCallback, useMemo} from "react";
import { Image, Pressable, StyleSheet, TouchableHighlight, View } from "react-native"
import { CustomTextComponent } from "../../components"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { MainStyle, lightTheme } from "../../utils/theme";
import { logoPaper } from "../../utils/images";
import { useTheme } from "../../customHooks/ThemeHook";
import { ChkBoxActiveIcon } from "../../components/icons";
import {useAppDispatch, useAppSelector} from "../../redux/store";
import {setDeliveryOptions} from "../../redux/slices/basketSlice";
import {useAppNavigation} from "../../customHooks/NavigationHook";

const maxTextSize = 400;

export const BasketAddToOrder: FC = () => {
    const {theme} = useTheme();
    const {t} = useLocalization();
    const dispatch = useAppDispatch();
    const navigation = useAppNavigation();

    const postcardText = useAppSelector(state => state.basket.postcardText);
    const addPostcard = useAppSelector(state => state.basket.addPostcard);

    const handleAddPostcard = useCallback(() => {
        dispatch(setDeliveryOptions({
            key: 'addPostcard',
            value: !addPostcard,
        }));
    }, [addPostcard]);

    const memoActiveStyle = useMemo(() => {
        return {
            wrapper: {borderColor: addPostcard ? theme.textDark : theme.grey3},
            checkbox: {borderColor: addPostcard ? theme.textDark : theme.grey3},
        }
    }, [addPostcard]);

    const navigateToCommentScreen = useCallback(() => {
        navigation.navigate('CommentScreen', {title: 'postcardComment'})
    }, [])

    return (
        <View style={[MainStyle.ph3]}>
            <View style={[MainStyle.flexRow, MainStyle.pb3]}>
                <TouchableHighlight
                    onPress={handleAddPostcard}
                    underlayColor={theme.grey3} style={[styles.tag, memoActiveStyle.wrapper]}>
                    <>

                        <Image style={styles.img} source={logoPaper} />
                        <View style={styles.main}>
                            <CustomTextComponent
                                style={styles.text}
                                textType="regular"
                                color="dark">
                                {t('postcard')}
                            </CustomTextComponent>
                            <CustomTextComponent textType="bold" color="dark">{t('Бесплатно')}</CustomTextComponent>
                        </View>
                        <View style={styles.left}>
                            <View style={[styles.checkbox, memoActiveStyle.checkbox]}>
                                {addPostcard ? <ChkBoxActiveIcon /> : null}
                            </View>
                        </View>
                    </>
                </TouchableHighlight>
            </View>
            {
                addPostcard ?
                    <Pressable onPress={navigateToCommentScreen} style={styles.textArea}>
                        <CustomTextComponent
                            numberOfLines={4}
                            style={MainStyle.flexGrow}
                            textType="regular"
                            color="gray">
                            { postcardText.length ? postcardText : t('writeBasketText') }
                        </CustomTextComponent>
                        <CustomTextComponent
                            style={MainStyle.tar}
                            textType="regular"
                            color="gray">{postcardText.length} / {maxTextSize}</CustomTextComponent>
                    </Pressable>
                : null
            }
        </View>
    )
}

const styles = StyleSheet.create({
    tag: {
        borderRadius: 12,
        borderWidth: 1,
        flexDirection: 'row',
        overflow: 'hidden',
    },

    img: {
        width: 48,
        height: '100%',
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12,
        backgroundColor: lightTheme.primaryColor,
    },

    main: {
        padding: 8,
    },

    text: {
        marginBottom: 4,
    },

    left: {
        paddingRight: 8,
        paddingVertical: 8,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },

    checkbox: {
        width: 20,
        height: 20,
        borderRadius: 6,
        borderWidth: 1,
    },

    textArea: {
        height: 100,
        borderRadius: 18,
        borderWidth: 1,
        padding: 12,
        borderColor: lightTheme.dark,
    },
});
