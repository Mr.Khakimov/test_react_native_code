import { View } from "react-native";
import { useBasketPriceHook } from "../../customHooks/ProductFilterHook";
import { MainStyle } from "../../utils/theme";
import { CustomTextComponent } from "../../components";
import { PriceFormat } from "../../utils/priceFormat";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { memo } from "react";

export const BasketFullPrice = memo(() => {
    const {price, deliveryTime} = useBasketPriceHook();
    const {t} = useLocalization();
    return (
        <>
            <View style={[MainStyle.flexRow, MainStyle.p3, MainStyle.jsb,  MainStyle.aic]}>
                <CustomTextComponent textType="bold" size="extraLarge" color="dark">Товары в заказе</CustomTextComponent>
                <View style={[MainStyle.flexRow, MainStyle.jsb,  MainStyle.aic]}>
                  <CustomTextComponent textType="bold" size="medium" color="dark">{PriceFormat(price)} {t('currency')}</CustomTextComponent>
                </View>
            </View>
        </>
    )
})