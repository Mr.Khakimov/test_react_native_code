import { View } from "react-native"
import { MainStyle } from "../../utils/theme"
import { CustomButton, CustomTextComponent } from "../../components"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { useBasketPriceHook } from "../../customHooks/ProductFilterHook"
import { PriceFormat } from "../../utils/priceFormat"

export const BasketBottomButton = ({onPress = () => {}}) => {
    const {t} = useLocalization();
    const {price, deliveryTime} = useBasketPriceHook();
    
    return (
        <View style={MainStyle.p3}>
            <CustomButton onPress={onPress} text="">
              <View style={[MainStyle.flexRow, MainStyle.jsb, MainStyle.ph3]}>
                <CustomTextComponent textType="regular" size="medium" color="white">{deliveryTime} {t('min')}</CustomTextComponent>
                <CustomTextComponent style={[MainStyle.flexGrow, MainStyle.tac]} textType="bold" size="large" color="white">{t("continue")}</CustomTextComponent>
                <CustomTextComponent textType="regular" size="medium" color="white">{PriceFormat(price)} {t('currency')}</CustomTextComponent>
              </View>
            </CustomButton>
          </View>
    )
}