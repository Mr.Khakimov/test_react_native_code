import { Image, StyleSheet, View } from "react-native"
import { CustomTextComponent } from "../../components"
import { useFindProduct } from "../../customHooks/ProductFilterHook";
import { PriceFormat } from "../../utils/priceFormat";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { lightTheme } from "../../utils/theme";
import { BasketCountButtons } from "./BasketCountButtons";
import { useAppSelector } from "../../redux/store";

export const BasketProduct = ({item} :any) => {
    const product = useFindProduct(item?.id);
    const onBasketCount = useAppSelector(state => state.basket.products.find(el => el.id === item.id)?.count);

    const {t} = useLocalization();

    return (
        <View style={styles.wrapper}>
            <Image style={styles.img} source={{uri: product?.img}} />
            
            <View style={styles.main}>
                <CustomTextComponent textType="bold" size="large" color="dark">
                    {product.name}
                </CustomTextComponent>

                <View style={styles.mainPrice}>
                    <CustomTextComponent style={styles.textPrice} textType="bold" size="medium" color="dark">
                        {PriceFormat(product.price * onBasketCount)} {t('currency')}
                    </CustomTextComponent>

                    <CustomTextComponent style={styles.textOldPrice} size="small">
                        {PriceFormat(product?.oldPrice ? (product?.oldPrice * onBasketCount) : (product?.price * 1.2 * onBasketCount))} {t('currency')}
                    </CustomTextComponent>
                </View>
            </View>

            <View style={styles.left}>
                <BasketCountButtons item={product} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingBottom: 16,
    },
    main: {
        flex: 2,
        padding: 12,
    },
    mainPrice: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 8,
    },
    left: {
        marginLeft: 8,
    },
    textPrice: {
        padding: 4,
        backgroundColor: lightTheme.grey3,
        borderRadius: 12,
        marginRight: 12,
    },
    textOldPrice: {
        textDecorationLine: 'line-through'
    },
    img: {
        width: 69,
        height: 66,
        borderRadius: 16,
    },
});