import { Image, StyleSheet, View } from "react-native"
import { CustomTextComponent } from "../../components"
import { constant, lightTheme } from "../../utils/theme";
import { useTheme } from "../../customHooks/ThemeHook";
import { useAppSelector } from "../../redux/store";
import { allData } from "../../MOCK/flowersShop";
import {FC, memo} from "react";

export const StoreTag: FC = memo(() => {
    const {theme} = useTheme();

    const basketStore = useAppSelector(state => {
        const prodLength = state.basket.products?.length
        if(!prodLength) {
            return null
        }
        const storeId = state.basket.products[0]?.storeId
        const store = allData.find(el => el.id === storeId);
        return store
    });

    return (
        <View style={[styles.wrapper, {backgroundColor: theme.grey3}]}>
            <Image style={styles.img} source={typeof basketStore?.img === "number" ? basketStore?.shopIcon : {uri: basketStore?.img}}></Image>
            <CustomTextComponent textType="bold" color="dark" size="medium">
                {basketStore?.title}
            </CustomTextComponent>
        </View>
    )
})

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: constant.space1,
        borderRadius: constant.space2,
    },
    img: {
        width: 32,
        height: 32,
        borderRadius: 32,
        marginRight: 8,
        backgroundColor: lightTheme.grey3,
    },
});
