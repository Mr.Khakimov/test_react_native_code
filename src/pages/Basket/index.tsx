import React, { useCallback } from 'react';
import {SafeAreaView, StyleSheet, TouchableHighlight, View} from "react-native";
import { LeftChevronIcon, RightChevronIcon, TrashIcon } from "../../components/icons";
import { ScrollView } from 'react-native-gesture-handler';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { useTheme } from '../../customHooks/ThemeHook';
import { MainStyle } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { CustomTextComponent, SectionTitle } from '../../components';
import { useFocusEffect } from '@react-navigation/native';
import { useAppDispatch } from '../../redux/store';
import { clearBasket, showBottomBasketLink } from '../../redux/slices/basketSlice';
import { StoreTag } from './StoreTag';
import { BasketProducts } from './BasketProducts';
import { BasketBottomButton } from './BasketBottomButton';
import { BasketAddToOrder } from './BasketAddToOrder';
import { BasketFullPrice } from './BasketFullPrice';
import {BasketAddToOrderProps} from "../../navigation/navigationTypes";


export const BasketScreen = ({navigation}: BasketAddToOrderProps) => {
  const {t} = useLocalization();
  const {theme} = useTheme();
  const dispatch = useAppDispatch();

  const onLeftIconPress = () => {
    navigation.goBack();
  }

  useFocusEffect(
    React.useCallback(() => {
      dispatch(showBottomBasketLink(false));


      return () => {
        dispatch(showBottomBasketLink(true));
      }
    }, [])
  );

  const handleClearBasket = useCallback(() => {
    dispatch(clearBasket());
    navigation.goBack();
  }, [])

  const handlePromoCodesSales = useCallback(() => {
    navigation.navigate('UsePromoCodeScreen');
  }, [])

  const handleDelivery = useCallback(() => {
    navigation.navigate('DeliveryScreen');
  }, [])

  const handleBtn = useCallback(() => {

  }, [])

  return (
    <SafeAreaView style={MainStyle.layout}>
        <HeaderComponent
            leftIcon={<LeftChevronIcon />}
            onLeftIconPress={onLeftIconPress}
            title={t('basket')}
            titleStyle={MainStyle.tac} />

        <ScrollView contentContainerStyle={MainStyle.contentContainerStyle}>
          <View style={wrapper}>
            <StoreTag />
          </View>

          <SectionTitle
              onPress={handleClearBasket}
              title={t("order")}
              icon={<TrashIcon />}
              value={''}
          />

          <BasketProducts />

          <View style={MainStyle.flex}>
            <CustomTextComponent
                style={MainStyle.p3}
                textType="bold"
                size="extraLarge"
                color="dark">{t('addToOrder')}
            </CustomTextComponent>

            <BasketAddToOrder />

            <TouchableHighlight
                onPress={handlePromoCodesSales}
                underlayColor={theme.grey3}
                style={freeBtnStyle}>
              <>
                <CustomTextComponent
                    textType="bold"
                    size="extraLarge"
                    color="dark">{t('promoCodesAndDisCont')}
                </CustomTextComponent>
                <View style={flexCenterAndBetween}>
                  <RightChevronIcon />
                </View>
              </>
            </TouchableHighlight>

            <BasketFullPrice />

            <TouchableHighlight
                onPress={handleDelivery}
                underlayColor={theme.grey3}
                style={freeBtnStyle}>
              <>
                <CustomTextComponent textType="bold" size="extraLarge" color="dark">Доставка</CustomTextComponent>
                <View style={flexCenterAndBetween}>
                  <CustomTextComponent textType="bold" size="medium" color="dark">Бесплатно</CustomTextComponent>
                </View>
              </>
            </TouchableHighlight>

          </View>

        </ScrollView>

        <BasketBottomButton onPress={handleBtn} />
    </SafeAreaView>
  )
}

const flexCenterAndBetween = StyleSheet.flatten([MainStyle.flexRow, MainStyle.jsb,  MainStyle.aic]);
const freeBtnStyle = StyleSheet.flatten([MainStyle.flexRow, MainStyle.p3, MainStyle.jsb,  MainStyle.aic]);
const wrapper = StyleSheet.flatten([MainStyle.flexRow, MainStyle.ph3]);

