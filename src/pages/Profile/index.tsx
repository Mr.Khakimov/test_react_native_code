import React from 'react';
import { StyleSheet, SafeAreaView, View, Image, Text, ScrollView } from "react-native";
import { CustomTitle, ListItem } from '../../components';
import { bonusStar, profileImage } from '../../utils/images';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { MainStyle, constant } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useTheme } from '../../customHooks/ThemeHook';
import { Localization } from '../../utils/types';

export const ProfileScreen = () => {
  const {t} : Localization = useLocalization();
  const {theme} = useTheme();

  const onRightIconPress = () => {
    console.log('handleright icon');
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
      <HeaderComponent title={t('profile')}></HeaderComponent>

      <ScrollView>

        <View style={[MainStyle.aic]}>
          <View style={styles.imgWrapper}>
            <Image style={styles.img} source={profileImage}></Image>
          </View>

          <CustomTitle style={MainStyle.mb4}>{t('Пользователь')}</CustomTitle>
        </View>

        <View style={styles.section}>
          <ListItem title="Мои бонусы" rightIcon="true">
            <>
              <Image style={styles.bonusImg} source={bonusStar} />
              <Text>{" "}300</Text>
            </>
          </ListItem>
        </View>

        <View style={styles.section}>
          <ListItem title="Мои промокоды" rightIcon="true"></ListItem>
          <ListItem titleStyle={{color: theme.red}} title="Пригласи, друга получи 20 000 сум" rightIcon="true"></ListItem>
        </View>

        <View style={styles.section}>
          <ListItem title="Валюта" rightIcon="true">
            Узбекский сум
          </ListItem>
        </View>

        <View style={styles.section}>
          <ListItem title="Русский язык" rightIcon="true"></ListItem>
          <ListItem title="Настройка уведомлений" rightIcon="true"></ListItem>
          <ListItem title="Центр помощи" rightIcon="true"></ListItem>
        </View>

        <View style={styles.section}>
          <ListItem title="О приложении" rightIcon="true"></ListItem>
        </View>

      </ScrollView>
      
    </SafeAreaView>
  ) 
}

const styles = StyleSheet.create({
  imgWrapper: {
    width: 104,
    height: 104,
    borderRadius: 32,
    marginTop: constant.space3 * 2,
    marginBottom: constant.space3,
  },
  img: {
    width:'100%',
    height: '100%',
    objectFit: 'contain'
  },
  section: {
    borderRadius: 16,
    overflow: 'hidden',
    marginHorizontal: constant.space3,
    marginBottom: constant.space1,
  },
  bonusStar: {
    width: 5,
    height: 5,
  },
  bonusImg: {
    marginBottom: -2,
    width: 16,
    height: 16,
    objectFit: 'contain'
  },
});
