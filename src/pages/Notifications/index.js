import React from 'react';
import { Text, View, Button, StyleSheet } from "react-native";
import EmptyPage from "../../components/EmptyPageComponent";
import { NotificationIcon } from "../../components/icons";

export const NotificationsScreen = () => {
  return <EmptyPage title={'Hozirda bildirishnomalar mavjud emas'} icon={<NotificationIcon size={96} />} />
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    fontSize: 24
  }
});
