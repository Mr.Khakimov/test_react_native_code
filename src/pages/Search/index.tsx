import React, { useRef } from 'react';
import { SafeAreaView, View } from "react-native";
import { LeftChevronIcon, PlusIcon } from "../../components/icons";
import { CustomInput, EmptyLayout, MultipleTextInput } from '../../components';
import { favEmpty } from '../../utils/images';
import { useLocalization } from '../../customHooks/LocalizationHook';
import { MainStyle } from '../../utils/theme';
import HeaderComponent from '../../components/layaouts/HeaderComponent';
import { useNavigation } from '@react-navigation/native';
import { useAppDispatch } from '../../redux/store';
import { setIsScreenLoginModal } from '../../redux/slices/registerSlice';

const favotireData = [];

export const SearchScreen = () => {
  const {t} = useLocalization();
  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const input = useRef();

  const onRightIconPress = () => {
    console.log('handleright icon');
  }

  const handleButton = () => {
    dispatch(setIsScreenLoginModal());
    // navigation.navigate('AuthScreen');
  }

  const onLeftIconPress = () => {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
      <HeaderComponent leftIcon={<LeftChevronIcon />} onLeftIconPress={onLeftIconPress} title={t('searchProductPlaceholder')}></HeaderComponent>
      <View style={MainStyle.ph3}>
        <CustomInput ref={input} placeholder={t('writeText')} />
      </View>
      {/* {favotireData.length 
        ? <>
        
        </> : 
        <EmptyLayout 
          imageSource={favEmpty} 
          title={t('writeToSearch')} 
          text={t('writeToSearchText')}
          buttonText={t('goToMain')}
          onButtonPress={handleButton}
        />
      } */}
    </SafeAreaView>
  ) 
}