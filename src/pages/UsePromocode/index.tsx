import { SafeAreaView, StyleSheet, TextInput, View } from "react-native"
import HeaderComponent from "../../components/layaouts/HeaderComponent"
import { useLocalization } from "../../customHooks/LocalizationHook"
import { MainStyle, constant, lightTheme } from "../../utils/theme";
import { LeftChevronIcon } from "../../components/icons";
import { useNavigation } from "@react-navigation/native";
import { useCallback, useState } from "react";
import { CustomTextComponent } from "../../components";

export const UsePromocodeScreen = () => {
    const {t} = useLocalization();
    const navigation = useNavigation();
    const [value, setValue] = useState('');

    const goBack = useCallback(() => {
        navigation.goBack();
    }, [])

    const onChangeText = useCallback((e) => {
        if(e.length < 401) {
            setValue(e)
        } else {

        }      
    }, [])

    const handleNotification = () => {}

    return (
        <SafeAreaView style={MainStyle.layout}>
            <HeaderComponent 
                leftIcon={<LeftChevronIcon />}
                onLeftIconPress={goBack}
                title={t('promocodes')}
            />

            <View style={MainStyle.p3}>
                {/* <View style={styles.inputWrapper}>
                    <TextInput value={value} multiline={true} onChangeText={onChangeText} autoFocus={true} placeholder={t('writeBasketText')} style={styles.input} />
                    <CustomTextComponent style={MainStyle.tar} textType="regular" color="gray">0 / 400</CustomTextComponent>
                </View> */}
                
            </View>
        </SafeAreaView>    
    )
}

const styles = StyleSheet.create({
    inputWrapper: {
        borderWidth: 2,
        borderColor: lightTheme.gray,
        borderRadius: constant.space3,
        paddingHorizontal: constant.space3,
        paddingVertical: constant.space2,
    },
    input: {
        minHeight: 200,
        ...MainStyle.inputStyle,
    }
});