import { useNavigation, useRoute } from "@react-navigation/native";
import React, { useCallback, useEffect, useState } from "react";
import {  View, SafeAreaView } from "react-native";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import { useLocalization } from "../../customHooks/LocalizationHook";
import { useAppDispatch } from "../../redux/store";
import { showBottomBasketLink } from "../../redux/slices/basketSlice";
import { MainStyle } from "../../utils/theme";
import { flowerShopData, recommendedList } from "../../MOCK/flowersShop";
import { CardItem } from "../../components";
import HeaderComponent from "../../components/layaouts/HeaderComponent";
import { LeftChevronIcon } from "../../components/icons";
import { toyMarketData } from "../../MOCK/toysMarkets";

export const ShopsScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const {t} = useLocalization();
  const dispatch = useAppDispatch();

  const [data, setData] = useState(route?.params?.value === 'recommended' ? recommendedList : route?.params?.value === 'flowers' ? flowerShopData : toyMarketData)

  useEffect(() => {
    dispatch(showBottomBasketLink(true))
  }, [])

  const renderItem = ({ item } :any) => (
    <View style={MainStyle.mb3}>
        <CardItem
            id={item.id}
            handleItem={handleItem}
            handleFavorite={handleFavorite}
            title={item.title}
            content={item.desc}
            freeDelivery={item.freeDelivery}
            isOpen={item.open}
            imageSource={item.img}
            rate={item.rate}
            reviewCount={item.reviewsCount}
            containerWidth={"100%"}
        />
    </View>
    
  );

    const handleItem = (id: any) => {
        navigation.navigate('StorePage', {storeId: id});
    }

    const handleFavorite = (id:any) => {
        console.log('handleFavorite product', id); 
    }

  const onLeftIconPress = () => {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={MainStyle.layout}>
        <HeaderComponent leftIcon={<LeftChevronIcon />} onLeftIconPress={onLeftIconPress} title={t(route?.params?.value)} titleStyle={MainStyle.tac}></HeaderComponent>

        <FlatList contentContainerStyle={[MainStyle.p3, MainStyle.bottomPadding]} data={data} keyExtractor={(item) => item?.id} renderItem={renderItem} />
    </SafeAreaView>
  );
}
