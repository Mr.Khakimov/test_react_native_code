import React, { useState } from 'react';
import { ThemeContext } from '../context/ThemeContext';
import {lightTheme, darkTheme} from "../utils/theme";

export type ThemeProviderType = {
    children?: any,
}

export const ThemeProvider: React.FC<ThemeProviderType> = ({ children }) => {
    const [theme, setTheme] = useState<{[key: string]:string}>(lightTheme);

    const toggleTheme = () => {
        setTheme((prevTheme: any) => (prevTheme === lightTheme ? darkTheme : lightTheme));
    };

    return (
        <ThemeContext.Provider value={{ theme, toggleTheme }}>
            {children}
        </ThemeContext.Provider>
    );
};
