import {LocalizationContext, SupportedLocales} from "../context/LocalizationContext";
import {ReactNode, useState} from "react";
import {en, uz, ru} from '../utils/translations';

type Translation = { [key: string]: string };

type LocalizationProviderType = {
    children?: ReactNode
}
export const LocalizationProvider = ({ children } : LocalizationProviderType) => {
    const [locale, setLocale] = useState<SupportedLocales>('ru');

    const translations: { [locale in SupportedLocales]: Translation } = {en, uz, ru};

    const t = (key: string) => translations[locale][key] || key;

    return (
        <LocalizationContext.Provider value={{ locale, setLocale, t }}>
            {children}
        </LocalizationContext.Provider>
    );
};
