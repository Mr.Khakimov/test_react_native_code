import React, { createContext } from 'react';

export type SupportedLocales = 'ru' | 'uz' | 'en';

type LocalizationContextType = {
    locale: SupportedLocales;
    setLocale: React.Dispatch<React.SetStateAction<SupportedLocales>>;
    t: (key: string) => string;
};

export const LocalizationContext = createContext<LocalizationContextType | undefined>(undefined);
