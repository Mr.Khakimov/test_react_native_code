import {createContext} from "react";

interface IThemeContextType {
    theme: any;
    toggleTheme: () => void;
}

export const ThemeContext = createContext<IThemeContextType | undefined>(undefined);
