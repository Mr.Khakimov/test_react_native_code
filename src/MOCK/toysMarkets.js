const sh1 = require('../../assets/shopImages/toys/chicco.png');
const sh2 = require('../../assets/shopImages/toys/chico.jpg');
const sh3 = require('../../assets/shopImages/toys/firsttoy2.png');
const sh4 = require('../../assets/shopImages/toys/firsttoy1.jpg');
const sh5 = require('../../assets/shopImages/toys/happykids1.png');
const sh6 = require('../../assets/shopImages/toys/happykids2.jpg');
const sh7 = require('../../assets/shopImages/toys/igrilya.png');
const sh8 = require('../../assets/shopImages/toys/igrilya1.jpg');
const sh9 = require('../../assets/shopImages/toys/kapika1.png');
const sh10 = require('../../assets/shopImages/toys/kapika.jpg');
const sh11 = require('../../assets/shopImages/toys/kindskings1.png');
const sh12 = require('../../assets/shopImages/toys/kindkids1.jpg');
const sh13 = require('../../assets/shopImages/toys/lego.png');
const sh14 = require('../../assets/shopImages/toys/lego1.jpg');
const sh15 = require('../../assets/shopImages/toys/mylogo1.png');
const sh16 = require('../../assets/shopImages/toys/mylogo.jpg');
const sh17 = require('../../assets/shopImages/toys/pony1.png');
const sh18 = require('../../assets/shopImages/toys/pony.jpg');
const sh19 = require('../../assets/shopImages/toys/quemimo1.png');
const sh20 = require('../../assets/shopImages/toys/quemimo.jpg');

export const toyMarketData = [
    {
      id: '211',
      title: 'CHICCO',
      desc: 'Огромный выбор детских игрушек',
      open: true,
      shopTime: ['09:00', '21:00'],
      freeDelivery: true,
      rate: '4.75',
      reviewsCount: '56',
    //   img: 'https://example.com/toy-market-1.jpg',
      shopIcon: sh1,
      img: sh2,
      location: 'Tashkent',
    },
    {
      id: '212',
      title: 'FIRST TOY',
      desc: 'Игрушки с любимыми персонажами',
      open: true,
      shopTime: ['10:00', '22:00'],
      freeDelivery: true,
      rate: '4.88',
      reviewsCount: '78',
      shopIcon: sh3,
      img: sh4,
    //   img: 'https://example.com/toy-market-2.jpg',
      location: 'Tashkent',
    },
    {
      id: '213',
      title: 'HAPPY KIDS',
      desc: 'Игрушки для малышей',
      open: true,
      shopTime: ['08:00', '20:00'],
      freeDelivery: true,
      rate: '4.65',
      reviewsCount: '42',
    //   img: 'https://example.com/toy-market-3.jpg',
        shopIcon: sh5,
        img: sh6,
        location: 'Fergana',
    },
    {
      id: '214',
      title: 'Игруля',
      desc: 'Радужные игрушки для детей',
      open: true,
      shopTime: ['10:00', '21:00'],
      freeDelivery: true,
      rate: '4.92',
      reviewsCount: '64',
    //   img: 'https://example.com/toy-market-4.jpg',
        shopIcon: sh7,
        img: sh8,
      location: 'Tashkent',
    },
    {
      id: '215',
      title: 'Капика',
      desc: 'Игрушки для всех возрастов',
      open: true,
      shopTime: ['07:00', '19:00'],
      freeDelivery: false,
      rate: '4.33',
      reviewsCount: '29',
    //   img: 'https://example.com/toy-market-5.jpg',
        shopIcon: sh9,
        img: sh10,
        location: 'Fergana',
    },
    {
      id: '216',
      title: 'Десятое каралевство',
      desc: 'Безопасные игрушки для детей',
      open: true,
      shopTime: ['09:00', '23:00'],
      freeDelivery: true,
      rate: '4.77',
      reviewsCount: '51',
    //   img: 'https://example.com/toy-market-6.jpg',
        shopIcon: sh11,
        img: sh12,
      location: 'Tashkent',
    },
    {
      id: '217',
      title: 'Магазин игрушек "ЛЕГО"',
      desc: 'Игрушки сказочных ЛЕГО',
      open: true,
      shopTime: ['08:30', '22:30'],
      freeDelivery: true,
      rate: '4.84',
      reviewsCount: '69',
    //   img: 'https://example.com/toy-market-7.jpg',
        shopIcon: sh13,
        img: sh14,
      location: 'Fergana',
    },
    {
      id: '218',
      title: 'My Logo',
      desc: 'Игры и развлечения для детей',
      open: true,
      shopTime: ['09:30', '21:30'],
      freeDelivery: true,
      rate: '4.96',
      reviewsCount: '82',
    //   img: 'https://example.com/toy-market-8.jpg',
        shopIcon: sh15,
        img: sh16,
      location: 'Tashkent',
    },
    {
      id: '219',
      title: 'Pony',
      desc: 'Магазин игрушек для малышей',
      open: true,
      shopTime: ['10:30', '20:30'],
      freeDelivery: true,
      rate: '4.71',
      reviewsCount: '47',
    //   img: 'https://example.com/toy-market-9.jpg',
        shopIcon: sh17,
        img: sh18,
        location: 'Fergana',
    },
    {
      id: '220',
      title: 'Que MIMO',
      desc: 'Детские игрушки для радости',
      open: true,
      shopTime: ['08:00', '24:00'],
      freeDelivery: true,
      rate: '4.89',
      reviewsCount: '123',
    //   img: 'https://example.com/toy-market-10.jpg',
        shopIcon: sh19,
        img: sh20,
      location: 'Tashkent',
    },
  ];