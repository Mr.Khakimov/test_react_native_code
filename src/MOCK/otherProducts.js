export const ordersData = [
    {
        "id": 1,
        "name": "Букет из 25 хризантем сантини",
        "deliveryTime": "2",
        "price": "196000",
        "rate": "4.56",
        "reviewsCount": "18",
        "storeId": "10",
        "img": "https://buketopt.ru/upload/resize_cache/iblock/cb7/530_530_14bd9de518dfe6e8e138a6a8540ab5874/cb72df39711dc30cb90dd49a4fd3902d.jpg"
    },
    {
        "id": 2,
        "name": "Розовый букет 'Любовь и нежность'",
        "deliveryTime": "3",
        "price": "225000",
        "rate": "4.78",
        "reviewsCount": "20",
        "storeId": "10",
        "img": "https://tribuketa.ru/wp-content/uploads/2017/08/buket-nevesty-lyubov-i-nezhnost.jpg"
    },
    {
        "id": 3,
        "name": "Тюльпаны 'Весеннее настроение'",
        "deliveryTime": "1",
        "price": "155000",
        "rate": "4.42",
        "reviewsCount": "3",
        "storeId": "12",
        "img": "https://www.studiofloristic.ru/files/catalog/3948/w1000_f61d1e3217a09561ca4e415ff2952ec3.jpg"
    },
    {
        "id": 4,
        "name": "Корзина с пионами и розами",
        "deliveryTime": "2",
        "price": "295000",
        "rate": "4.89",
        "reviewsCount": "34",
        "storeId": "1",
        "img": "https://nflo.ru/upload/iblock/4a8/4a8274caff1205d4f81a1ad2bd8f1f54.JPG"
    },
    {
        "id": 5,
        "name": "Букет из 15 гвоздик 'Романтика'",
        "deliveryTime": "2",
        "price": "120000",
        "rate": "4.34",
        "reviewsCount": "21",
        "storeId": "2",
        "img": "https://flowersvalley.ru/images/products/954/555d11da4c9b1a8a53324d10514c42ba.webp"
    },
    {
        "id": 6,
        "name": "Сиреневая роза 'Загадка'",
        "deliveryTime": "1",
        "price": "80000",
        "rate": "4.75",
        "reviewsCount": "12",
        "storeId": "3",
        "img": "https://flor2la.ru/assets/cache_image/images/user/upload/acc6bc7e857610a7cacd5086fe816eee_800x800_ff8.jpeg"
    },
    {
        "id": 7,
        "name": "Пионы в оранжевой упаковке",
        "deliveryTime": "3",
        "price": "245000",
        "rate": "4.67",
        "reviewsCount": "93",
        "storeId": "4",
        "img": "https://via.placeholder.com/400x300?text=Пионы+в+оранжевой+упаковке"
    },
    {
        "id": 8,
        "name": "Букет из 20 лилий 'Летний вечер'",
        "deliveryTime": "2",
        "price": "210000",
        "rate": "4.54",
        "reviewsCount": "1",
        "storeId": "5",
        "img": "https://cs2.livemaster.ru/storage/ea/e6/523bbf77e8a38baa70ea0e14e03t--kartiny-i-panno-kartina-maslom-romashki.jpg"
    },
    {
        "id": 10,
        "name": "Букет роз и орхидей 'Аромат рая'",
        "deliveryTime": "2",
        "price": "280000",
        "rate": "4.81",
        "reviewsCount": "6",
        "storeId": "6",
        "img": "https://proflowers.ua/upload/images/showcase/1355.jpg"
    },
    {
        "id": 11,
        "name": "Тюльпаны 'Весеннее настроение'",
        "deliveryTime": "1",
        "price": "155000",
        "rate": "4.42",
        "reviewsCount": "9",
        "storeId": "7",
        "img": "https://dostavka-tsvety.ru/wp-content/uploads/2019/12/25_raznotsvetnyih_tyulpanov-9430_4b3_b.jpg"
    },
    {
        "id": 12,
        "name": "Пионы в оранжевой упаковке",
        "deliveryTime": "3",
        "price": "245000",
        "rate": "4.67",
        "reviewsCount": "11",
        "storeId": "3",
        "img": "https://www.rosemarkt.ru/resources/data/photos/5b3e740d77a30/big.jpg"
    },
    {
        "id": 13,
        "name": "Розовый букет 'Любовь и нежность'",
        "deliveryTime": "3",
        "price": "225000",
        "rate": "4.78",
        "reviewsCount": "5",
        "storeId": "4",
        "img": "https://cveti-butovo.ru/assets/cache_image/products/1174/product/img-20230511-wa0002_650x650_215.jpg"
    },
    {
        "id": 14,
        "name": "Сиреневая роза 'Загадка'",
        "deliveryTime": "1",
        "price": "80000",
        "rate": "4.75",
        "reviewsCount": "1",
        "storeId": "5",
        "img": "https://via.placeholder.com/400x300?text=Сиреневая+роза+'Загадка'"
    },
    {
        "id": 15,
        "name": "Букет из 15 гвоздик 'Романтика'",
        "deliveryTime": "2",
        "price": "120000",
        "rate": "4.34",
        "reviewsCount": "5",
        "storeId": "15",
        "img": "https://venusinfleurs.ru/image/catalog/product/2204/2204_1.jpg"
    },
    {
        "id": 16,
        "name": "Букет из 25 хризантем сантини",
        "deliveryTime": "2",
        "price": "196000",
        "rate": "4.56",
        "reviewsCount": "11",
        "storeId": "2",
        "img": "https://krasnodarflora.ru/upload/resize_cache/iblock/d5d/801_801_2/d5d381bf3313e223f0eb6638c870e3eb.jpg"
    },
    {
        "id": 17,
        "name": "Корзина с пионами и розами",
        "deliveryTime": "2",
        "price": "295000",
        "rate": "4.89",
        "reviewsCount": "2",
        "storeId": "3",
        "img": "https://via.placeholder.com/400x300?text=Корзина+с+пионами+и+розами"
    },
    {
        "id": 18,
        "name": "Букет из 20 лилий 'Летний вечер'",
        "deliveryTime": "2",
        "price": "210000",
        "rate": "4.54",
        "reviewsCount": "21",
        "storeId": "1",
        "img": "https://nflo.ru/upload/iblock/4a8/4a8274caff1205d4f81a1ad2bd8f1f54.JPG"
    },
    {
        "id": 19,
        "name": "Ромашки в горшке 'Детские радости'",
        "deliveryTime": "1",
        "price": "90000",
        "rate": "4.61",
        "reviewsCount": "1",
        "storeId": "2",
        "img": "https://ir.ozone.ru/s3/multimedia-a/c1000/6324729334.jpg"
    },
    {
        "id": 20,
        "name": "Букет роз и орхидей 'Аромат рая'",
        "deliveryTime": "2",
        "price": "280000",
        "rate": "4.81",
        "reviewsCount": "4",
        "storeId": "1",
        "img": "https://www.buket-piter.ru/thumb/jJR9_7NVtGlOVTLzO4w7pw/285r285/203678/%D0%B1%D1%83%D0%BA%D0%B5%D1%82_%D1%81_%D0%B0%D0%BB%D1%8C%D1%81%D1%82%D1%80%D0%BE%D0%BC%D0%B5%D1%80%D0%B8%D0%B5%D0%B9_33.jpg"
    },
    {
        "id": 21,
        "name": "Тюльпаны 'Весеннее настроение'",
        "deliveryTime": "1",
        "price": "155000",
        "rate": "4.42",
        "reviewsCount": "6",
        "storeId": "1",
        "img": "https://dostavka-tsvety.ru/wp-content/uploads/2019/12/25_raznotsvetnyih_tyulpanov-9430_4b3_b.jpg"
    },
    {
        "id": 22,
        "name": "Пионы в оранжевой упаковке",
        "deliveryTime": "3",
        "price": "245000",
        "rate": "4.67",
        "reviewsCount": "7",
        "storeId": "2",
        "img": "https://flowers.ua/images/Flowers/3184.jpg"
    }
];