export const endpoints = {
    sendSms: '/client',
    confirmSms: '/valid',
    getMe: '/me',
    logout: '/logout',
    registerUser: '/register',
};