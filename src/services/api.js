import axios from 'axios';



const apiConfigs = () => {
  // const token = store?.getState()?.user?.access_token;
  // const locale = store.getState().settings.locale;

  return {
    // Authorization: `Bearer ${token}`,
    "User-Agent-OS": Platform.OS,
    "Content-Type": "application/json",
    "brand-id": 1,
    // timeout: 5000,
    // "Device-Mode": "mobile",
    // "User-Agent-Version": app_version,
    // Device: "client",
    // lang: locale === 'en' ? 'ru' : locale,
  }
}

export const BASE_URL = "http://api.ildam.uz:1701/cli";

const api = axios.create({
  // baseURL: 'https://217.30.167.30:1700/cli',
  baseURL: BASE_URL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
  },
});

// api.interceptors.request.use(
//   (config) => {
//     // const token = getAuthToken(); // Replace with your token retrieval logic
//     // if (token) {
//     //   config.headers.Authorization = `Bearer ${token}`;
//     // }
//     return config;
//   },
//   (error) => {
//     return Promise.reject(error);
//   }
// );

// api.interceptors.response.use(
//   (response) => {
//     return response;
//   },
//   (error) => {
//     // console.log('HTTP Error:', error);
//     if (error.response) {
//       // Server responded with an error (e.g., 400, 500)
//       // console.log('HTTP Error:', error.response.status);
//       // console.log('Response Data:', error.response.data);
//     } else if (error.request) {
//       // No response received (e.g., network error)
//       console.log('Request Error:', error.request);
//     } else {
//       // Something happened in setting up the request
//       // console.log('Request Setup Error:', error.message);
//     }
//     return Promise.reject(error);
//   }
// );


api.interceptors.request.use(
    async config => {
      const customConfigs = apiConfigs();
      config.headers = {
        ...config.headers,
        ...customConfigs
      };
      return config;
    },
    error => Promise.reject(error.response),
);

api.interceptors.response.use(
    response => successHandler(response),
    error => errorHandler(error),
);

api.interceptors.response.use(x => {
  console.log(
    `Execution time for: ${x.config.url} - ${
      new Date().getTime() - x.config.meta.requestStartedAt
    } ms`,
  );
  return x;
});

api.interceptors.request.use(x => {
  x.meta = x.meta || {};
  x.meta.requestStartedAt = new Date().getTime();
  return x;
});

const errorHandler = error => {
  console.log('errorHandler', JSON.stringify(error?.response?.data))
  if (
    JSON.stringify(error).includes("Network Error") ||
    error?.response?.code === 12002
  ) {
    // toastBottom("Network Error");
  }
  switch (error?.response?.data?.code) {
    case 500: {
      // toastBottom(error?.response?.data?.message || "Error", 200);
      break;
    }
    case 501:
      break;
    case 422:
      break;
    case 404:
      break;
    case 400:
      break;
    case 401: {
      break;
    }
  }
  return Promise.reject(error);
};

const successHandler = response => {
  const customConfigs = apiConfigs();
  response.headers = {
         ...response.headers,
         ...customConfigs
       };
  if (response.status !== 200 || response.data == null) {
    return Promise.reject("Нулевой результат! (null)");
  }
  return response;
};


export default api;