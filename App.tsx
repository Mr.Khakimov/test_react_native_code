import React from 'react';
import {
  StyleSheet,
  ViewStyle
} from 'react-native';
import Navigation from "./src/navigation/navigation";

import store from './src/redux/store';
import { Provider } from 'react-redux';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { SafeAreaProvider } from "react-native-safe-area-context";
import { ThemeProvider } from './src/providers/ThemeProvider';
import {LocalizationProvider} from "./src/providers/LocalizationProvider";
import { BottomSheetModalProvider } from '@gorhom/bottom-sheet';
import { PortalHost, PortalProvider } from '@gorhom/portal';
import { AuthModal } from './src/pages/Auth/AuthModal';
import { ToBasketBtn } from './src/components/ToBasketBtn';

function App(): JSX.Element {

  return (
      <Provider store={store}>
        <SafeAreaProvider>
          <GestureHandlerRootView style={styles.container}>
              <PortalProvider>
                <ThemeProvider>
                    <LocalizationProvider>
                          <Navigation />
                          
                          <PortalHost name="BasketHost" />
                          <PortalHost name="CustomPortalHost1" />

                          <AuthModal />
                    </LocalizationProvider>
                </ThemeProvider>
              
            </PortalProvider>
          </GestureHandlerRootView>
        </SafeAreaProvider>
      </Provider>
  );
}

type Style = {
  container: ViewStyle;
};

const styles = StyleSheet.create<Style>({
  container: {
    flex: 1,
  },
});

export default App;
